<?php 
    session_start();
    include_once("php/functions/misc.php");
    date_default_timezone_set('Asia/Manila');


    $currentFileName = basename($_SERVER["SCRIPT_FILENAME"], '.php');
    

    //set Document title + Redirection for Illegal entry.
    $pageName = "";
    switch($currentFileName){
        case "index":       $pageName = "Home"; break;
        case "search":      $pageName = "Search"; break;
		case "view-thesis": checkIfLogin(NORM,MOD,ADMIN); $pageName = "Viewing Research Study"; break;
		case "pending-thesis": checkIfLogin(MOD,ADMIN); $pageName = "Viewing Research Study"; break;
        case "contact":     checkIfLogin(NORM,MOD,ADMIN); $pageName = "Contact"; break;
        case "add-thesis":  checkIfLogin(MOD,ADMIN); $pageName = "New Research Study"; break;
		case "edit-thesis":  checkIfLogin(MOD,ADMIN); $pageName = "Edit Research Study"; break;
		case "userlogs":    checkIfLogin(MOD,ADMIN); $pageName = "User Activity Logs"; break;
        case "researchlogs":checkIfLogin(MOD,ADMIN); $pageName = "Research Study Logs"; break;
        case "inbox":       checkIfLogin(MOD,ADMIN); $pageName = "Messages"; break;
        case "appointment": checkIfLogin(MOD,ADMIN); $pageName = "Appointments"; break;
        case "accounts":    checkIfLogin(ADMIN); $pageName = "Accounts"; break;
        case "schools":     checkIfLogin(ADMIN); $pageName = "Schools"; break;
		case "courses":     checkIfLogin(ADMIN); $pageName = "Courses"; break;
		case "pending-thesis":     checkIfLogin(ADMIN); $pageName = "Pending Thesis"; break;
		
        default:            $pageName = "Library";
    }
    
  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="description" content="A collection of Thesis">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/font-awesome/css/all.css">
    <title><?php echo $pageName;?> - Online ICT Library System</title>
</head>
<body style="background: rgb(200,200,200);" class="d-flex flex-column h-100">
	<!-- Start of Header -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark-blue py-3" >
		<div class="container">
			<a class="navbar-brand" href="#">Online ICT Library</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarText">
				<ul class="navbar-nav mr-auto">
					
						<li class="nav-item <?php returnActive($currentFileName,"index");?> "> <a class="nav-link" href="index.php">Home <!-- <span class="sr-only">(current)</span> --></a> </li>
					
						<li class="nav-item <?php returnActive($currentFileName,"search","view-thesis");?>"> <a class="nav-link " href="search.php">Search</a> </li>
						<?php if(checkUser(MOD,ADMIN)): ?>
							<li class="nav-item <?php returnActive($currentFileName,"add-thesis","edit-thesis");?>"> <a class="nav-link " href="add-thesis.php">Add Research Study</a> </li>
							<li class="nav-item <?php returnActive($currentFileName,"userlogs");?>"> 				 <a class="nav-link " href="userlogs.php">User Logs</a> </li>
							<li class="nav-item <?php returnActive($currentFileName,"researchlogs");?>"> 			 <a class="nav-link " href="researchlogs.php">Research Logs</a> </li>
							<?php if(checkUser(ADMIN)): ?>
								<li class="nav-item <?php returnActive($currentFileName,"accounts");?>"> 			 <a class="nav-link " href="accounts.php">Accounts</a> </li>
								<li class="nav-item <?php returnActive($currentFileName,"schools");?>"> 			 <a class="nav-link " href="schools.php">Schools</a> </li>
								<li class="nav-item <?php returnActive($currentFileName,"courses");?>"> 			 <a class="nav-link " href="courses.php">Courses</a> </li>
								<li class="nav-item <?php returnActive($currentFileName,"pending-thesis");?>"> 		 <a class="nav-link " href="pending-thesis.php">Pending</a> </li>
								
					
						<?php endif ?>
					<?php endif ?>
				</ul>
				<span class="navbar-text">
					<?php if(isset($_SESSION["username"])): ?>
						 <div class="dropdown">
							<a class="  dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<?php echo $_SESSION["username"]; ?>
							</a>

							<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
								<a class="dropdown-item text-primary" href="logout.php">Logout</a>
							</div>
						</div>
					<?php else: ?>		
						<a href="login.php">Login</a>
					<?php endif ?>
					
				</span>
			</div>
		</div>
	</nav>
	<!-- End of Header -->