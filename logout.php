<?php
    session_start();
    include_once("php/functions/misc.php");

    checkIfLogin(NORM,MOD,ADMIN);

    session_destroy();
    header("location: login.php");
    exit();

?>