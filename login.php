<?php 
    session_start();
    include_once("php/functions/misc.php");

    //Check if already logged in. then redirect to home page.
    checkIfAlreadyLogin();

    //TODO--Add Redirect to homepage if already logged in.

    $errors = array("username"=>"","password"=>"","invalid"=>FALSE);
    $input  = array("username"=>"","password"=>"");
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $input["username"]  = cleanInput($_POST["username"]);
        $input["password"]  = cleanInput($_POST["password"]);

        $errors["username"] = checkEmpty($input["username"],"Username");
        $errors["password"] = checkEmpty($input["password"],"Password");

        //Check all Errors (If there is any)
        $validated = true;
        foreach ($errors as $error) {
            if($error != ""){
                $validated = false;
            }
        }

        if($validated){
            include_once("php/functions/Query/AccountController.php");
            $AccountController = new AccountController();
            $account = $AccountController->FetchLoginAccount($input["username"],$input["password"]);
            if($account["id"] != NULL){
                //Set Session
                $_SESSION["user_id"]  = $account["id"];
                $_SESSION["username"] = $account["username"];
                $_SESSION["fname"]    = $account["fname"];
                $_SESSION["lname"]    = $account["lname"];
                $_SESSION["access"]   = $account["access"];
                $_SESSION["school"]   = $account["school"];

                include_once("php/functions/Query/LogController.php");
                $LogController = new LogController();
                $LogController->InsertLogUser($_SESSION["user_id"],"login");
                header("Location: index.php");
                exit();
            }else{
                $errors["invalid"] = TRUE;
            }
        }
        
    }
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Login - Online Thesis Library</title>
    <meta name="description" content="A collection of Thesis">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
</head>

<body class="bg-gray-900">
    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-md-9 col-lg-12 col-xl-10 mt-5">
                <div class="card shadow-lg o-hidden border-0 my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-flex">
                                <div class="flex-grow-1 bg-login-image" style="background-image: url(&quot;assets/img/login.jpg&quot;);"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <?php if($errors["invalid"]): ?>
                                        <div class="bg-danger py-2 rounded text-white mb-3">
                                            Invalid Username or Password
                                        </div>
                                        <?php endif ?>
                                        <h4 class="text-dark mb-5">Welcome</h4>
                                        
                                    </div>
                                    <form class="user" action="" method="post">
                                        <?php returnErrorMsg($errors["username"]); ?>
                                        <div class="form-group"><input class="form-control form-control-user" type="text" id="username"  placeholder="Username" name="username" value="<?php echo $input["username"];?>"></div>
                                        <?php returnErrorMsg($errors["password"]); ?>
                                        <div class="form-group"><input class="form-control form-control-user" type="password" id="password" placeholder="Password" name="password"></div>
                                        <button class="btn btn-primary btn-block text-white btn-user mt-5" type="submit">Login</button>
                                        <a class="btn btn-danger btn-block btn-user" href="index.php">Back to Home</a>
                                    </form>
                                    <div class="mt-1">
                                    Need an account?<a href="register.php"> Create One</a>
                                    </div>
                                    <!-- <a class="btn btn-info btn-block mt-5" href="index.php">Back to Home</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src="assets/js/script.min.js"></script>
</body>

</html>