<?php 
    include_once("header.php");
    include_once("php/functions/misc.php");
    
    
?>
            <div class="container-fluid ">
                <h3 class="mb-4 text-dark">Appointments</h3>
                    <?php 
                         if(isset($_GET["view"])){
                             include_once("php/pages-includes/view-inbox.php");
                         }else{
                             include_once("php/pages-includes/display-appointments.php");
                         }
                    
                    ?>
                </div>
            </div>
<?php 
    include_once("footer.php");
?>
