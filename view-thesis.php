<?php 
    include_once("header.php");

    //Check if GET_edit is set,
    if(!isset($_GET["id"]) || empty($_GET["id"])){
        //echo 'Cannot find ID | <a href="index.php">Click here to return</a>';
        include_once("404.php");
        exit();
    }

    include_once("php/functions/misc.php");
    include_once("php/functions/Query/ThesisController.php");
    include_once("php/functions/Query/SchoolController.php");
    include_once("php/functions/Query/CourseController.php");
    include_once("php/functions/Query/LogController.php");
    $ThesisController = new ThesisController();
    $SchoolController = new SchoolController();
    $CourseController = new CourseController();
    $LogController = new LogController();
    $thesis = $ThesisController->FetchThesis($_GET["id"]);
    //Log Searched thesis


    $LogController->InsertLogSearch($_SESSION["user_id"],$thesis["id"]);
    $ThesisController = NULL; // CLOSE SQL CONNECTIOn
    $LogController = NULL;

    
?>
    <div class="w-75 mx-auto mt-5">
        <h3 class="mb-3 text-dark"><?php echo $pageName; ?></h3>
        <?php $back = $_SESSION["forback"];?>
        <a class="btn btn-outline-primary mb-3" href="<?php echo $_SESSION["forback"];?>">Back</a>
        <div class="card shadow">
            <div class="card-header py-3">
                <p class="text-primary m-0 font-weight-bold"><?php echo $thesis["title"];?> by (<?php echo $thesis["author"];?>)</p>
                <small class="text-secondary" >Date Submitted: <span class="custom-txtcolor-1"> <?php echo substr($thesis["published_date"],0,7);?> </span></small>
                <p class="m-0 text-secondary">School: <span class="custom-txtcolor-3">  <?php echo $SchoolController->FetchSchool($thesis["school"])["name"];?></span>  </p>
                <p class="m-0 text-secondary">Course: <span class="custom-txtcolor-3">  <?php echo $CourseController->FetchCourse($thesis["course"])["name"];?></span> </p>
            </div>
            <div class="card-body text-dark">
                <h4>Rationale / Introduction</h4>
                <p><?php echo $thesis["abstract"];?></p>        
            </div>
        </div>
    </div>

<?php 
    include_once("footer.php");
?>
