<?php 
    include_once("header.php");
    include_once("php/functions/misc.php");

    //Mod and Admin cannot send a message.
    if(checkUser(MOD,ADMIN)){
        include_once("404.php");
        exit();
    }

    //Check if GET_edit is set,
    if(!isset($_GET["id"]) || empty($_GET["id"])){
        //echo 'Cannot find ID | <a href="index.php">Click here to return</a>';
        include_once("404.php");
        exit();
    }

    include_once("php/functions/misc.php");
    include_once("php/functions/Query/ThesisController.php");
    include_once("php/functions/Query/SchoolController.php");
    include_once("php/functions/Query/CourseController.php");
    $ThesisController = new ThesisController();
    $SchoolController = new SchoolController();
    $CourseController = new CourseController();
    $thesis = $ThesisController->FetchThesis($_GET["id"]);
    $ThesisController = NULL; // CLOSE SQL CONNECTIOn

    $success = "";
    $errors = array("name"=>"","contact"=>"","message"=>"");
    $input  = array("name"=>"","contact"=>"","message"=>"","thesis"=>$thesis["id"]);

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $input["name"]    = cleanInput($_POST["name"]);
        $input["contact"] = cleanInput($_POST["contact"]);
        $input["message"] = cleanInput($_POST["message"]);

        $errors["name"]            = checkEmpty($input["name"],"Full Name");
        $errors["contact"]         = checkEmpty($input["contact"],"Contact Number");

        $currentNumber = strlen($_POST["contact"]);
        
        if($currentNumber != 12){
            $errors["contact"] .= " Contact number should be 12 digit. Starting (639)+9 digit number";
        }

 

        $errors["message"]         = checkEmpty($input["message"],"Message");
        
        //Check all Errors (If there is any)
        $validated = true;
        foreach ($errors as $error) {
            if($error != ""){
                $validated = false;
            }
        }

        if($validated){
            
            include_once("php/functions/Query/MessageController.php");
            $MessageController   = new MessageController();
            if($MessageController->InsertMessage($input)) {
                $success = "Message has been added.";
                $input = returnEmptyVar($input);
            }else{
                $success = '<p class="text-danger">There was an error sending this message</p>';
            }
            $MessageController = NULL;

        }

    }

?>

            <div class="container-fluid ">
                <h3 class="mb-4 text-dark">Contact Form</h3>
                <div class="card shadow">
                    <div class="card-header py-3">
                        <p class="text-primary m-0 font-weight-bold"><?php echo $thesis["title"];?> by (<?php echo $thesis["author"];?>)</p>
                        <small class="text-secondary" >Date published: <span class="custom-txtcolor-1"> <?php echo $thesis["published_date"];?> </span></small>
                        <p class="m-0 text-secondary">School: <span class="custom-txtcolor-3">  <?php echo $SchoolController->FetchSchool($thesis["school"])["name"];?></span>  </p>
                        <p class="m-0 text-secondary">Course: <span class="custom-txtcolor-3">  <?php echo $CourseController->FetchCourse($thesis["course"])["name"];?></span> </p>
                        
                    </div>
                    <div class="card-body ">
                        <?php returnSuccess($success); ?>
                        <h4>Please fill up this form</h4>
                        <form action="" method="post">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="name">Full Name <?php returnErrorMsg($errors["name"]); ?></label>
                                        <input class="form-control" type="text" id="name" name="name" value="<?php echo $input["name"];?>">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="contact">Contact Number <?php returnErrorMsg($errors["contact"]); ?></label>
                                        <input class="form-control" type="number" id="contact" name="contact" value="<?php echo $input["contact"];?>">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="message">Message <?php returnErrorMsg($errors["message"]); ?></label>
                                        <textarea class="form-control" id="message" rows="5" name="message"><?php echo $input["message"];?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="submit" class="form-control btn btn-secondary"value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<?php 
    //close sql connections
    $SchoolController = NULL;
    $CourseController = NULL;
    include_once("footer.php");
?>
