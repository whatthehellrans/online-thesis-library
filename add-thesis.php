<?php 
    include_once("header.php");
    include_once("php/functions/misc.php");
    include_once("php/functions/Query/SchoolController.php");
    include_once("php/functions/Query/CourseController.php");
    $SchoolController   = new SchoolController();
    $CourseController   = new CourseController();
    $schools = $SchoolController->FetchAllSchool(); //Will be using for School Dropdown selection.

    //Fetch Available Course for This School
    $schoolCourses = $SchoolController->FetchSchoolCourse($_SESSION["school"]); //Will be using for course dropdown.

    $courses = array();
    foreach ($schoolCourses as $schoolCourse) {
        $courseToAdd = $CourseController->FetchCourse($schoolCourse);
        array_push($courses,$courseToAdd);
    }


    $SchoolController = NULL; //We won't be needing this anymore
    $CourseController = NULL; //Don't need it anymore

    $errors = array("school"=>"","published_date"=>"","course"=>"","title"=>"","author"=>"","abstract"=>"");
    $input  = array("school"=>"","published_date"=>"","course"=>"","title"=>"","author"=>"","abstract"=>"","tags"=>"");
    $success = "";

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        //Clean all input before passing it to the array
        $input["school"]        = (checkUser(MOD,ADMIN))?  $_SESSION["school"] : cleanInput($_POST["school"]);
        $input["published_date"]= cleanInput($_POST["published_date"]);
        $input["title"]         = cleanInput($_POST["title"]);
        $input["author"]        = cleanInput($_POST["author"]);
        $input["abstract"]      = cleanInput($_POST["abstract"]);
        $input["course"]        = cleanInput($_POST["course"]);
        $input["tags"]          = cleanInput($_POST["title"]);
        
        //Validate all inputs
        $errors["school"]        = checkEmpty($input["school"],"School");
        $errors["published_date"]= checkEmpty($input["published_date"],"Published Date");
        $errors["title"]         = checkEmpty($input["title"],"Title");
        $errors["author"]        = checkEmpty($input["author"],"Author");
        $errors["abstract"]      = checkEmpty($input["abstract"],"Abstract");
        $errors["course"]        = checkEmpty($input["course"],"Course");

        $input["published_date"] .= "-01";

        if(strtotime($input["published_date"]) > time()){
            $errors["published_date"] = "You cannot set the Date Submitted in the Future.";
        }

        //Check all Errors (If there is any)
        $validated = true;
        foreach ($errors as $error) {
            if($error != ""){
                $validated = false;
            }
        }

        //Proceed to SQL INSERT if no error found.
        if($validated){
            include_once("php/functions/Query/ThesisController.php");
            $ThesisController   = new ThesisController();
            if($ThesisController->InsertThesisPending($input))
            {
                $success = "Thesis has been added.";

                $lastID = $ThesisController->FetchLastThesis();
                //Tags Handling-------------------
                if(isset($_POST["tags"]) && !empty($_POST["tags"])){
                    $tags = cleanInput($_POST["tags"]);
                    //Split String into Array by Commas
                    $tags = explode(",",$tags);
                    foreach ($tags as $tag) {
                        //Check if element is empty before passing it
                        if($tag != "" || $tag == " "){
                            $ThesisController->InsertTags($lastID,cleanInput($tag));
                        }
                    }
                }
                include_once("php/functions/Query/LogController.php");
                $LogController = new LogController();
                //$LogController->InsertLogThesis($lastID,$_SESSION["user_id"],"add");
            }else{
                $success = '<p class="text-danger">There was an error adding this thesis</p>';
            }

            //Destory the objects to close connection.
            $ThesisController = NULL;
            $input = returnEmptyVar($input);
        }

    }



?>
    <div class="w-75 mx-auto mt-5">
        <h3 class="mb-3 text-dark"><?php echo $pageName; ?></h3>
        <div class="card shadow">
            <div class="card-header py-3">
                <p class="text-primary w-0 font-weight-bold">Please fill up this form</p>
                <?php returnSuccess($success);?>
            </div>
            <div class="card-body">
                <form action="" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="school">School <?php returnErrorMsg($errors["school"]); ?></label>
                                <!-- Add Restriction for MODERATOR but not for ADMIN Accounts -->
                                <select <?php if(checkUser(MOD,ADMIN)) echo "disabled"; ?> class="form-control" id="school" name="school">
                                <!-- Loop the option from schools array -->
                                <?php foreach ($schools as $school) :?>
                                    <option <?php if(checkModSchool($school["id"])){echo "selected";}?> value="<?php echo $school["id"];?>" ><?php echo $school["name"];?></option>
                                <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="published_date">Date Submitted <?php returnErrorMsg($errors["published_date"]); ?></label>
                                <input class="form-control" type="month" name="published_date" id="published_date" value="<?php echo substr($input["published_date"],0,7);?>">
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="course">Course <?php returnErrorMsg($errors["course"]); ?></label>
                                <!-- TODO Add Restriction for MODERATOR Accounts -->
                                <select class="form-control" id="course" name="course">
                                <!-- Loop the option from schools array -->
                                <?php foreach ($courses as $course) :?>
                                    <option <?php if($input["course"] == $course["id"]) echo "selected"; ?> value="<?php echo $course["id"];?>" ><?php echo $course["name"];?></option>
                                <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Title <?php returnErrorMsg($errors["title"]); ?></label>
                                <input class="form-control" type="text" id="title" name="title" value="<?php echo $input["title"];?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="author">Author <?php returnErrorMsg($errors["author"]); ?></label>
                                <input class="form-control" type="text" id="author" name="author" value="<?php echo $input["author"];?>">
                            </div>
                        </div>
                        <!-- <div class="col-md-6">
                            <div class="form-group">
                                <label for="author">Tags <small>(Separate by commas)</small> </label>
                                <input class="form-control" type="text" id="tags" name="tags" value="<?php echo $input["tags"];?>">
                            </div>
                        </div> -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="abstract">Rationale / Introduction <?php returnErrorMsg($errors["abstract"]); ?></label>
                                <textarea class="form-control" id="abstract" rows="5" name="abstract"><?php echo $input["abstract"];?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <input type="submit" class="form-control btn btn-secondary"value="Submit">
                        </div>
                    </div>
                </form>  
            </div>
        </div>
    </div>

<?php 
    include_once("footer.php");
?>
