<?php 

    //TODO add pagination
    include_once("header.php");
    include_once("php/functions/misc.php");
    include_once("php/functions/Query/LogController.php");
    include_once("php/functions/Query/AccountController.php");
    include_once("php/functions/Query/ThesisController.php");

    $_SESSION["forback"] = basename($_SERVER['REQUEST_URI']);

    $LogController = new LogController();
    $AccountController = new AccountController();
    $ThesisController = new ThesisController();
    

    $search = "";
    if(isset($_GET["search"])){
        $search = cleanInput($_GET["search"]);
    }
    $UserResults = $AccountController->FetchSearchAccount($search);
    

?>
    <div class="w-75 mx-auto mt-5">
        <h3 class="mb-3 text-dark"><?php echo $pageName; ?></h3>
        <div class="card shadow">
            <div class="card-header py-3">
                <p class="text-primary m-0 font-weight-bold">Search for User's Name</p>
            </div>
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-6">
                            
                                <div class="input-group md-form form-sm form-1 pl-0">
                                    <div class="input-group-prepend">
                                        <button class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search text-black"></i></button>
                                    </div>
                                    <input class="form-control my-0 py-1" type="text" placeholder="First name / Last name" name="search" value="<?php echo $search;?>">
                                </div>
                            
                        </div>
    
                    </div>
                </form>
                <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                    <table class="table dataTable my-0" id="dataTable">
                        <thead>
                            <tr>
                                <th>User</th>
                                <th>Date - Time</th>
                                <th>Activity</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach ($UserResults as $userID) {
                                $schoolOfUser = $AccountController->FetchAccount($userID["id"])["school"];
                                if($_SESSION["school"] != $schoolOfUser && checkUser(MOD)){
                                    continue;
                                }
                                $UserHistorySearch = $LogController->FetchLogThesis($userID["id"]);
                                foreach($UserHistorySearch as $log) : ?>
                                <tr>
                                    <td><?php echo "(".$userID["id"].") ".$userID["fname"]." ".$userID["lname"]; ?></td>
                                    <td><?php echo $log["time"];?></td>
                                    <?php if($log["type"] == "add"):?>
                                        <td><?php echo "Added new Research Study: "?>  <a href="view-thesis.php?id=<?php echo $log["thesis_id"];?>"> <?php echo $ThesisController->FetchThesis($log["thesis_id"])["title"];?></a>  </td> 
                                    <?php elseif($log["type"] == "modify"): ?>
                                        <td><?php echo "Edited a Research Study: "?>  <a href="view-thesis.php?id=<?php echo $log["thesis_id"];?>"> <?php echo $ThesisController->FetchThesis($log["thesis_id"])["title"];?></a>  </td> 
                                    <?php endif; ?>
                                </tr>

                            <?php
                                endforeach;
                                ?>
                                
                        <?php  } ?>

                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php 

    $SchoolController = NULL; // to close sql connection
    include_once("footer.php");
?>
