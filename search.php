<?php
    //TODO ADD PAGINATION

    include_once("header.php");
    include_once("php/functions/misc.php");
    include_once("php/functions/Query/ThesisController.php");
    include_once("php/functions/Query/SchoolController.php");
    include_once("php/functions/Query/CourseController.php");

    $_SESSION["forback"] = basename($_SERVER['REQUEST_URI']);
    
    $ThesisController   = new ThesisController();
    $SchoolController   = new SchoolController();
    $CourseController   = new CourseController();
    


    if(isset($_GET["delete"]) && $_GET["delete"] != ""){
        include_once("php/functions/Query/MessageController.php");
        include_once("php/functions/Query/LogController.php");
        $MessageController = new MessageController();
        $LogController      = new LogController();

        $idToDelete = $_GET["delete"];
        $messages = $MessageController->FetchMessageWithThesisID($idToDelete);
        foreach ($messages as $msg) {
            $MessageController->DeleteAppointment($msg["id"]);
            $MessageController->DeleteMessage($msg["id"]);
        }
        
        if($ThesisController->DeleteThesis($idToDelete)){
            $LogController->DeleteLogSearchByThesis($idToDelete);
            $LogController->DeleteLogStudyByThesis($idToDelete);
            echo "Research Study Has been Deleted. ";
            echo '<a href="index.php">Click here to go back.</a>';
         }else{
            echo "There was an error deleting this Research Study.";
         }
         exit();
    }


    $schools    = $SchoolController->FetchAllSchool();
    $courses    = $CourseController->FetchAllCourse();
    $blankSearch = "true";

    $search = array("title"=>"","school"=>"","course"=>"");


    if(isset($_GET["title"]) && $_GET["title"] != ""){
        $search["title"] = $_GET["title"];
        $blankSearch = "false";
    }
    if(isset($_GET["school"]) && $_GET["school"] != ""){
        $search["school"] = $_GET["school"];
    }
    if(isset($_GET["course"]) && $_GET["course"] != ""){
        $search["course"] = $_GET["course"];
    }
    

    $results = $ThesisController->FetchThesisTitle($search);


    $url = "https://api.datamuse.com/words?ml=" . urlencode($search["title"]);

    $json = file_get_contents($url);
    $obj = json_decode($json);
    foreach ($obj as $data) {
        $search["title"] = $data->word;
        $sqlFetchResult = $ThesisController->FetchThesisTitle($search);
        if(!empty($sqlFetchResult)){
            $proceed_to_add = true;
            //Check for duplicate result
            foreach ($results as $result) {
                if($result["id"] == $sqlFetchResult[0]["id"]){
                    $proceed_to_add = false;
                }
            }
            if($proceed_to_add){
                array_push($results,$sqlFetchResult[0]);
            }

        }
        
    }


    $strResults = array();
    $logicResults = array();
    
    foreach ($results as $result) {
        $thesis_id = $result["id"];
        $title     = addslashes($result["title"]);
        $school    = addslashes($SchoolController->FetchSchool($result["school"])["name"]);
        $course    = addslashes($CourseController->FetchCourse($result["course"])["name"]);
        $allow_edit = $allow_delete = "false";
        //$allow_delete = (checkUser(ADMIN)) ? "true":"false";

        //Validation for edit
        $SchoolID = $SchoolController->FetchSchool($result["school"])["id"];
        if(/*checkUser(ADMIN) || */ (checkUser(MOD,ADMIN) && checkModSchool($SchoolID)) ){
            $allow_edit = "true";
            $allow_delete = "true";
        }

        $canView = checkUser(NORM,MOD,ADMIN) ? "true":"false";

        $strResult = "{
                     title: '$title',
                     school: '$school',
                     course: '$course',
                     allow_view: $canView,
                    },";
        $logicResult = "{thesis_id: $thesis_id, allow_delete: $allow_delete, allow_edit: $allow_edit,},";

        array_push($strResults,$strResult);
        array_push($logicResults,$logicResult);
    }
    ?>
    <script type="text/javascript">
        let items = [];
        let links = [];

        if(!<?php echo $blankSearch; ?>)
        {
            items = [
            <?php
            foreach ($strResults as $strResult) {
                echo $strResult;
            }    
            ?>
            ];
            links = [
                <?php
                foreach ($logicResults as $logicResult) {
                    echo $logicResult;
                }    
                ?>
            ];
        }
    </script>


    <div class="w-75 mx-auto mt-5 ">
        <h3 class="mb-3 text-dark"><?php echo $pageName; ?></h3>
        <div class="card shadow">
            <div class="card-header py-3">
                <p class="text-primary m-0 font-weight-bold">Search for Study</p>
            </div>
            <!-- Start of Card Body -->
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group md-form form-sm form-1 pl-0">
                                <div class="input-group-prepend">
                                    <button class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search black"></i></button>
                                </div>
                                <input class="form-control my-0 py-1" type="text" placeholder="Title Search" name="title">
                            </div>
                        </div>
                        <div class="col-md-2 py-1">
                                <select class="form-control form-control-sm custom-select custom-select-sm" name="school">
                                    <option value="" selected>All Schools</option>
                                    <?php foreach ($schools as $school) :?>
                                        <option <?php returnStrTrue($search["school"],$school["id"],"selected"); ?> value="<?php echo $school["id"];?>"><?php echo $school["name"];?></option>
                                    <?php endforeach; ?>
                                </select>
                        </div>
                        <div class="col-md-2 py-1">
                                <select class="form-control form-control-sm custom-select custom-select-sm" name="course">
                                    <option value="" selected>All Course</option>
                                    <?php foreach ($courses as $course) :?>
                                        <option <?php returnStrTrue($search["course"],$course["id"],"selected"); ?> value="<?php echo $course["id"];?>"><?php echo $course["name"];?></option>
                                    <?php endforeach; ?>
                                </select>
                        </div>   
                    </div>
                </form>            
                <!-- Start of Table Content -->
                <table class="table  mt-3">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>School</th>
                            <th>Course</th>
                            <?php if(checkUser(NORM,MOD,ADMIN)):?>
                            <th>Action</th>
                            <?php endif?>
                        </tr>
                    </thead>
                    <tbody id="result">
        
                    </tbody>
                </table>
                <!-- End of Table Content -->
                <!-- Start of pagination -->
                <div id="pagination" >
                    <a id="btn_prev" onclick="PrevPage()" class="btn btn-primary"href="#">Prev</a>
                    <a id="btn_next" onclick="NextPage()" class="btn btn-primary"href="#">Next</a>
                    <p id="page_span" >Page 1 of 5</p>
                </div>
                
                <!-- end of pagination -->
            </div>
            <!-- end of card body -->
        </div>
    </div>

    <script src="assets/js/pagination.js"></script>

<?php 
    $SchoolController = NULL; // to close sql connection
    include_once("footer.php");
?>
