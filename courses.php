<?php 
    include_once("header.php");
    
?>
    <div class="w-75 mx-auto mt-5">
        <h3 class="mb-3 text-dark"><?php echo $pageName; ?></h3>
        <div class="card shadow">
            <?php 
                if(isset($_GET["option"])){
                    if($_GET["option"] == 1){
                        include_once("php/pages-includes/new-course.php");
                    }else{
                        include_once("php/pages-includes/edit-course.php");
                    }
                }else{
                    include_once("php/pages-includes/display-courses.php");
                }
            ?>
        </div>
    </div>

<?php 
    include_once("footer.php");
?>
