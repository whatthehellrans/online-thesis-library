<?php 
    session_start();
    include_once("php/functions/misc.php");

    //Check if already logged in. then redirect to home page.
    checkIfAlreadyLogin();

    include_once("php/functions/Query/SchoolController.php");
    $SchoolController = new SchoolController();
    $schools = $SchoolController->FetchAllSchool();
    $SchoolController = NULL; // close sql connection

    $errors = array("username"=>"","password"=>"","fname"=>"","lname"=>"","email"=>"","school"=>"","invalid"=>FALSE);
    $input  = array("username"=>"","password"=>"","fname"=>"","lname"=>"","email"=>"","school"=>"");
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        include_once("php/functions/Query/AccountController.php");
        $AccountController = new AccountController();

        $input["username"]  = cleanInput($_POST["username"]);
        $input["password"]  = cleanInput($_POST["password"]);
        $input["fname"]  = cleanInput($_POST["fname"]);
        $input["lname"]  = cleanInput($_POST["lname"]);
        $input["email"]  = cleanInput($_POST["email"]);
        $input["access"]  = NORM;
        $input["school"]  = cleanInput($_POST["school"]);

        $errors["username"] = checkEmpty($input["username"],"Username");
        $errors["password"] = checkEmpty($input["password"],"Password");
        $errors["fname"]    = checkEmpty($input["fname"],"First name");
        $errors["lname"]    = checkEmpty($input["lname"],"Last name");
        $errors["fname"]    = checkHasDigits($input["fname"],"First name");
        $errors["lname"]    = checkHasDigits($input["lname"],"Last name");
        $errors["email"]    = checkEmpty($input["email"],"Email");
        $errors["school"]   = checkEmpty($input["school"],"School");
        
        if($AccountController->CheckEmailDuplicate($input["email"]))
        {
            $errors["email"] = "Email is already used.";
        }


        //Check all Errors (If there is any)
        $validated = true;
        foreach ($errors as $error) {
            if($error != ""){
                $validated = false;
            }
        }

        if($validated){
            
            if($AccountController->InsertAccount($input["username"], $input["password"],$input["fname"],$input["lname"],$input["email"], $input["access"], $input["school"])){
                $account = $AccountController->FetchLoginAccount($input["username"],$input["password"]);
                if($account["id"] != NULL){
                    //Set Session
                    $_SESSION["user_id"]  = $account["id"];
                    $_SESSION["username"] = $account["username"];
                    $_SESSION["fname"]    = $account["fname"];
                    $_SESSION["lname"]    = $account["lname"];
                    $_SESSION["access"]   = $account["access"];
                    $_SESSION["school"]   = $account["school"];

                    include_once("php/functions/Query/LogController.php");
                    $LogController = new LogController();
                    $LogController->InsertLogUser($_SESSION["user_id"],"register");

                    header("Location: index.php");
                    exit();
                }else{
                    $errors["invalid"] = TRUE;
                }
            }else{
                $errors["username"] = "Username already exists.";
            }
 
        }
        
    }
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Register - Online Thesis Library</title>
    <meta name="description" content="A collection of Thesis">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/Nunito.css">
    <link rel="stylesheet" href="assets/font-awesome/css/all.css">
</head>

<body class="bg-gray-900">
    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-md-9 col-lg-12 col-xl-10 mt-5">
                <div class="card shadow-lg o-hidden border-0 my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-flex">
                                <div class="flex-grow-1 bg-login-image" style="background-image: url(&quot;assets/img/login.jpg&quot;);"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <?php if($errors["invalid"]): ?>
                                        <div class="bg-danger py-2 rounded text-white mb-3">
                                            Invalid Username or Password
                                        </div>
                                        <?php endif ?>
                                        <h4 class="text-dark mb-5">Register a new Account</h4>
                                        
                                    </div>
                                    <form class="user" action="" method="post">
                                        <?php returnErrorMsg($errors["username"]); ?>
                                        <div class="form-group"><input class="form-control form-control-user" type="text" id="username"  placeholder="Username" name="username"  value="<?php echo $input["username"];?>"></div>
                                        <?php returnErrorMsg($errors["password"]); ?>
                                        <div class="form-group"><input class="form-control form-control-user" type="password" id="password" placeholder="Password"  name="password"></div>
                                        <?php returnErrorMsg($errors["fname"]); ?>
                                        <div class="form-group"><input class="form-control form-control-user" type="text" id="fname" placeholder="First name" name="fname"  value="<?php echo $input["fname"];?>"></div>
                                        <?php returnErrorMsg($errors["lname"]); ?>
                                        <div class="form-group"><input class="form-control form-control-user" type="text" id="lname" placeholder="Last name" name="lname"  value="<?php echo $input["lname"];?>"></div>
                                        <?php returnErrorMsg($errors["email"]); ?>
                                        <div class="form-group"><input class="form-control form-control-user" type="email" id="email" placeholder="Email" name="email"  value="<?php echo $input["email"];?>"></div>
                                        <?php returnErrorMsg($errors["school"]); ?>
                                        <div class="form-group">
                                            <small>School</small>
                                            <select class="form-control " name="school" id="school">
                                            <?php foreach ($schools as $school) :?>
                                                <option <?php returnStrTrue($input["school"], $school["id"], "selected"); ?> value="<?php echo $school["id"];?>"><?php echo $school["name"]?></option>
                                            <?php endforeach ?>
                                            </select>
                                        </div>
                                        
                                        <button class="btn btn-primary btn-block text-white btn-user mt-5" type="submit">Submit</button>
                                    </form>
                                    <div class="mt-1">
                                    Already have an account?<a href="login.php"> Login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/jquery.easing.js"></script>
    <script src="assets/js/script.min.js"></script>
</body>

</html>