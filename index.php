<?php 

	include_once("header.php");
    include_once("php/functions/misc.php");

    include_once("php/functions/Query/SchoolController.php");
    $SchoolController   = new SchoolController();
	$schools    = $SchoolController->FetchAllSchool();
	
?>


	<div class="pt-5 pb-3 bg-primary"  >
		<div class="jumbotron container text-center">
			<div id="carouselExampleIndicators" class="carousel slide " data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item active">
					<img class="d-block w-100 rounded" style="height: 600px;" src="assets/img/slider/ccdi.jpg" alt="First slide">
					</div>
					<div class="carousel-item">
					<img class="d-block w-100" style="height: 600px;" src="assets/img/slider/001.jpg" alt="Second slide">
					</div>
					<div class="carousel-item">
					<img class="d-block w-100" style="height: 600px;" src="assets/img/slider/002.jpg" alt="Third slide">
					</div>
				</div>
				<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</div>
	


	<div class="container my-3">
		<div class="row">
			<?php foreach ($schools as $school) : ?>
			<div class="col-sm-4 mx-auto my-3">
				<div class="card-deck">
					<div class="card text-white" style="background: black;" >
						<img class="card-img-top" style="min-height: 150px; max-height:150px; max-width:150px; margin:10px auto;" src="assets/img/schools/<?php echo $school["id"];?>.jpg" alt="Card image cap">
						<div class="card-body">
							<h5 class="card-title"><?php echo $school["name"];?></h5>
							<p class="card-text"><?php echo $school["address"];?><br>
							<small><?php echo $school["contact"]; ?></small>
							</p>
						</div>
						<div class="card-footer" style="background: black;" >
							<small class="text-muted"><a href="search.php?title=&school=<?php echo $school["id"]; ?>&course=">Show all research study from this school</a> </small>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach ?>
		</div>
	</div>
	


<?php
	$SchoolController = NULL; // to close sql connection
	include_once("footer.php");
?>
