let current_page = 1;
const record_per_page = 10;

function PrevPage() {
    if (current_page > 1) {
        current_page--;
        UpdateTable(current_page);
    }
}

function NextPage() {
    if (current_page < TotalPages()) {
        current_page++;
        UpdateTable(current_page);
    }
}

function UpdateTable(page) {
    let result = document.getElementById("result");
    //validate page number
    if (page < 1) {
        page = 1;
    }
    if (page > TotalPages()) {
        page = TotalPages();
    }

    ClearResultTable(result);
    GenerateDataTable(result);
    UpdatePageSpan();
}

function GenerateDataTable(result) {
    let i = (current_page - 1) * record_per_page;

    for (i; i < current_page * record_per_page && i < items.length; i++) {
        let tr = document.createElement("tr");

        for (const item in items[i]) {
            if (items[i].hasOwnProperty(item)) {
                const element = items[i][item];
                let txtnode = document.createTextNode(element);
                let tdtag = document.createElement("td");

                tdtag.appendChild(txtnode);
                tr.appendChild(tdtag);
            }
        }
        //View Link
        let actionTd = tr.lastChild;
        let thesis_id = links[i].thesis_id;
        let viewlinktxtnode = document.createTextNode("View");
        let viewlinkElement = document.createElement("a");

        actionTd.innerHTML = "";

        viewlinkElement.appendChild(viewlinktxtnode);
        if (items[i].allow_view) {
            actionTd.appendChild(viewlinkElement);
        }
        viewlinkElement.href = `view-thesis.php?id=${thesis_id}`;

        //Edit and Delete
        let allow_delete = links[i].allow_delete;
        let allow_edit = links[i].allow_edit;
        if (allow_edit) {
            InsertSpanDivider(actionTd);
            let editTxtNode = document.createTextNode("Edit");
            let editElement = document.createElement("a");
            editElement.appendChild(editTxtNode);
            actionTd.appendChild(editElement);
            editElement.href = `edit-thesis.php?edit=${thesis_id}`;
        }
        if (allow_delete) {
            InsertSpanDivider(actionTd);
            let delTxtNode = document.createTextNode("Delete");
            let delElement = document.createElement("a");
            delElement.appendChild(delTxtNode);
            actionTd.appendChild(delElement);
            delElement.href = `search.php?delete=${thesis_id}`;
            delElement.addEventListener("click", confirmDelete);
        }

        //Set class for Title
        tr.children[0].classList += "font-weight-bold custom-txtcolorhover-4";

        result.appendChild(tr);
    }
}

function confirmDelete() {
    confirm("Are you sure you want to delete");
}

function InsertSpanDivider(parentToAppend) {
    let spanTxtNode = document.createTextNode(" | ");
    let spanElement = document.createElement("span");
    spanElement.appendChild(spanTxtNode);
    parentToAppend.appendChild(spanElement);
}

function ClearResultTable(result) {
    result.innerHTML = "";
}

function UpdatePageSpan() {
    let btn_next = document.getElementById("btn_next");
    let btn_prev = document.getElementById("btn_prev");
    let page_span = document.getElementById("page_span");


    page_span.innerHTML = `Page ${current_page} of ${ (TotalPages() == 0) ? "1":TotalPages()  }`;

    if (current_page == 1) {
        btn_prev.classList.add("disabled");
    } else {
        btn_prev.classList.remove("disabled");
    }
    if (current_page == TotalPages()) {
        btn_next.classList.add("disabled");
    } else {
        btn_next.classList.remove("disabled");
    }

    if (TotalPages() == 0 )
    {
        btn_next.classList.add("disabled");
    }
}

function TotalPages() {
    return Math.ceil(items.length / record_per_page);
}

window.onload = function() {
    UpdateTable(1);
};
