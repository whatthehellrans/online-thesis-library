-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2020 at 12:38 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thesis_library`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `message_id` int(11) NOT NULL,
  `meeting_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `abbr` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `full_name`, `abbr`) VALUES
(500, 'Bachelor of Science in Computer Science', 'BSCS'),
(501, 'Bachelor of Science in Information Technology', 'BSIT'),
(502, 'Bachelor of science and information system', 'BSIS'),
(503, 'Bachelor of Science Information System', 'BSIS');

-- --------------------------------------------------------

--
-- Table structure for table `log_search`
--

CREATE TABLE `log_search` (
  `user_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `thesis_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_search`
--

INSERT INTO `log_search` (`user_id`, `time`, `thesis_id`) VALUES
(100, '2019-12-18 05:45:50', 1),
(100, '2019-12-18 05:53:38', 3),
(100, '2019-12-18 05:53:53', 2),
(101, '2019-12-18 06:08:26', 5),
(101, '2019-12-18 06:08:36', 4),
(101, '2019-12-18 06:08:59', 1),
(100, '2019-12-18 16:17:00', 4),
(100, '2019-12-18 16:28:16', 5),
(100, '2019-12-19 02:22:49', 6),
(100, '2019-12-19 02:24:19', 7),
(100, '2019-12-19 03:37:19', 7),
(100, '2019-12-19 03:37:42', 7),
(102, '2019-12-20 04:22:18', 7),
(102, '2019-12-20 04:22:35', 1),
(102, '2019-12-20 04:22:44', 5),
(102, '2019-12-20 04:29:41', 5),
(100, '2020-01-15 03:26:09', 7),
(107, '2020-01-15 03:37:12', 3),
(107, '2020-01-15 03:47:43', 7),
(107, '2020-01-15 03:48:01', 7),
(100, '2020-01-15 04:00:29', 7),
(100, '2020-01-15 04:01:44', 6),
(100, '2020-01-19 02:18:40', 7),
(100, '2020-01-19 02:45:44', 12),
(100, '2020-01-19 02:52:13', 7),
(100, '2020-01-19 02:54:25', 7),
(100, '2020-01-21 03:36:10', 43),
(105, '2020-01-21 03:53:56', 24),
(104, '2020-01-21 05:50:51', 43),
(100, '2020-01-23 13:50:13', 50),
(100, '2020-01-23 14:07:46', 29),
(102, '2020-01-31 00:40:35', 44),
(100, '2020-02-02 02:16:06', 57),
(100, '2020-02-02 02:17:29', 23),
(104, '2020-02-02 02:52:19', 59),
(104, '2020-02-02 02:52:47', 58),
(111, '2020-02-07 07:47:37', 57),
(100, '2020-02-12 06:49:16', 59),
(100, '2020-02-12 06:49:26', 58);

-- --------------------------------------------------------

--
-- Table structure for table `log_study`
--

CREATE TABLE `log_study` (
  `thesis_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` enum('add','modify','remove') NOT NULL DEFAULT 'modify'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_study`
--

INSERT INTO `log_study` (`thesis_id`, `user_id`, `time`, `type`) VALUES
(1, 100, '2019-12-18 05:45:36', 'add'),
(2, 100, '2019-12-18 05:49:00', 'add'),
(3, 100, '2019-12-18 05:51:32', 'add'),
(3, 100, '2019-12-18 05:53:31', 'modify'),
(4, 100, '2019-12-18 05:56:15', 'add'),
(5, 100, '2019-12-18 05:59:11', 'add'),
(5, 100, '2019-12-18 16:16:46', 'modify'),
(4, 100, '2019-12-18 16:17:13', 'modify'),
(3, 100, '2019-12-18 16:17:36', 'modify'),
(2, 100, '2019-12-18 16:17:59', 'modify'),
(2, 100, '2019-12-18 16:18:35', 'modify'),
(6, 100, '2019-12-18 16:28:09', 'add'),
(7, 100, '2019-12-18 16:31:00', 'add'),
(7, 100, '2019-12-18 16:36:33', 'modify'),
(7, 100, '2019-12-18 16:36:57', 'modify'),
(8, 100, '2020-01-19 02:21:16', 'add'),
(9, 100, '2020-01-19 02:23:09', 'add'),
(10, 100, '2020-01-19 02:40:12', 'add'),
(11, 100, '2020-01-19 02:41:36', 'add'),
(12, 100, '2020-01-19 02:44:53', 'add'),
(11, 100, '2020-01-19 02:46:53', 'modify'),
(9, 100, '2020-01-19 02:47:36', 'modify'),
(8, 100, '2020-01-19 02:50:42', 'modify'),
(7, 100, '2020-01-19 02:53:53', 'modify'),
(13, 100, '2020-01-19 02:56:55', 'add'),
(14, 100, '2020-01-19 02:59:03', 'add'),
(14, 100, '2020-01-19 03:00:07', 'modify'),
(8, 100, '2020-01-19 03:00:52', 'modify'),
(9, 100, '2020-01-19 03:12:52', 'modify'),
(11, 100, '2020-01-19 03:14:23', 'modify'),
(15, 100, '2020-01-19 03:19:09', 'add'),
(16, 100, '2020-01-19 03:21:20', 'add'),
(17, 100, '2020-01-19 03:25:29', 'add'),
(17, 100, '2020-01-19 03:31:39', 'modify'),
(18, 100, '2020-01-19 03:36:23', 'add'),
(19, 100, '2020-01-19 03:38:27', 'add'),
(20, 100, '2020-01-19 03:39:59', 'add'),
(21, 100, '2020-01-19 03:42:09', 'add'),
(22, 100, '2020-01-19 03:47:07', 'add'),
(23, 100, '2020-01-19 03:50:05', 'add'),
(24, 100, '2020-01-19 03:52:29', 'add'),
(25, 100, '2020-01-19 03:55:21', 'add'),
(26, 100, '2020-01-19 03:56:54', 'add'),
(27, 100, '2020-01-19 03:59:39', 'add'),
(28, 100, '2020-01-19 04:05:10', 'add'),
(29, 100, '2020-01-19 04:07:43', 'add'),
(5, 100, '2020-01-19 04:09:26', 'modify'),
(3, 100, '2020-01-19 04:09:48', 'modify'),
(2, 100, '2020-01-19 04:09:58', 'modify'),
(30, 100, '2020-01-19 04:14:33', 'add'),
(30, 100, '2020-01-19 04:21:01', 'modify'),
(31, 100, '2020-01-19 04:25:05', 'add'),
(32, 100, '2020-01-19 04:27:20', 'add'),
(33, 100, '2020-01-19 04:29:54', 'add'),
(34, 100, '2020-01-19 04:33:33', 'add'),
(35, 100, '2020-01-19 04:36:21', 'add'),
(36, 100, '2020-01-19 04:38:08', 'add'),
(37, 100, '2020-01-19 04:40:21', 'add'),
(38, 100, '2020-01-19 04:42:25', 'add'),
(30, 100, '2020-01-19 04:44:54', 'modify'),
(39, 100, '2020-01-19 04:55:44', 'add'),
(40, 100, '2020-01-19 05:02:44', 'add'),
(41, 100, '2020-01-19 05:06:33', 'add'),
(42, 100, '2020-01-19 05:08:32', 'add'),
(43, 100, '2020-01-19 05:10:09', 'add'),
(44, 100, '2020-01-20 05:55:30', 'add'),
(40, 100, '2020-01-20 06:31:08', 'modify'),
(40, 100, '2020-01-20 06:31:27', 'modify'),
(44, 100, '2020-01-23 13:15:09', 'add'),
(45, 100, '2020-01-23 13:21:01', 'add'),
(46, 100, '2020-01-23 13:26:32', 'add'),
(47, 100, '2020-01-23 13:34:25', 'add'),
(48, 100, '2020-01-23 13:38:37', 'add'),
(49, 100, '2020-01-23 13:44:04', 'add'),
(50, 100, '2020-01-23 13:49:24', 'add'),
(50, 100, '2020-01-23 13:51:16', 'modify'),
(51, 100, '2020-01-23 13:54:21', 'add'),
(52, 100, '2020-01-23 14:01:04', 'add'),
(53, 100, '2020-01-23 14:03:06', 'add'),
(44, 102, '2020-01-31 00:41:03', 'modify'),
(54, 100, '2020-02-01 12:51:42', 'add'),
(55, 100, '2020-02-01 12:55:34', 'add'),
(54, 100, '2020-02-01 12:56:46', 'modify'),
(56, 100, '2020-02-01 12:58:51', 'add'),
(57, 100, '2020-02-01 13:03:01', 'add'),
(57, 100, '2020-02-01 13:04:58', 'modify'),
(58, 100, '2020-02-01 13:07:54', 'add'),
(59, 100, '2020-02-02 02:28:55', 'add');

-- --------------------------------------------------------

--
-- Table structure for table `log_user`
--

CREATE TABLE `log_user` (
  `user_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` enum('login','register') NOT NULL DEFAULT 'login'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_user`
--

INSERT INTO `log_user` (`user_id`, `time`, `type`) VALUES
(100, '2019-12-18 03:02:43', 'login'),
(100, '2019-12-18 03:17:13', 'login'),
(100, '2019-12-18 03:33:07', 'login'),
(100, '2019-12-18 03:33:56', 'login'),
(100, '2019-12-18 03:51:24', 'login'),
(100, '2019-12-18 04:12:22', 'login'),
(100, '2019-12-18 04:36:37', 'login'),
(101, '2019-12-18 05:29:57', 'login'),
(100, '2019-12-18 05:38:00', 'login'),
(101, '2019-12-18 06:01:02', 'login'),
(100, '2019-12-18 06:06:18', 'login'),
(101, '2019-12-18 06:08:19', 'login'),
(100, '2019-12-18 06:09:15', 'login'),
(103, '2019-12-18 06:14:43', 'login'),
(100, '2019-12-18 06:16:27', 'login'),
(100, '2019-12-18 16:11:26', 'login'),
(100, '2019-12-19 01:41:38', 'login'),
(100, '2019-12-19 02:14:19', 'login'),
(100, '2019-12-19 02:46:12', 'login'),
(100, '2019-12-19 02:54:14', 'login'),
(100, '2019-12-19 02:55:16', 'login'),
(100, '2019-12-19 03:23:17', 'login'),
(100, '2019-12-19 03:37:03', 'login'),
(100, '2019-12-19 05:55:03', 'login'),
(100, '2019-12-20 04:17:35', 'login'),
(102, '2019-12-20 04:21:12', 'login'),
(100, '2019-12-20 04:30:08', 'login'),
(100, '2019-12-20 05:56:27', 'login'),
(100, '2019-12-20 05:58:05', 'login'),
(100, '2019-12-20 06:00:17', 'login'),
(100, '2019-12-30 05:06:42', 'login'),
(100, '2020-01-03 12:39:33', 'login'),
(100, '2020-01-13 02:19:34', 'login'),
(100, '2020-01-13 02:22:31', 'login'),
(102, '2020-01-13 02:26:10', 'login'),
(100, '2020-01-13 02:26:28', 'login'),
(100, '2020-01-13 02:46:16', 'login'),
(102, '2020-01-13 02:51:00', 'login'),
(100, '2020-01-14 02:45:45', 'login'),
(100, '2020-01-14 02:51:06', 'login'),
(100, '2020-01-14 02:54:52', 'login'),
(100, '2020-01-14 02:59:08', 'login'),
(100, '2020-01-14 03:06:13', 'login'),
(100, '2020-01-14 03:12:01', 'login'),
(100, '2020-01-14 03:16:25', 'login'),
(100, '2020-01-14 03:26:44', 'login'),
(102, '2020-01-14 03:27:07', 'login'),
(100, '2020-01-14 03:31:16', 'login'),
(104, '2020-01-14 03:31:34', 'login'),
(100, '2020-01-14 03:51:48', 'login'),
(100, '2020-01-14 03:52:04', 'login'),
(102, '2020-01-14 03:58:01', 'login'),
(100, '2020-01-14 03:59:47', 'login'),
(105, '2020-01-14 04:00:40', 'login'),
(105, '2020-01-14 04:02:02', 'login'),
(100, '2020-01-15 03:25:59', 'login'),
(107, '2020-01-15 03:34:46', 'register'),
(107, '2020-01-15 03:46:45', 'login'),
(100, '2020-01-15 03:50:02', 'login'),
(100, '2020-01-15 04:29:57', 'login'),
(100, '2020-01-15 05:17:07', 'login'),
(104, '2020-01-15 05:17:40', 'login'),
(102, '2020-01-15 05:18:27', 'login'),
(100, '2020-01-15 05:18:39', 'login'),
(108, '2020-01-15 05:20:02', 'register'),
(100, '2020-01-15 05:21:51', 'login'),
(102, '2020-01-15 05:23:00', 'login'),
(108, '2020-01-15 05:23:55', 'login'),
(108, '2020-01-15 05:24:24', 'login'),
(108, '2020-01-15 05:26:52', 'login'),
(100, '2020-01-15 05:31:46', 'login'),
(104, '2020-01-15 08:09:08', 'login'),
(104, '2020-01-15 08:10:36', 'login'),
(102, '2020-01-15 08:11:15', 'login'),
(100, '2020-01-18 05:59:30', 'login'),
(100, '2020-01-18 06:49:32', 'login'),
(100, '2020-01-18 06:57:52', 'login'),
(100, '2020-01-18 07:12:24', 'login'),
(100, '2020-01-18 07:14:20', 'login'),
(100, '2020-01-18 07:16:23', 'login'),
(100, '2020-01-19 02:15:53', 'login'),
(100, '2020-01-19 02:37:57', 'login'),
(100, '2020-01-19 08:13:41', 'login'),
(100, '2020-01-20 03:53:15', 'login'),
(100, '2020-01-20 04:05:18', 'login'),
(100, '2020-01-20 04:19:18', 'login'),
(100, '2020-01-20 04:44:36', 'login'),
(100, '2020-01-20 05:50:30', 'login'),
(100, '2020-01-20 06:29:14', 'login'),
(100, '2020-01-21 03:29:22', 'login'),
(102, '2020-01-21 03:50:44', 'login'),
(100, '2020-01-21 03:52:46', 'login'),
(105, '2020-01-21 03:53:08', 'login'),
(100, '2020-01-21 03:55:12', 'login'),
(104, '2020-01-21 05:49:58', 'login'),
(105, '2020-01-21 05:52:40', 'login'),
(100, '2020-01-21 05:53:43', 'login'),
(100, '2020-01-21 06:36:08', 'login'),
(100, '2020-01-21 06:37:46', 'login'),
(105, '2020-01-21 06:41:13', 'login'),
(100, '2020-01-22 01:59:08', 'login'),
(102, '2020-01-22 02:42:30', 'login'),
(101, '2020-01-22 02:48:53', 'login'),
(100, '2020-01-22 03:49:45', 'login'),
(102, '2020-01-22 03:53:21', 'login'),
(101, '2020-01-22 03:55:13', 'login'),
(100, '2020-01-22 05:32:42', 'login'),
(102, '2020-01-22 05:36:49', 'login'),
(100, '2020-01-23 13:10:21', 'login'),
(105, '2020-01-23 14:33:51', 'login'),
(100, '2020-01-23 14:35:01', 'login'),
(105, '2020-01-23 14:37:26', 'login'),
(100, '2020-01-23 14:37:54', 'login'),
(105, '2020-01-23 14:40:01', 'login'),
(100, '2020-01-23 14:41:39', 'login'),
(109, '2020-01-23 14:48:13', 'login'),
(100, '2020-01-23 14:53:16', 'login'),
(105, '2020-01-23 14:55:21', 'login'),
(100, '2020-01-23 15:05:02', 'login'),
(100, '2020-01-24 03:13:33', 'login'),
(100, '2020-01-24 03:18:04', 'login'),
(100, '2020-01-31 00:34:02', 'login'),
(102, '2020-01-31 00:34:44', 'login'),
(100, '2020-01-31 09:05:07', 'login'),
(100, '2020-01-31 09:08:07', 'login'),
(100, '2020-01-31 09:09:01', 'login'),
(105, '2020-01-31 09:17:23', 'login'),
(105, '2020-01-31 09:19:26', 'login'),
(104, '2020-01-31 09:27:41', 'login'),
(100, '2020-02-01 04:46:19', 'login'),
(103, '2020-02-01 05:50:21', 'login'),
(100, '2020-02-01 10:46:51', 'login'),
(100, '2020-02-01 12:43:45', 'login'),
(100, '2020-02-02 01:22:01', 'login'),
(100, '2020-02-02 02:14:16', 'login'),
(101, '2020-02-02 02:22:20', 'login'),
(100, '2020-02-02 02:25:26', 'login'),
(100, '2020-02-02 02:38:51', 'login'),
(104, '2020-02-02 02:52:00', 'login'),
(100, '2020-02-02 02:54:43', 'login'),
(104, '2020-02-02 02:59:54', 'login'),
(100, '2020-02-07 07:21:41', 'login'),
(100, '2020-02-07 07:22:27', 'login'),
(105, '2020-02-07 07:37:49', 'login'),
(100, '2020-02-07 07:40:15', 'login'),
(105, '2020-02-07 07:43:47', 'login'),
(111, '2020-02-07 07:47:23', 'register'),
(100, '2020-02-07 07:50:12', 'login'),
(100, '2020-02-07 08:10:39', 'login'),
(100, '2020-02-07 08:20:12', 'login'),
(100, '2020-02-08 05:47:09', 'login'),
(100, '2020-02-08 07:19:40', 'login'),
(100, '2020-02-08 07:52:06', 'login'),
(100, '2020-02-08 07:57:21', 'login'),
(100, '2020-02-12 06:48:16', 'login'),
(100, '2020-02-12 06:53:07', 'login'),
(100, '2020-02-12 11:09:51', 'login'),
(113, '2020-02-12 11:34:19', 'register');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `thesis_involved` int(11) NOT NULL,
  `message` varchar(7500) NOT NULL,
  `sender_name` varchar(50) NOT NULL,
  `sender_contact` varchar(24) NOT NULL,
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Unread','Read') NOT NULL DEFAULT 'Unread',
  `response` enum('Accepted','Declined','Unset') NOT NULL DEFAULT 'Unset'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE `schools` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(5000) NOT NULL,
  `contact` varchar(24) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`id`, `name`, `address`, `contact`) VALUES
(1000, 'CCDI', 'Sikatuna St., Old Albay, Legazpi City', '639123456791'),
(1001, 'CAT College', 'Balintawak St., Old Albay, Legazpi City', '639907708664'),
(1002, 'SLTCFI', 'Ramon Santos Street, Barangay 33, Penaranda, Legazpi, Albay', '639905742182');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `thesis_id` int(11) NOT NULL,
  `tag` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `thesis`
--

CREATE TABLE `thesis` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `abstract` varchar(15000) NOT NULL,
  `published_date` date NOT NULL,
  `school` int(11) NOT NULL,
  `course` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `thesis_pending` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `abstract` varchar(15000) NOT NULL,
  `published_date` date NOT NULL,
  `school` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `date_submitted` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Thesis_fk0` (`school`),
  KEY `Thesis_fk1` (`course`)
) AUTO_INCREMENT=20000;


CREATE TABLE schools_courses(
	school_id int(11),
    course_id int(11)
);
--
-- Dumping data for table `thesis`
--

INSERT INTO `thesis` (`id`, `title`, `author`, `abstract`, `published_date`, `school`, `course`) VALUES
(1, 'Provincial General Office Tracking', 'Wawie O. Llaneta, Rex Paolo D. Capitulo, Alvino R. Asejo and D. Arizapa', 'The PGSO envinsions an effective, efficient and sustainable program on administration and local governance that are already in plcae which operates on optimum budget with adequete stocks readily available and competent manpower that are responsive to the needs of the clients with the right quality of supplies and general services at the right time at the right plcae.\r\n	\r\n	Tracking and inventory system was design and provided with a convenient, easy to use and a user friendly system. Provincial General Services Office was still using the manual process. The primary proponent of the system was to help the employees to reduce their workload and easily manage the documents and reports. It will help the inveronment by minimizing the consumption of paper.', '2018-03-01', 1000, 500),
(2, 'Online Loan Transaction System of Sunheir Sending Corporation', 'Vina Moral, Mary Ann Marticio, Sumitha Joy Perera, and Pearl Bonde', 'Sunheir Lending Corporation is a family corporation that is located at Bario St., Old Albay, Legazpi City. Their primary offered is loans service that manually operated. Their process in applying a loan takes days to be processed.\r\n	\r\n	The objective of this study are: to determine the current system they are using; to identify the problems they encountered; and to develop an Online Loan Transaction System in terms of: registration for new clients, storage and retrieval of client loan records, computation of loan payments, loan paymment record, and list of payments report.\r\n\r\n	Online Lon Transaction System was designed to improve the operation and process data rapidly, accurently and reliably. This would lessen the work of the administrator in checking the registration of the clients. The system would provide an efficient way of record keeping activity. It is maintains the daily record of client\'s payment. The proposed system wants to help the company to be innovated involving the use of computerization. It would become more organize and productive system of procedures in the future. The system would make the transaction between employees and the clients easier.', '2017-03-01', 1000, 501),
(3, 'Enrollment System of Bogtong Elementary School', 'Diana H. Llamoso, Wenilyn N. Herrera, Elsan C. Arimbay and Phil Arjay B. Gomez', 'Bogtong Elementary School is one of the prestigious public schools in Legazpi City. It is located near Sagrada Familia Church witn approximately seven hundred(700) enrollees per year from kinder to grade six. Established in 1948, and now it has a total of eighteen (18) classrooms, consist of two (2) room for kinder, three (3) rooms for grades one and two, two (2) rooms for grade three, two (2) rooms for grade four, two (2) rooms for grade five, two (2) rooms for grade six, one(1) room for principal\'s office, and one(1) rooms for the library. During report generation of the master list of all the enrolled pupils; the teacher manually input all the information such as name, age, address, grade and section manage of the pupils of the name of the adviser from the bond paper to MS Excel Making the process long redundant, hard accessibilityof data the consumming and posible loss of data or records.\r\n\r\n	The proponent system entitled Enrollment System of Bogtong Elementary School would be designed to simplify the task of the users, with fast, accurate hassle from the making the records safe and secure, that would absolutely lessen the work of the teachers especially during enrolment period. It can also retrieve important information of the pupils record in terms of storage and retrieval of grades. It can also generate report such us the list of enrolees per section at the end of school year. This would also enable the school to be more competitive and be an track in today\'s demand. Bogtong Elementary School aims to give brighter future to all their pupil, with this system, the school would be more updated like the other schools here in legazpi.\r\n\r\n	The system provides the client\'s unique, innovative and easy to use interface to improve the way people use the technology today. By using this system, the principal would be having a full access to the system and can easily get the needed information, such as the total number of enrolees from kinder garden to grade six, and the exact number of pupils in each section; and the grade that has been submitted by the teachers.\r\n	However, Bogtong Elementary School Still adopts the current enrollment is on-going, the teacher writes down all the information on a bond paper manually by writing it in bond paper. To be able to enroll they need to pass the assessment conducted by the teachers and by that the pupils together with their parents fall in line for assessment.', '2017-03-01', 1000, 500),
(4, 'Public Employment Services Office Online Summer Job Application with SMS notification', 'Jackielyn Anonuevo, Wenalyn M. Magayanes, Dan Michael Rubic, Jason Silencio, Valerie Kim Swee Talde.m', 'The Public Employment Service Office of Albay Capital. PESO have a program for student, the special program for Employment of Student (SPES) is a program being mandated by the Republic Act No. 7323, entitled &quot;An Act to help poor but Deserving student pursue their Education by Encouraging their Employment during Summer and/or Christmas vacations, through Incentives Granted to Employers, allowing them to pay only Sixty per centrum of their salaries or wages and the forty per centrum through Education vouchers to be paid by the Government, Prohibiting the filling of fraudulant or fictitous claims, and for the other purposes&quot;.\r\n\r\nThe objective of their study are: to determine the current system they are using; to identify the problems they encountered; and to develop on Online Summer Job Application System in terms of; registration for new applicant, keeping and retrieving of applicant record, SMS notification and report generation.\r\n\r\nPublic Employment Service Office Online Summer Job Application System was designed to improve the transaction and process of data rapidly, accurately and reliably. This would lessen the work of the personal in charge in checking the application of the applicants. The system would provide and effecient way of record keeping activity. It maintains the SPES record record secured, the proposed system wants to help the PESO Office to innovated involving the use or computerized and organized. The proposed system wants to easily update the students for the summer job approval. The system would make the transaction easily between the department head and applicant.', '2018-03-01', 1000, 500),
(5, 'Casa Basilica Online Information and Inventory System with SMS Notification', 'Jason R. Llanera, Michael B. Khemiani, John Lessier M. Lodor, Jonh Anthony Bangagnan, and Daryl B. Llanera', 'Casa Basilisa is a special interent resort located along the national road at Barangay road at Barangay San Rafael, Guinobatan, Albay Philippines. This Resort offers an ideal place for the tired and weary to rest and rejuvenate. Very unassuming from the outside, it has never failed to suprise. As one foreign guest excitely remarked on her mobile upon setting foot; &quot;Honey, come over. I found paradise!&quot; But they are also experiencing problems in acquiring request from clients via Facebook and Text Messages. Such as, when a client made a transaction they accidentally get wrong detail, specially the bank numbers and the wrong information of reservation dates.\r\n\r\nOnline Information and Inventory System with SMS Notification is designed to help the staff in managing their transaction process and complete against todays emerging. 	Through this system the staff can easily manage the gathered data upon the clients request and can even update employee\'s information of the company.', '2017-03-01', 1000, 501),
(6, 'Computer Arts and Technological College Class Scheduling System', 'Mhar B. Daniel, Jeric A. Millete', 'The world seems to be rapidly evolving towards modernization and continuously gearing up on becoming a technology-driven planet because of the changes of information technology offers. Information Technology (IT) infrastractures have already invaded various facets of our lives. It becomes a vital instrument in our activities. More and more establishments, institutions and organizations both public and private are instituting IT systems in operations to achieve better speed, accuracy and efficiency in the quality of their works. Obviously, the academe is one of the sectors that will be granted the appropriate information systems could benefit most from the modernization brought about by technology. Also, an information system that would eliminate the tedious task of manual scheduling or time tabling of the availability of the faculty, students and classrooms is one of the greatest contribution that the IT could impart in school especially in larger universities wherein there are vast amount of resources.', '2017-03-01', 1001, 500),
(8, 'DECA HOMES Online Information System', 'May B. Cuachin, Christian B. Primavera', 'A website is a collection of web pages, videos,images and other digital assets and hosted on a particular domain connected to the World Wide Web.\r\n   	 websites are use to deliver different kinds of information such as news,  weather reports and among others. Aside from this, it is also used by many \r\n	 companies as a marketing tool to promote  to advertise their products and services. The internet has been widely available and accessible,so most people \r\n         used this medium to obtain the necessary information they need rather than going to places  to personally gather such information.\r\n\r\n         with this new trend, our project focuses on the development of Online Information System for Deca Homes Corporation, A home developer company in the \r\n         Bicol Region. The site contains web pages that provide information about the company, including the company background, its various home design and \r\n	 others useful information linked to the company.\r\n   \r\n 	 the purpose of this Online Information System is to help the company in terms of of their marketing strategy and advertising company, which could\r\n	 potentially lead to improved profitability. This site will showcase its various home design to catch up the potential buyers and customer\'s attention.\r\n	\r\n	 The development of this website  is mostly written in codes through adobe Dreamweaver with supports different formatting language like javascript, jQuery\r\n	 cascading  Style sheet(CSS). We use MySQL database to build database structures, back up data,inspect status and work with data records. Content Management \r\n         System (CMS) allows publishing,editing,and modifying content as well as maintenance from central interface.', '2013-03-01', 1001, 500),
(9, 'Online Information System of Manhattan Beach Resort', 'Mary Grace A. Buban, Jennifer A. Del Mundo', 'An Online Information of Manhattan Beach Resort is a collection of information about the beach resorts.it is designed to help people gain knowledge \r\n     information and facts about the beach.Almost all filipinos love to swim and have fun with their love ones and friends in the beach especially during summer.\r\n     And we all know that a beach resorts is a place to spend holidays, for relaxation and recreation, where one can give dynamism to his leisure time.\r\n 	manhattan Beach resorts website can be use by anyone, for any purpose, it is vital for anyone searching for a good resort.most of the beach on the \r\n  internet today exist to advertise one particular beach. Manhattan beach Resort which offers a panoramic ocean view that is not only picture perfect  but also \r\n  relaxing alluring.Manhattan Beach Resort has complete amenities for anything from a simple picnic with the family to events such as intimate weddings, seminars \r\n  and conventions. the private pool for kids and adults is perfect for a refreshing dip on a hot summer day. Those listed on the site have welcomed the opportunity \r\n  to make them available id n this way.The purpose of this website is to give detailed information on how to recognized the real Manhattan Beach resort.', '2013-03-01', 1001, 501),
(10, 'Iglesia Watawat ng lahi Website', 'Rosemaria C.Mendoza Ronica Ll.Montero Jayrol C. Mujar', 'The Iglesia Watawat ng Lahi, Inc. was founded by Arsenio de Guzman on December 25,1936 at Lecheria Hill, Calamba, Laguna. it was purposely organized for\r\n        the filipinos to have a christian religios Seet of their own, independent of foreign domination.\r\n       \r\n	the seets believe in one Omnipotent God, in jesus christ, the son of God and Jose Rizal the Filipino Christ and messeger of God in the Far East.\r\n	The three(3)Golden Rules of the seet are:\r\n\r\n	1.Love of God above all\r\n	2.Love of fellowomen as one love himself\r\n	3.Love country \r\n        the supreme ruling body of the seet is the Board of Directors with 15 members. Similar to the supreme ruling body, a committee of 15 supervises the activities \r\n	in each  chapter in the defferent parts of the country. all religious rites and services of the seet are officiated by bishops and priests.\r\n    web based application is one of the most advance technologies being developed for the benefit of mankind.\r\n\r\n   this website launches  of the first ever web based information provider for the Iglesia Watawat ng Lahi (IWL) which contains several tabs that correspond to each \r\n   information that one is looking for. also, photos with insert captions are presented for more enjoyable web surfing with getting to know more about Iglesia\r\n    Watawat nang Lahi.', '2013-03-01', 1001, 501),
(11, 'An Online Information System of National Emblems of different countries', 'Reggie A.Abion, Arlene V. Arquero', 'The Online System of National Emblems of Different Countries is intended for viewing of emblems that is available in the site. the system is composed of a user and\r\n	an administrator. The system will provide information to users about the national emblems of different countries and they can copy it. Once a person visit the site \r\n	there will select a country by selecting one of the countries and categories in the list.', '2008-12-01', 1001, 501),
(12, 'Mission Montessori Child Center Website', 'John Gabriel H. Rivera, Catherine H. Ruiz', 'A website is a collection of related web pages containing images videos or other digital assets. It could be the work of an Individual a business or other \r\n	organization, and its typically Dedicated to some particular topic or purpose. any website can contain a hyperlink to any other website so the distinction \r\n        between	individual sites. as perceived by the user, may sometimes be blurred. A website is a depository of information destined for a public or private use \r\n	usually residing in a remote server. When a computer terminal calls the website (using the HTTP or HTTPS protocol) the server responds by sending the \r\n	information requested back to the user.\r\n\r\n	\r\n	Mission montessori child center website can be used by anyone, for any purpose; it is a vital that anyone searching for a good. Montessori school or teacher\r\n	training center be aware of this. Most of the Montessori sites on the internet today exist to advertise one particular Montessori organization, school or \r\n	training center. The Mission Montersorri child center website was created to provide detailed information in order to help in the search for a school or training\r\n	and all montessori training centers and organization who are official members of the MMCC share this specific details to be easy with others. those listed on this \r\n	site have welcomed the opportunity to make them available in this way. The purpose in the pages of this site is to give detailed information on how to recognize\r\n	real montessori.', '2012-03-01', 1001, 501),
(13, 'Embarcadero de legazpi Online information System', 'Charlaine E. Espirito Geian M. Alcazar', 'Embarcadero online information System Website is a system that provides information to the users, \r\n	where they can enjoy and experience the website of Embarcadero de Legazpi. this system highlights its content wherein it is rich in information yet, \r\n      accessible, consistent and easy to navigate. The goal is to grab the attention of the users for them to patronage the site. the store owners have their \r\n      own access in the website to post their products in their designated shop windows. This system will help the users to find where they can ease, fun, and \r\n      time in their shopping.', '2014-03-01', 1001, 501),
(15, 'Marvelous Floating Cottage Online Information System', 'Jun IV Demapitan, Jo anna Maria L. Miranda', 'Marvelous Floating Cottage is one of the best tourist attraction in bacacay that provide good services to those who enjoy island hopping,swimming and\r\n        other ocean adventures. This project will give the tourist and clients an overview of the company\'s amenities, routes and destinations and others given \r\n	that will satisfy them according to their needs.\r\n\r\n        this project focuses on the development of the marvelous floating cottage website.  this site contains web pages that contains information as well as photos \r\n	with corresponding captions to provide information concerning the marvelous floating cottage website.All information is designed for educational and informative\r\n	purposes only.', '2013-03-01', 1001, 501),
(16, 'Hotel victoria online Reservation System (HVORS)', 'Hotel victoria online Reservation System (HVORS)', 'Computer plays a very important role in our society. A computer is defined as a device that receives,processes,and presents data. so it is a fact that \r\n	computer are used as instruments for human beings to be able to build an application that is efficient and accurate.\r\n\r\n	The computer continued to have a profound impact not only in business and science but also the society in general.This will extremely useful in evaluating \r\n	the impact and understanding the processes that leads to even more advance technological developments. how computers will likely to affect our society in the \r\n	future.\r\n\r\n	Numerous hotel exist now a days and they compete with each other by means of media through advertisement in radio,magazines ,internet,televisions, newspapers \r\n	the like. in the USA and europe, hotels are passionate and attractive. many travelers visits that place to observe and experience the magnificence that those \r\n	hotels offer. when technology is applied on this certain business, not only domestic tourists can avail. enjoy and visit but also the foreign tourist through\r\n	online reservations hotels here in albay are very much competitive with those hotels in manila . Albay is one of the tourist attractions here in Bicol Region\r\n	so hotels are important  in bringing the local and foreign tourist and inventors as well and it also helps promote tourism .\r\n\r\n	the Online Hotels Reservation is also for the benefit of the people who manage and go to the places here in Albay province for gathering information needed.', '2016-03-01', 1001, 500),
(17, 'Causes And Effects of Pornography The ages (10-16)', 'Charles De Guia, john Hel Esplana, jeffrey A. Felia, Jayvan Imperial, Nilo Mesa, Zairah Diane Castuera, Erica mae L. Merjilla', 'Pornography is the depiction of act in a sensational manner so as to arouse a quick intense emotional reaction.pornography may also be used tolure children\r\n	into sexual activity and instruct them, since young children who view such material may be lead to believe that is an acceptable behavior.\r\n	pornography, when used in the proper places by the proper people is perfectly acceptable, but when is more easily excissable for children to view then it is \r\n	for them to research homework, them is wrong.\r\n\r\n	obscene literature photographs and paintings.intended to cause sexual excitement treating of obscene subjects in art literature as well. picture or writing\r\n	describing erotic behavior are intended to cause sexual excitement. without parental guidance, they don\'t know what they are watching is not appropriate for\r\n	their ages that may cause aggressiveness in sexual intercourse which may lead them to an idea to commit sexual activity.\r\n\r\n	watching pornography is not prohibited to the older ages but in the younger ages it is strongly prohibited.it is bad habit because first of all, it is wrong\r\n	In the eyes of the Lord. By simply watching it,you have already committed a sin/crime called adultery. making habit of it, may lead you to obsession that will \r\n	provoke you to commit sexual harassment. Our goal is to prevent the younger ages in watching pornography.', '2017-12-01', 1001, 500),
(18, 'The causes and Effects of the Modern Technology to the Selected Senior High School Students of Computer Arts and Technological College', 'Nichole M. Bayna , Krishiel Beato , Ray Ian Joseph Curaming, Joshua Pintal , Sven Kyle Nzareno , Antonio Macandog, William Olarita', 'Technology has become an integral part of the majority of our daily lives. as teens continue to become more immerced in media, many adults begun to wonder whether \r\n	or not this exposure to such a high amount of electronic media is good thing or not.on the other side, there is an argument that technology is preparing children \r\n	for the &quot;real world&quot; that they will have to encounter in the future.\r\n\r\n	Modern technology have a good and bad impact to the world and especially to the youth/teens As we become increasingly more releint and absorbed in technology,it \r\n	is not surprising that today\'s teens have become avid users as well.in order to fully understand the pros and the cons,how our decisions about technology use will\r\n	affect today\'s children as they develop.the importance of the modern technology in our daily lives is undeniable. this is due to the fact that in today\'s dynamic\r\n	world ,life without technology, our live wouldn\'t be just the usual walk in the park daily routine .technology is a huge contributor to the well-being of human kind.\r\n	just try to imagine how hard it would be to make it through the day without the simplest of technologies. some people may be confused on what exactly technology is? \r\n	technology is systematic study of study of the methods and techniques employed in industry,research,agriculture and commerce.teens have their own likes,dislikes,\r\n	curiosities,and needs that are not the same as their parents or teachers.as obvious as this may seem, we as designers of new technologies for children,sometimes \r\n	forget that young people are not &quot;just short adults&quot; but an entirely different user population with their own culture, norms, and complexities.', '2017-12-01', 1001, 501),
(19, 'Perceptions of the selected students of Computer Arts And Technological college Inc.Regarding Social Media Addiction', 'Joel M. Baclao Jr., Christian S. Bornella, Charlie L. Espiel, Renan Kim B. Racho,Alexis b.Aranel, Kazeleen Y.  Cardano, Bianhel T. Zepeda', 'As our world goes through modernazation, changes occur really big and because technology pgraded so much, social media isone the top list of our best buddy yet greatest\r\n	enemy. plenty of deffirent vlogs are showing up and so the social media addiction. &quot;social networking is ingeneered to be as a habbit forming as a crack cocain&quot;\r\n	Mike Elgan, according to him people has this feeling or urge to keep on browsing strolling or checking their account not just everyday butevery second.\r\n	Mike Elgan also stated that social media addiction is  just like alcohol and cigarettes it keeps you sturve on it once you taste it.social media helps us a lot,it help us \r\n	to make our paper works easier and our other stuffs too, it serves as our way of communication toour love once since it can reach far distance but like what they say \r\n	&quot;To much won\'t bring any good&quot; and so does addiction came up.it doesn\'t mean that social media could always brought advantages to us people, it also brings danger that\r\n	could harm not just us but the community too. It leads us to plenty of illnesses and sometimes because of it not just several times we heard people ending their life.', '2018-03-01', 1001, 501),
(20, 'An online System of Albay Breeding Station', 'Jowelyn B. Guiriba , Arne jonh Nocomora', 'Information System is the combination of hardware, software, procedures, people and data which are used to provide data processing capabilities to an establishment\r\n	and also to provide information which will be used during decision making.\r\n\r\n	Albay Breeding station  Online Information System was created to help web users who wanted research or visit the place. it is a site wherein everyone can view \r\n	all the information of albay Breeding Station.', '2014-03-01', 1001, 501),
(21, 'Kawa kawa Hill Online Information System', 'Brixton jhon L. Gavina , Jake A. Buena', 'Kawa kawa hill is known to have a very relaxing nature and magnificeint landscape.this makes it one of the beautiful scenic spots in albay which strongly promotes \r\n	tourism in the province  not only here in locality but also in the whole country in abroad as well.our information system made it possible for the people to view \r\n	the kawa kawa through the internet which will provide information for those who are concerned, interested and willing to visit the place.', '2013-03-01', 1001, 501),
(22, 'Tamera Plaza Inn Online Reservation System', 'April G. Homebrobueno, Mariz Ivy O. Nolasco', 'A website is a set related web pages containing content such as text , images, video, Audio,etc. A website is hosted on at least one web server,accessible via a network \r\n	a network such as the internet or a private local area network through an internet address known as a uniform Resource Locator. all public accessible websites \r\n	collectively constitute the world wide web. \r\n\r\n	widespread use of the technology is changing the way we work, learn, and communicate even the way we carry out our regular, daily activities. in higher education,\r\n	technology has had a dramatic impact on teaching and learning including service learning experiences. service learning classes and activities can be augmented \r\n	through the use of the technology to provide more effective experiences for faculty, student and communicate participants. we their ready access to new technologies, \r\n	higher educational in institutions are well positioned to take advantages of rapid changes in the field. internet has arise our life in a variety of ways. internet\r\n	Reservations could get you the hotel of your choice at the click of a mouse in any part of the world not only saves money but also energy and time. it has made \r\n	to process of accessing information and also its management much more comfortable for both the customers  and providers.', '2013-03-01', 1001, 501),
(23, 'AMA COMPUTER COLLEGE', 'Leizel L. Federes, Michelle L. Agripa', 'Information system are constantly changing and involving as technology continues to grow. An information system is any combination of information system is frequently\r\n	used to refer to the interaction between people, processes data and technology. in this sense the term is used to refer not only to the information and communication\r\n	technology that an organization uses. but also to the way in which people interact with this technology in support of bussines processes .\r\n\r\n	computer has become a ways of life in this modern ages; it is evident that a majority of the country\'s institution still do not adapt the high technology.we all\r\n	knew  that some of the schools nowadays are more relient in the technologies to ease their tasks and make working fast and efficient . The project AMA computer \r\n	college Legazpi campus website, is to view the information regarding in AMA. We are looking forward to develop a web based application that will display the \r\n	information needed in the website. details are given to anyone who can view the website.We just provide an option to the administrator and the users to manage \r\n	the all information regarding to college.this project gives a simple way to work on it. \r\n\r\n	AMA information System is the system for operating college and to help the college students and the faculty members by providing the information needed', '2015-03-01', 1001, 501),
(24, 'Albayanos Online Furom', 'Rlva Lorelyn M., Barcenas Maricel V.', 'An internet forum is a discussion area on a website. Website members can post discussion and read and respond to posts  by other forum members. An Internet forum\r\n	can be focused on nearly any subject and a sense of an online community, or virtual community.tends to develop among forum members.\r\n	An internet forum is also called a massage board. discussion group bulletin board or web forum. how ever it differs from a blog. the name  for a web log as a \r\n	blog is usually written by one user and usually only allows for the response of others to the blog material.an internet for usually allows all members to make posts\r\n	and start new topics.\r\n\r\n	Albayanos Online Forum is Created Especially for albayanos, to make a new way of communication and sharing ideas to each members of the forum.', '2013-03-01', 1001, 501),
(25, 'An Online Information System of elements on Periodic Table', 'Marvin A. Rivero, Joan Navera', 'Information technologies nowadays are very useful in our everyday life.it deals with the use of electronics computers and computer software to convert. store \r\n	protect process transmit and retrieve information, securely, it is the study. design, development, implementation,support or management of computer based \r\n	information system,particularly software application and computer hardware. \r\n\r\n	this study an online information system about the periodic table of elements is a website where in the users easily view information about the periodic table of \r\n	elements.the periodic table is the important chemistry reference and a tabular method of displaying the chemical elements. therefore this study will give you \r\n	information about the periodic table of elements.', '2007-03-01', 1001, 501),
(26, 'An online Information System of Hoyop Hoyopan cave', 'Anjo T. arepentido,Jenifer M.Lomibao', 'web based application has broad definition in terms in our modern technology today which also includes creating a website. But its main role is to develop and \r\n	design an application providing everyone to their tasks efficiently and accurately. an online information system of hoyop hoyopan cave was created in a full \r\n	detailed information to help web users who wanted to research or visit to place.it is a site wherein everyone can view the all the information and also seek to \r\n	advertise and promote the cave. this sites provides web pages that include photos together its corresponding information about the cave.', '2013-03-01', 1001, 501),
(27, 'Causes and Effects of the Modern Technology to the Selected Senior High School Students of Computer Arts and Technological College', 'Nichole M. Bayna , Krishel Beato , Ray Ian Joseph Curaming , joshua Pintal, Sven Kyle Nazareno, Antonio Macandog ,William Olarita', 'Technology has become an integral part of the majority of our daily lives.As teens continue to become immerced in media ,many adults begun to wonder whether or \r\n	not this exposure to such a high amount of electronic media is a good thing or not.on they other side, there is an argument that technology is preparing children\r\n	for the real world that they will have to encounter in the future.\r\n\r\n	Modern technology have a good and bad impact to the world and specially to the youth/teens.As we become increasingly more reliant and absorbed in technology,it \r\n	is not surprising that today\'s teens have become avid users as well. in order to fully understand argument surrounding technology and teens,we must understand \r\n	the pros and the cons, and how our decision about technology use will affect to days children as they develop.\r\n\r\n	the important of the modern technology in our daily lives is undeniable. this is due to the fact that today\'s dynamic world, life without technology is meaningless.\r\n	without technology ,our lives would don\'t be just the usual walk in the park daily routine. technology is a huge contributor to the well being of human kind.just\r\n	try to imagine how hard it would be to make it through the day without the simpliest technologies Some people may be confused on what exactly technology is? \r\n	Technology is the systematic study of study of the methods and techniques employed in industry, research agriculture and commerce .\r\n\r\n	teens have their own likes, dislikes,curiosities, and needs that are not the same as their parents, teacher.as obvious as they may seem we as designers of new \r\n	technologies for children,sometimes forget that young people are not &quot; just shorts adults&quot; but an entirely different user population with their own culture norms,\r\n	complexities.', '2017-12-01', 1001, 501),
(28, 'An online Information System of Legazpi Grand Terminal', 'Rowelyn D. Marigondon , Mae O.Canada', 'we based application has a broad definition in terms of modern technology today which also includes creating a website. But its main role is to develop and design\r\n	an application providing everyone with the means to do their tasks efficiently and accurently . An online information system of legazpi terminal is a collection \r\n	of information design to gain knowledge about the terminal. We all know that the grand terminal is the first state of the art station and also the biggest station \r\n	in Legazpi City. It is Located the premier and the newest commercial and development project in Legazpi. It is a site wherein everyone can view all the information\r\n	and also seek to advertise and promote the terminal.', '2014-03-01', 1001, 501),
(29, 'An Online Information System of the Human Anatomy', 'Verano Ron Carlo L., Baclao Marlie A.', 'Information technologies nowadays are very useful in our everyday life.it deals with the use of electronics computers and computer software to convert, store, protect\r\n	process transmit and and retrieve  information. securely. It is the study,design,development,implementation, support or management   of computer based information \r\n	system, particularly software application and computer hardware. this study an Online information system about the Human Anatomy is a website wherein the users easily\r\n	view information about the human anatomy. web base application is one of the most advance technologies today that is designed and developed to help people do their \r\n	tasks efficiently and accurately, presently, it plays a very significant role in our social and business life. nowadays, a lot of new technologies are being developed\r\n	for the benefit of mankind.', '2008-03-01', 1001, 501),
(30, 'Personnel and payroll management system of nelimer maintenance and manpower services', 'Judey flores Abes, jhon mike Loganio Ballester', 'Nowadays, all establishment are becoming modernized, they use modern technologies to make their transactions fast,easy, and accurate in order to avoid waste of time and for\r\n	the sake of safety and security.it also helps human to solve and understand complex problem and analysis such as the computational need of humans. especially to business \r\n	establishment or corporation processing enormous data complex transaction. payroll is an example of a complex transaction because it is a critical business operation dealing\r\n	with numerous accounts and procedure on a payroll transaction involving the vast beat answer in that problem would be computer because computers can simulate huge data\r\n	and can process complex transaction in a fast and efficient way. it can generate numerous accounts and data accurately.\r\n\r\n	payroll is one of a series of accounting transactions, dealing with the process of paying employee for the services they rendered.companies experience a lot of payroll fraudulent\r\n	occuring by utilizing payroll software which effects their capital due to high increased payroll cost.\r\n\r\n	A computerized Payroll system will not only provide accurate calculation and fast process of payroll transaction but it will secure data through security implementation and \r\n	Accordingly arranged files provide by a well designed database that will produce a paperless environment.', '2019-03-01', 1002, 501),
(31, 'Point of sale and Inventory System for jim-bee tailoring and Fashion centre', 'Alvin Lazaro Sombrero, jaya Maria Gatallo Mapa, Ma. Jazen Bersabe Lotivio', 'In today\'s global apparel market retailers &amp; consumers push for lower prices, better quality and quicker delivery. the text tile and apparel manufacturing industries are\r\n	 rapidly modernazing, as new investments in automation and information technology have been made necessary by growing international competition, films also have responded\r\n	to competition by developing new products and services for instance some manufacturer are producing text tiles developed from fibers made from recycled materials, computers\r\n	controlled equipment aid in many functions, such as pattern making, design, and cutting.\r\n\r\n	Manila based asia Texttile mills Inc. has developed away to mass produce abaca yarm out of raw fibers and then weave it into high-end text tile products such as jeans, the\r\n	company exported its first lot of abaca jeans to japan in 2012 and now is marketing the jeans in the united states from this statement the philippines has been working to\r\n	promote local production and use of textiles made by abaca, and also it is biodegradable and global demand for eco friendly fabrics and led to increase the use of fibers \r\n	in apperaland home punishings in a niche market with the help of information system that could put the philippines at an advantage.', '2017-04-01', 1002, 501),
(32, 'Record Management System', 'Banas Rona B., Barrios Shiela B., Bongalon Sarah jane B., Villegas Jhonna M.', 'Records are the primary sources of evidence of all the undertaking and activities conducted by the government and private institutions, and serve as a point of reference by which \r\n	future  activities and decisions are measured Through the proper management of the records, the organization can eventually relate the current situation to their past and leave a \r\n	status. Record is an essential element in the formulation of the basic strategies for the organization to forecast the future of the organization and its people, the process and \r\n	intervation created  by the organization and its people, the process for improvement and the road towards the success is ready to be taken however the reports that gathered from\r\n	the past performances of the organization are created to detail the different situations and the applied solutions that they create. this may serves as the evidence of a person \r\n	or organization, generating timely that helps the different levels of management in assessing the performances as individual and as an organization.', '2013-04-01', 1002, 500),
(33, 'Online mall finder Information System for Legazpi city', 'Lopez Jordan V., Echaluce Samuel A.,Nueva kristine Joyce N.', 'Shopping malls are not stores in the real sense.it is a common mall in which many stores are located. The shopping Mall has one or even a couple of anchor stores. the variety of 	retailers to open branches in the mall. the higher the human traffic the more sales. In Legazpi city. there are several malls such as the pacific mall Legazpi Embarcadero de legazpi\r\n	A Bichara Silverscreen and Entertainment Center , Ayala Mall Legazpi, Lcc Mall and Yashano Mall.these malls have a wide variety of store such as fast food, Accessories and apparel.gift \r\n	Items. Electronics and the like.in many cases. the entire retail store is simply a kiosk. in others.the kiosk is an extention of the main store and serves as a brand ambassador as \r\n	well as a selling space.\r\n\r\n 	In the interest of keeping shoppers in the building.several malls are renovating their food courts to bring in more local eateries while others are opening new wings for high-end \r\n	restaurants. department stores.', '2018-03-01', 1002, 501),
(34, 'Anti-Bullying Android Game application', 'Jean B. Bolima , Bryan o. Belardo, Micah E. Madrona, Frednalyn D. Dajac, Clariss Mae E.Nariz', 'Gaming in our age has a habit.it is relatively modern compered to other traditional forms of entertainment like performing arts in theaters, and the like.I his broad process or gaming \r\n	concepts  into the entire spectrum or human  experience presents us with two potential barriers to understanding games (Utoyo M. 2016) the liberal use of gaming terms promotes an exaggerated\r\n	 perceptions of our own understanding of games. We fail to render unto the subject the careful and critical analysis that we tenders to more academic topics and we merrly can ignore the complexities of game design.A complete amateurs whose only relevant skill is programming undertake to design games with no further preparation than their own experience as a game players. \r\n	and those who overate their own understanding undercut their own potential for learning (washington State University 1996-7).\r\n\r\n	computer games are the  most popular entertainment in modern society and played by different ages. these recreational activities excite the players especially the teenager.Player\'s  excitement\r\n	leads them to play more and reach higher levels in every game.', '2013-03-01', 1002, 500),
(35, 'Southern Luzon Technological College Foundation Incorporated Automated Payroll System', 'Jean L. Areola jane M. Aringo Jane N. Barajas Jonelyn B.Travisonda', 'Payroll plays a vital role in a company for certain reasons. from an accounting perspective payroll is a crucial because payroll and payroll taxes consideribly affects the net income of most companies \r\n	and they are subject to the laws and regulations. from an ethics in a business viewpoint, payroll is a critical department for employees are responsive to payroll departments is to ensure that all emloyees \r\n	are responsive to payroll error and irregularities. the primary mission of the payroll department is to ensure that all employees are paid accurently and timely to correct  with holdings and the deductions \r\n	and to ensure that those with holdings and deductions  are remitted in a timely manner Even if is a big or small there is a deep satisfaction in having that the salary and it is like a composition  fora job \r\n	well done. payroll seems to be a simple task for other\'s perspective but actually it is more than just signing a check and delivering it to the employees.\r\n\r\n	Processing a payroll seems to be a complex task for it requires being accurate as the salary of the employees being computed and taxes must be considered also. This is also included generating, submitting \r\n	Reports and filling of taxes these are the usual problems that are an organization will be encountering much more if they are using the manual payroll system. a manual payroll system is effective for small \r\n	organization which has a few employees.', '2013-03-01', 1002, 500),
(36, 'Probikes Business Transaction system', 'Azores joan P., Suarez Eduardo Jr. Toledo Sandra N.', 'Information Technology has taken its center stage.The numerous advantages it had in easing the delivery of information around the world means that information around the world means the information technology \r\n	would shape the dynamics of the new generation. information technology started to spread mainly on the network technologies especially in computer using internet connection. \r\n	computer now a days became an unavoidable device use by people this is simply because of the advantages brought the computer. computer is now used by people regardless of their ages, young and old. faster data \r\n	transfer was one of the advantages of the computer. with the use this can access and share data and information in just a click through social media such as email, messenger, viber, SMS and facebook.\r\n\r\n	this time also fax became very useful comparing to traditional letter and airmails.we also applied the benefits of computer on banking sector. At preesent fund transfer in online bill payment have been utilized \r\n	by the public. An ATM machine,which is used in a banks, also worked with the help of computer.computers also served as a tool for entertainment and for the study purpose. to sum it up computers has multiple \r\n	uses which help the users find quickly and easy ways of doing things.it instantly generates accurate data if users find quickly and easy ways of doing things.it instantly generates accurate data if users utilize \r\n	it in proper way.', '2013-04-01', 1002, 500),
(37, 'An android Game Application Anchored From the Story Oxiaden', 'Brian Calma, Karen P. Candia, Krise ann Cebujano, Rheyncy Jhoy Mendez, Theodore Salcedo,', 'Technology is shifting the shape of our world in the current time.from the past years we cannot hide the truth of the rapid evolution of technology thus it really helps us on making our life easier and better. \r\n	one big example of the power of technology is what lies in our mobile phones our phones today to keep us safe in terms of accidents, emergencies and make us always connected to the new happenings of the real world\r\n	our mobile phones us keep in touch with friends,family  and co workers but also to tell us where to go what to do and how to do it.even the most domestic of events  seem to revolve  around mobile phones. Because of \r\n	the huge  amount of exploration and modernization  that our beloved developers exert now has the big fruit of technology in the form of android phones.\r\n\r\n	Android phones are the phones powered by the android operating system(OS)android is a mobile OS based on the lenux kernel and currently developed by google\r\n	when the simple touch,swipe,tap,pinch or reverse on every machine or devices that is powered by it can we simply enjoy our world in games,music video, contacts messages and many more Google is the strategy was to give android \r\n     	a way as free open platforms for OS so that would become the dominant mobile operating system for mobile phones and devices worldwide which work a lot.now billions of smartphone users, manufactures and business worldwide use \r\n	android as its primary mobile OS.', '2017-04-01', 1002, 500),
(38, 'Albay cathedral Online Information System', 'Crisol Michelle B., Reynancia Jheson D., Camacho Roderick L. Mangosta Pearl Rose B.', 'Any people information system aims to support operations, management and decision making. In broader sense the term is used to refer not to the information and communication Technology (ICT) that an organization uses\r\n	use to only access  a fregmant of information space accordingly to their current goal. computers, as one of the most effective technology tools, serve as efficient data storage system and excellent information processors\r\n	it can store, organized and manage huge amount of data.\r\n\r\n	A computer is more capable of information gathering especially when it is being connected to internet. Online is the condition of being connected to a network of computers or other devices. the term is frequently used \r\n	to describe someone who is currently connected to the internet Government, private organizations and individuals in contemporary society rely in information system to manage their operations compete in the marketplace \r\n	supply service and augment personal lives . information and provide a corrective reaction to meet an objective. the internet is a powerful tool for sharing information the world wide web helps organizations business\r\n	and individuals find information on the net.', '2017-08-01', 1002, 501);
INSERT INTO `thesis` (`id`, `title`, `author`, `abstract`, `published_date`, `school`, `course`) VALUES
(39, 'An android Game based on bicol theme', 'Bananai Jona D., Bolanos Edmon j., Bongalbal Michele A. Madronio jessa M. Noleal Angelica N.', 'Mobile Entertainment usually includes any activity meant for liesture that is undertaken via personal technology that is both networked and can move over distances \r\n	it consists of activities such as mobile games Social media music etc.that use to be as entertainment  applications.it also provides  a mode of relaxation and entertainment \r\n	for the users. There are mobile applications that serve as a great medium to the users for easy access in any place with the use of portable devices. users will be more productive\r\n	and can make an interaction via personal gadgets not just an entertainment  but a look personnel private matter.it is away for the users to communicate and to be updates easily with \r\n	with others form of entertainment.\r\n	\r\n	the continues growth of portable devices is very applicable in terms of high standard of application. these are also reasons for the consequent rise in application download.the \r\n	segment of the mobile entertainment market. a lot of users are engaged in the use of their phones and tablets as entertainment everywhere they settle.game applications developed\r\n	the trended this year are very challeging  to the users which made their past time fun and enjoyable.and also because of different social media such as facebook twitter yahoo tumbler instagram etc. sharing status updating and posting, are  commonly what users to do their accounts.So mobile applications is fast emerging and more preferred mobile device for entertainment.\r\n	this is why it makes perfect sense for users to adopt it whole heartedly.', '2017-08-01', 1002, 500),
(40, 'Clinic Management System with SMS Support for Sacred Heart', 'Azores Noel E., Akao Marjorie B., Limpo Erence Jerimhie G.,', 'Clinics a variety of options for outpatient. they are very convenient as they are often situated in rural areas where hospitals are not close by.most community clinics tend to offers services at low costs that hospitals or other clinics facilities.currently many clinics in our country record and store the patients record using paper or card manual system only. nurses need to manually\r\n	write down the patients information and index the patients medical card. these medical cards are kept in organized racks or in the cabinets.this system of storing the patient information is plaguing \r\n	and not secure.\r\n	information is crucial in making decision in relation to health. new communication technologies show great promises in providing ways to developed and deliver changes in health behaviors. the behaviors and communication changes in consumers patient provides and organizational levels due to innovations in electronic health information system such as personal health records (PHRs) electronic media health\r\n	information systems improved the quality patients care easy accurate and quick information sharing quick decision.', '2018-03-01', 1002, 501),
(41, 'C++ Syntax Through Puzzle Game', 'Lorilla Erlene B., Lopera Regine M., PandiiAna joy B., Bansale Gerald B., Herrera Malvar Jr M.', 'There\'s no constant thing in this world, everything is a subject for a change. as the time goes by,innovation of technology  in the modern society is continously increasing as a source of information, highly \r\n	empowered for industrial work force, very convenient for business and even for leisure of all generation now a days. internet was adopted further developed as means of communication by educational institutions \r\n	in the 1970\'s it has massive potential as a learning tool. In recent years, all nations are now enter connected and can help to build an economic developed country. \r\n\r\n	technology has revolutionized the way people work and is now set to transformed education. children cannot be effective in tommorow\'s world if they are trained in yesterdays skills.nor should teachers be denied the\r\n	that tools that other professionals take for granted (tony Blair 1998)\r\n	Our schools community college and universities should be incubators of exploration and invention. educators should be collaborators in learning seeking  new knowledge and constantly acquiring new skills along side \r\n	their students.', '2017-03-01', 1002, 500),
(42, 'E-Travel Mo', 'Hakeem Magayanes, justin Balderama, Kimberly Lopez, Ryan Alaurin', 'Tourism is travel for leisure recreational and business purpose. Tourist can be defined as people who travel to and stay in places outside their usual surroundings for more than twenty-four hours and not more than\r\n	one consecutive years for vacation business and other purposes.it has been an industry of vast dementions and eventually supports economic and social growth. a travel agency is a private retailer or public services \r\n	 that provides travel and tourism realated tourism services to the public and car rental is one of the services they provide.\r\n	number of tourist in bicol region has increased in 2017.based on the statistic dated august 7, 2017 from departmen of tourism region 5 there were  4,519,531` tourist who visited Bicol  boths domestic and foreign \r\n	tourist there are many travel and tour agencies here in bicol adventure 111 travel and tours royal quest careton travel and tourst transport cooperative JRA travel and tourst northink events and travel services \r\n	amreach travel and tours. based on the record the number of travel and tour agencies as of january 2018 is (51) fifty one according to department of tourism region  5. Establishing a travel and tour busineess \r\n	here in bicol is good because of the increasing tourist arrivals. there are some travel and touragencies here in bicol that only have a manual transaction or websites that the customers can visit and inquire.\r\n\r\n	Based on a survey result conducted by newstyle phillippines use smartphones in the whole southest asia. in 2017 (32) thirty two percent of the pupolation of the philippines use smartphones majority of them use android \r\n	over OIs  because anroid is adaptable for cheeper smartphones. Mobile applications likeExpedia have many features. it has hotel reservation. this application also have some travel packages and promos, it has alsoa discovey \r\n	feature where the client can easily access the details and the city that he is. dealrey is aflish reservation application where the client can recieve a text message if the flight day booked where canceled or delayed \r\n	velocity is a restuarant reservation aplication; it as a list of 200 restuarants that the client wants to eat. autoslash is a car rental reservation app; this application asks  the users to input their car reservaton \r\n	details into is application their tracks the rate of the car rental competitors pricing if a lower price for the same car renatl is available it will send the user an email and it can rebook the users reservation. in the \r\n	study conducted in Brac university entitled realtime vehicle tracking system  they use GPS to track vehicle and to know the current location of the vehicle.', '2019-04-01', 1002, 500),
(43, 'Fireout An Online BFP Out emergency Application', 'Bas Jay Mhelson T., Nolasco John Lyndon Ll., Baldo Angela Faith C.', 'fire safety has alaways been critical issue and concern in the philippines manila city is home of hundreds of high rise buildings constructed using a variety of flammable materials generating concern of the fire safety. it \r\n	it important to have a basic understanding about fire accurs and behaves with a buildings.essentially fire is chemical reaction. a carbon based material(fuel) mixes of oxygen usually a component of air) and comes in contact \r\n	with something hot enough to heat this mixtures so that composituble vapors are produced. if these vapors dissipate, then nothing', '2019-08-01', 1002, 501),
(44, 'Physical Fitness Gym Online Registration with SMS Notification', 'Lizette P. Belaro, Christian Paul A.Carinal, Meccah Ella R. Lita, Mark john L. Penilla , April Lee P. Tabasa', 'Information system(IS) is an organized system for the collection, organization, storage and communication of information. specifically, it is the study \r\nof complementary networks that people and organization used to collect filter, process, create and distribute data. Information system, an integrated set of components\r\nfor collecting,storing and processing data and for providing information, knowledge, and digital products. business firms and other organization reliaf on information \r\nsystems to carry out and manage their operations interact with their customers and suppliers, and competed in the market place. many organizations worked with large amount\r\nof data. data are basics values or facts and are organized in a database. many people thinkof data as synonymous with information actually consists of data that has been \r\norganized to helps answers questions and to solve the problems. an information system is defined as the software that helps organized and analyze data. so, the purpose of \r\nan information system is to turn raw data.', '2018-03-01', 1000, 501),
(45, 'Barangay Health Center Information System with SMS Notification of anislag daraga, albay', 'Cary Mae M. Cariga, Jerome P. Jacob, Jennifer B. Lambunao, Jhade C. Listanco', 'Anislag health center i a community-based and patient directed organization. Its is located at Purok 6 anislag, daraga, albay. They offer different programs \r\nsuch as immunization, tuberculosis treatment,family planning, they also offer health monitoring and profiling , medical services involving medical care, consultations child\r\ncare, postpartum and pre-natal barangay health center of anislag, daraga, albay still adopts the current system that causes many problems such as, slow process in retrieving\r\nprocess in retrieveing patient records, unmonitored inventory of medicines and uninformed resident health programs the proposed system  Barangay health center information \r\nwith SMs notification of anislag daraga, albay was designed to make the transactions easy fast and convineant.the midwife and staff would the beficiaries of this proposed \r\nsystem because it would help to avoid the redundant proccess of transactions and toavoid to lose of patient\'s data. the proposed system would solve the problems in retrivieng\r\nand storing the patient\'s for the information of upcoming health programs/services and to notify the patient for their next checkup. in monitoring of medicine. the system would\r\nalarm the users if the medicine, reach the minimum quantity to avoid inconveniences and also for the generating of patient\'s data would give them a hassle free making a reports. The systems would improve to the transactions and would give the patients/residents service.', '2018-03-01', 1000, 501),
(46, 'Senior High School Notification System of CCDI Legazpi using Biometric and GSM Modem', 'Geralidine Joy N. Arao, Egan D. Arizapa, Mel Brayn L Mendoza, Rico L. Quimpo', 'Computer Communication Development institute(CCDI) legazpi is one of the fastest growing and innovate institution in legazpi city. It aims offer quality educations students providing them with excellent facilities. But there were some concern that should be improved, like the attendance monitoring. the data was prone to lose that could cause insufficient and in accurate information of the student attendance monitoring.  In the study, the proponents aims to provide better way of monitoring the attendance of CCDI legazpi. In order to provide \r\na system that could organize and could keep the data administrator handled to speed up the process of attendance monitoring. Attendance monitoring provided a backup and restored data in \r\ncase of data loses. It would reduce the work of the person in charge and it would never be the time consuming. all the attendance consuming. All the attendance information of every students \r\nof senior high school in CCDI legazpi would be safe, secured and would be updated in this system.', '2018-03-01', 1000, 501),
(47, 'Magayon Animal Clinic Veterinary Records Information System', 'Annalyn A. Atillo , Ma. Rayzel A. Abay , Dhan jason T. Molina, Mark justin B. Rasco', 'Magayon animal clinic is one of the well known veterinary clinic in albay. It was an originally established as a corporation by three (3) veterinarians as soon as they passed the board exam four years ago. The original name given to it was &quot;Pet&quot; farm and wild animals unfortunately, it only lasted for  years and is now operated and managed as a sole proprietorship\r\nbusiness by Dr.Christian Lemuel R.vargas, for three years already. the said clinic is located at Rizal st. (pag asa) San ruque, Daraga, Albay that serves and medicates animals. there are also two(2) veterinary technicians inside the clinic namely Roman D. Paguia and Jessa May l. Doploso. The proposed systemen titled magayon animal clinic: vaterinary records information system simplify \r\nthe workload of the staff, provides ease of access on records more secured and safer storage of data. production of much accepted receipt and reliable documents, preferable communication with the\r\ncustomers and provide convenience and hassle  free work on staff.unlike with the current system adopted by the clinic which is prone to human errors, consumes a lot of time and efforts more on \r\npaper works and dependition on the observation and analysis of the workforce.  the proponents aim to develop a system that will improve and enhances the clinic as a whole, help finish their works\r\non time, Fullfill and guarantee the needs of the clinic, the staff and the costumer.', '2018-03-01', 1000, 501),
(48, 'Patients Reservation s]System with SMS Support of Washington Medical Clinic and Diagnostic Laboratory', 'Angeline A. Atos, Christian jay L. Lianzana, Owen V. Miralles, Danica A. Madredeo', 'The washington Medical clinic and diagnostic laboratory is one of the accessible, affordable, and reliable clinics for albayanos and nearby province the legapzi city . Dr Randie asuncion \r\nowns the washington Medical clinic and diagnostic laboratory. the clinic was established on year 2012 at washington drive legazpI city (near albay astrodome and LTO albay) the clinic offers medical and \r\nLaboratory exams. in the year 2016, Dr. asuncion decided to established another clinic at Rayuco BLDG., Rizal St., Bano., Legazpi City. the clinic  still makes good use of the manual process of storing \r\nand recording of patient\'s record. the patient will visit the clinic and ask the nurse on duty about the availability of the doctor and the possible schedule of chick up. the patient will not be accommodated\r\non the day that the patient visited the clinic without prior appointment because the doctor\'s is on call therefore, patient will come back on the schedule date. when the scheduled date the come,the nurse will\r\ngive general patients form to be filled out the patients, and all the information from the said form will be recorded on the computer of the clinic in the MS. word. the patient\'s will be checked up and after \r\nsuch the doctors will give a prescription. the patient will pay at the cashier and will ask if their is available medicine prescribed by the doctors. the nurse manually inputs all the given information by the \r\npatients will pay at the cashier and will ask if there is available medicine prescribed by the doctors. the nurse manually inputs all the given information by the patients and that will make the process long \r\nand time consuming. Using the manual process. they encountered different problems.security, the medical forms as easily exposed to an authorized user because anyone can easily get the vital patient information\r\nfrom clinic because the medical forms are just kept on the rock or cabinet. It is also time consuming because using medical forms, time is wasted when the medical form needs to be.', '2018-03-01', 1000, 500),
(49, 'Flood Alert System of Barangay 14 ilawod Legazpi City', 'Marian Suseth A. Espinas, Jayson A. Recamunda , Roger V. Adanza, Jeff A. Emberga', 'Floods are the most frequent type of the disaster worldwide. it can strike anywhere and anytime. although floods can be predicted, they often cause massive damage and distractions of property as most urban \r\ncommunities are located near water sources  such as course and rivers. however not all floods are alike while some floods.floods develop quickly, many storm pass through by sometimes in just a few minutes and without any \r\nvisible signs of rain. Weather such as the heavy rain thunderstorms ,hurricanes, or tsunamis can cause flooding.flooding can also can a river also stream overflows its bank with a level is breached, or when a dambreaks \r\nflash floods which can develop quickly often have a dangerous wall of roaring water, the wall carries rocks, mud, and rubble and can sweep away most things in its path. flood affects and community and even large areas \r\naffecting entire river to floods and landslides,prompting the mines and geosciences bureau (MGB) to warm people living low-lying villages,near river channels and shorelines as well as mountain slopes to be extra cautions \r\nin the light of the onset of the rainy season this months. in the united states, the U,s Geological survey and the national weather service part of the national oceanic and atmospheric administration work together to\r\nmaintain flood warning systems across the country.specifically, the USGs acts the principal source on surface and groundwater data, and operate more then 85 percent of strain gagging stations in the US the NWs uses the \r\ndata from other sources to issue river forecasts and flood alerts.the NWS issues river and streams. the alerts are divided into the several basis,or for the particular rivers and streams. the alerts are divided into \r\nseveral basic categories flood watches are issue when conditions suggest a possibility or flooding or if flooding is expected across a large region or if flooding is imment or actively taking place flash floods watches\r\nand warning follow the same protocol but indicate potential for especially rapid flooding usually from heavy rain or expected along major streams where people and property are threatened.', '2019-03-01', 1000, 500),
(50, 'Relief Goods Assessment System for Provincial Social Welfare and Development Office of Albay', 'Jerry A. Cantorio, Alexis F. Lomibao, Rosalie E. Ascano, Janice B. Naag', 'One of the most important programs of provincial social welfare and development office (PSWDO) of albay is giving relief goods in times of calamities for their citizen,especially in most affected areas. This\r\nstudy involves government agencies which are tasked in the management of relief operations such as: the provincial social welfare and development office, city social welfare and development office and municipality social welfare \r\nand development office (PSWDO) face some of the common problems encountered by the relief management team is that some of the werent even opened yet. more ever,it also focused on the problem of the non moving relief goods and to \r\npropose a solution on how we can improve this thought the use of faster assessment of the relief goods. The proposed system is highly technical and requires very minimal staff to keep it running smoothly. the objective of the propose\r\nsystem is to make sure that the relief goods reach the disaster victims in as little time as possible, for the reason that people are disperately crying out for food during a calamity.\r\n\r\nThe FDD methodology is used in this study where in FDD features are planned and developed one by one as incremental units it means that the developers and clients can see the results and implementation in a short period of time, hence \r\nerrors can be easily corrected suggestions of clients can be easily corrected, suggestions of clients can be easily integrated.', '2019-03-01', 1000, 500),
(51, 'Emergency Monitoring System of Municipality of Daraga', 'Asilah Rashid R. Al alawi,Analisa M. Dolosa, Mary ann Lopo ,Geralyn B. Vista', 'Philippine National Police and bureau of fire protection of daraga albay of have been very active in responding to any incident, but sometimes the slow process in responding to any incident but sometimes the slow process\r\nin responding cannot be prevented due of difficulties in communication in rural areas in daraga albay. for this reason, this system capability to identify the kind of the incident took place in a certain area. This study developed a system\r\nthat could assist the PNP and BFP, as well as a barangay councils of daraga albay in identifying any untoward incident or emergency that may arise in any place at any time.with the help of the proposed system and a sms support, respondent \r\nwere able to immedaitely take action.the proponents pursued to designed the overall model of the proposed system and identified the features of the purposed system and to test the extent of its functionality and usability.\r\n\r\nthe first part of the study was to gather the recorded history of incidents like fire riots and robbery and to responding to untoward incidents or emergencies. This prompted the researchers for the development of a new systems to helps the \r\nresidents of daraga albay and concern agencies (PNP and BFP) to make it easier for them to communicate and report incident.The study was conducted in the municipality of daraga, albay which as 54 barangays information were gathered from the \r\ndifferent agencies in charge to respond to any untoward incidents or emergencies like PNP and BFP barangay officials and the selected residents of daraga Albay.', '2019-03-01', 1000, 500),
(52, 'Typhoon Relief Medical Assistance of Municipality of Daraga Albay', 'Myka Vanessa Vargas , Elton Jhon A. Arcos , Marisol Antiquiera, Francis E. Reyes', 'Daraga town is one of the most highly affected municipality when a typhoon directly hits the province of albay. This lead to food shortage, hunger and illnesses  because of devastating effects to facilities and resources that \r\ncater services to its constituents. In addition the government with the participation of agencies like MSWDO, DSWD and DOH provides an immedaitely relief of operations in evacuations sites in daraga albay. This study aims to developed a system \r\nthat covered and focus on the monitoring of needs of evacuees in the evacuation sites of the municipality of daraga in terms relief and medical assistance. the researchers conducted an initial interview to the client which is the MSWDO \r\ndaraga regarding the manual process of monitoring the needs of the evacuees. the information gathered from the initial interview was collected and accumulated to design the interview was collected and accumulated to design the overall model\r\nof the proposed system, to identify the features of the system and to test the accuracy and usability of the system. \r\n\r\nOn november 30,2006 the strongest typhoon that battered the 54 barangays of daraga albay was the typhoon reming. The aftermath of the typhoon lead to food shortage hunger and not potable water. the response of the government and local government \r\nagencies was slow due to the fact that they could hardly identify the needs of the affected areas not to mention the inconvenience by the situation to the affected families. complication arose because they were not immediately provided humanitarian\r\nassistance for relief and medical supplies they urgently needed for the survival. this prompted the development of a system to monitor the needs of the evacues and to improve the efficiency of service of the local government of daraga and the \r\nagencies that will assets them in monitoring the needs to the evacuess in times of calamities such as a typhoon the 54 barangays of the municipality of daraga will serve as a subject in this study.', '2019-03-01', 1000, 500),
(53, 'Children\'s Nutrition Monitoring System of Municipality of Daraga Albay', 'Kim Beberly A. Peralta , Patricia D. Destura , Charlyn E. Bermillo, Jessica P. Navia', 'Daraga Albay is one of the municipalities in the Bicol region that is experiencing malnutrition. the recognition that malnutrition may emerge from childhood which results to the ability to reach their full genetic intellectual\r\npotential, introduces a new and perhaps frightening note into theories of the national development it is now suggested that malnourished children may be basically dull.the significance of this can be appreciated when we recognized that as many \r\nas two-thirds of the children of most developing countries are now suffering from some degree of malnutrition.', '2019-03-01', 1000, 500),
(54, 'Water Billing Information System of Daraga Water District Via SMS Notification', 'Sarah Jane Baldon, Melanie Mirabete, Ulysses Lofamia, Lyka Inocencio', 'Daraga water district is a company that provides water supply in most area in the municipality of daraga albay. it currently serves amount 23 barangay \r\ncoming from the municipal poblacion to the nearby barangay accessible to prevalent transmission mains. most of the areas served at present recieve a constatnt  24 \r\nhours water supply. The water Billing system of daraga water district was using an automated system that was based on the direct paying water bills. this system managed transaction it tracked all the records of the costumer if they have paid on the due date or not. A penalty will is added to their bill should they fail a pay. this  system \r\ntraced the costumers should they fail to pay. this system traced the costumers account if she/he was not Able to pay the previous months the study used the FDD methodology because the FDD features are planned and developed one by one as incremental units, it means that the develop and clients.\r\ncan see the results and implementation  in a short period of time, hence,errors was easily corrected, suggestions of clients was easily integrated.', '2019-03-01', 1000, 500),
(55, 'Anti Thief Motion Detection Information System with SMS Support', 'John Benedict R. Ortal, Jenner Neil Aragon, Joshua Bulawan, Dominic Cardel', 'One of the most pressing issues in the country was security.not only in protecting human lives but also protecting properties specially establishment\r\nPresently, there was many different captured  devices used and installed into different areas to monitor every event into an environment.  Anti thief motion detection information system with SMS Support was created to solve this problem in sociaty. It was room or place.this study used an applicable captured \r\ndevice to object into a certain space and detected by the motion detector.the very concepts of the study was to secure a space in a set of time where there are no allowed \r\nhumans inside the automatically sent a message to the administrator via SMS and capture the picture of the place through the use of the camera and save to the central file\r\nlocation for future use. this study benefited not only the researchers but also the pilot implementor of this study,which is the computer communication Development institute\r\nLegazpi as a whole.', '2019-03-01', 1000, 501),
(56, 'Post Road Damage Disaster Assessment System of Daraga Albay', 'Denhensly Lanuzo Amaranto, Mary cris Pocaan Rivera, Lalaine Mapusao Ativo, Karla Navares Lina', 'In the current system post damage disaster assessment system of daraga albay focused in the main damage of roads in every barangay, by using SMS notification the \r\nDPWH directly distinguished the damage roads or the affected roads. all barangay captains of daraga albay where the persons who are capable of sending a message and receiving an immediate response from the user. In this way the difficulties about effected road areas of the barangay were solved by using proposed system. \r\n\r\nthe department of public works and highways(DPWH) was the user and took responsibility of the affected roads in daraga. the researchers conducted information in daraga\r\nalbay and specifically focused in 54 barangays in daraga.', '2019-03-01', 1000, 501),
(57, 'Mayon Volcano Eruption Monitoring System of Albay with SMS Support', 'Rico Dazal, Elwood Tanio, Jefferson Tumbga, Thalia Obana', 'One of the natural causes of climate change is volcanic eruptions it is evident over a period of time. a new capability to predict the climatic response to a\r\nlarge tropical volcanic eruptions for the succeeding 2 years will prove its value to society. in addition, to detect its influence on climate, including effects of greenhouse\r\ngases, aerosol, and ozone-depleting chemicals it is crucial to quantify the natural fluctuation so as to separate them from anthropocentric forces, furthermore modeling the effects\r\nof volcanic eruptions helps us improve climate models that are needed to study anthropocentric effects. large volcanic eruptions inject the sulfur gases into the stratosphere\r\nwhich is converted to sulfate aerosol with an a folding residence time of about the year. large ash particles fall out quicker. the rediative and chemical effects of this aerosol \r\ncloud solar radiation back to space, the aerosol cool the surface but by absolving solar and terrestrial radiation, the aerosol layer heats stratosphere. Mayon volcano eruption \r\nmonitoring system of albay with sms support was created to solve the problem in assessment and lack of preparedness before the volcanic eruptions.eruption monitoring system was in the \r\nfact the most advanced solution for the assessment and for the management of the information related to the volcanic eruptions. creating such as system was beneficial in assessment \r\nand reducing risks of life during volcanic eruption. According to PHIVOLCS 2017, a weak phreatic eruptions occurred at 1357 on 2 march 2017. the event was recorded by the seismic networks as an explosion type earth quick \r\nfollowed by short duration tremor that lasted approximately 26 minutes. visual observation were obscured by weather clouds, although a small steam plume rising from the SE be visually\r\nobserved due to dense weather clouds covering the summit. PHILVOCS reported that precise leveling data obtained during 14-23 june 2017  indicated inflation since February 2017. according \r\nto PHILVOLCS, continuous GPS measurements have indicated an inflationary trend since July 2016.', '2019-03-01', 1000, 501),
(58, 'DILG Legazpi Personnel Department Information System with SMS Notification', 'Marlyn S. Dela cruz, Christian Ralph Ecal, Irene Llagas, Ma. Sherry Ann Regatcho,', 'The department of interior and local government was established to assist the president in his general supervision over local government units, oversee and monitor the \r\npromulgation of policies, rules and regulation. its people are constant in performing their duties and functions with the ultimate goal, which was to ensure the best delivery of basic \r\nservice to the people, but there were some concern that should be improved like the storage and retrieval of record the data were prone to lose that caused missing of files.\r\n\r\nthe proponents in this study had provided of the convince, easy to access and user friendly personnel information system with sms notification.this system could organized and keeped \r\ndata for storage and retrieval at applicant records, employees 201 file and pertinent record.\r\nthe DILG Legazpi personnel department information system with sms notification provided back up and restores data in case the data loses. it helped the administrator aid to reduce the \r\nwork and lessen the time consumed with the system all the records of the employees and the applicant was secured and was located easily. the system also had SMS notification that helped\r\nthe applicants in notifying them.', '2018-03-01', 1000, 500),
(59, 'online enrollment system', 'Wawie O. Llaneta, Rex Paolo D. Capitulo, Alvino R. Asejo and D. Arizapa', 'abcd', '2020-02-01', 1002, 500);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `access` enum('Administrator','School User','Normal User') NOT NULL DEFAULT 'Normal User',
  `school_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `access`, `school_id`) VALUES
(100, 'admin-CCDI', '12345', 'Yolanda', 'Gravito', 'gravitoyolanda@yahoo.com', 'Administrator', 1000),
(101, 'vivian', 'labayo', 'vivian', 'labayo', 'vivian@gmail.com', 'Normal User', 1000),
(102, 'dean', 'ccdi', 'Jay', 'Dadea', 'JayDadea@gmail.com', 'School User', 1000),
(103, 'chano', '123', 'christian', 'herrera', 'chano@email.com', 'Normal User', 1000),
(104, 'marvz', '123', 'marvin', 'lotivio', 'taba@gmail.com', 'Normal User', 1000),
(105, 'CATCOLLEGE', '1234', 'vivian', 'labayo', 'cat_college@yahoo.com', 'School User', 1001),
(107, 'vivs', '123', 'vivian', 'labayo', 'vivian@gmail.com', 'Normal User', 1000),
(108, 'lem', 'baks', 'lemuel', 'baks', 'bakslemuel@yahoo.com', 'Normal User', 1000),
(109, 'SLTCFI-ADMIN', '1234', 'vivian', 'labayo', 'vivians@gmail.com', 'School User', 1002),
(110, 'admin1', '12345', 'vivian', 'labayo', 'chano@email.com', 'School User', 1002),
(111, 'baks', '123', 'baks', 'baks', 'baks@gmail.com', 'Normal User', 1000),
(112, '123445', '12334', '1223323', '121323234', 'james@gmail.com', 'Administrator', 1000),
(113, '1223', '12334', '123344', '122344', 'james@gmail.com', 'Normal User', 1000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schools`
--
ALTER TABLE `schools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `thesis`
--
ALTER TABLE `thesis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Thesis_fk0` (`school`),
  ADD KEY `Thesis_fk1` (`course`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `Users_fk0` (`school_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=504;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `schools`
--
ALTER TABLE `schools`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1003;

--
-- AUTO_INCREMENT for table `thesis`
--
ALTER TABLE `thesis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `thesis`
--
ALTER TABLE `thesis`
  ADD CONSTRAINT `Thesis_fk0` FOREIGN KEY (`school`) REFERENCES `schools` (`id`),
  ADD CONSTRAINT `Thesis_fk1` FOREIGN KEY (`course`) REFERENCES `courses` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `Users_fk0` FOREIGN KEY (`school_id`) REFERENCES `schools` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
