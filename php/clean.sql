CREATE TABLE `Users` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`username` varchar(50) NOT NULL UNIQUE,
	`password` varchar(50) NOT NULL,
	`fname`	varchar(255) NOT NULL,
	`lname` varchar(255) NOT NULL,
	`email` varchar(255) NOT NULL,
	`access` ENUM('Administrator','School User','Normal User') DEFAULT 'Normal User' NOT NULL,
	`school_id` INT NOT NULL,
	PRIMARY KEY (`id`)
)
AUTO_INCREMENT=100;

CREATE TABLE `Schools` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	`address` varchar(5000) NOT NULL,
	`contact` varchar(24) NOT NULL,
	PRIMARY KEY (`id`)
)
AUTO_INCREMENT=1000;


CREATE TABLE `Thesis` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`title` varchar(255) NOT NULL,
	`author` varchar(255) NOT NULL,
	`abstract` varchar(15000) NOT NULL,
	`published_date` DATE NOT NULL,
	`school` INT NOT NULL,
	`course` INT NOT NULL,
	PRIMARY KEY (`id`)
)
AUTO_INCREMENT=10000;

CREATE TABLE IF NOT EXISTS `thesis_pending` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `abstract` varchar(15000) NOT NULL,
  `published_date` date NOT NULL,
  `school` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `date_submitted` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Thesis_fk0` (`school`),
  KEY `Thesis_fk1` (`course`)
) AUTO_INCREMENT=20000;

CREATE TABLE schools_courses(
	school_id int(11),
    course_id int(11)
);

CREATE TABLE `tags`(
	`thesis_id` INT NOT NULL,
	`tag` varchar (255) NOT NULL
);

CREATE TABLE `Courses` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`full_name` varchar(255) NOT NULL,
	`abbr` varchar(12) NOT NULL,
	PRIMARY KEY (`id`)
)
AUTO_INCREMENT=500;

CREATE TABLE `Messages` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`thesis_involved` INT NOT NULL,
	`message` varchar(7500) NOT NULL,
	`sender_name` varchar(50) NOT NULL,
	`sender_contact` varchar(24) NOT NULL,
	`date_sent` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`status` ENUM('Unread','Read') NOT NULL DEFAULT 'Unread',
	`response` enum('Accepted','Declined','Unset') NOT NULL DEFAULT 'Unset',
	PRIMARY KEY (`id`)
)
AUTO_INCREMENT=5000;

CREATE TABLE `Appointment` (
	`message_id` INT NOT NULL,
	`meeting_date` TIMESTAMP NOT NULL,
	PRIMARY KEY (`message_id`)
);


CREATE TABLE `log_user`(
	`user_id` INT NOT NULL,
	`time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`type` enum ('login','register') NOT NULL DEFAULT 'login'
);

CREATE TABLE `log_search`(
	`user_id` INT NOT NULL,
	`time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`thesis_id` INT NOT NULL
);

CREATE TABLE `log_study`(
	`thesis_id` int NOT NULL,
	`user_id` int NOT NULL,
	`time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`type` enum ('add','modify','remove') NOT NULL DEFAULT 'modify'
);

ALTER TABLE `Users` ADD CONSTRAINT `Users_fk0` FOREIGN KEY (`school_id`) REFERENCES `Schools`(`id`);

ALTER TABLE `Thesis` ADD CONSTRAINT `Thesis_fk0` FOREIGN KEY (`school`) REFERENCES `Schools`(`id`);

ALTER TABLE `Thesis` ADD CONSTRAINT `Thesis_fk1` FOREIGN KEY (`course`) REFERENCES `Courses`(`id`);

ALTER TABLE `Messages` ADD CONSTRAINT `Messages_fk0` FOREIGN KEY (`thesis_involved`) REFERENCES `Thesis`(`id`);

ALTER TABLE `Appointment` ADD CONSTRAINT `Appointment_fk0` FOREIGN KEY (`message_id`) REFERENCES `Messages`(`id`);


INSERT INTO `Schools` VALUES (NULL,'CCDI','Legazpi City, Albay','09123456791');
INSERT INTO `Users`	  VALUES (NULL,'admin','password','John','Doe','john@doe.com','Administrator','1000');
INSERT INTO `Courses`  VALUES (NULL,'Bachelor of Science in Computer Science','BSCS');