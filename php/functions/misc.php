<?php 
    include_once("Config.php");


    /*  SESSION RELATED Function*/
    function checkIfLogin(...$allowed){
        if(!isset($_SESSION['username']))
        {
            header("location: login.php");
            exit();
        }
        $redirect = TRUE;
        foreach ($allowed as $access) {
            if($_SESSION["access"] == $access){
                $redirect = FALSE;
            }
        }

        if($redirect){
            header("location: login.php");
            exit();
        }

    }

    //Usage allowUser("School User","Administrator")
    //allowUser(ADMIN)
    function checkUser(...$param){
        if(!isset($_SESSION["access"])){
            return false;
        }
        foreach ($param as $access) {
            if($_SESSION["access"] == $access){
                //echo $access;
                return TRUE;
            }
        }
        return false;
    }

    //Return true if account user has the same school id
    function checkModSchool($schoolid){
        if($_SESSION["school"] == $schoolid){
            return TRUE;
        }
        return false;
    }


    //Call in Login Page only
    function checkIfAlreadyLogin(){
        if(isset($_SESSION['username']))
        {
            header("location: index.php");
            exit();
        }
    }


    /* FORM VALIDATION FUNCTIONS */

    function cleanInput($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    function checkEmpty($data,$item){
        if($data = "" || empty($data)){
            return $item ." is Invalid or Empty.";
        }
    }

    function checkHasDigits($data,$item)
    {
        if(!ctype_alpha($data))
        {
            return $item ." cannot contain digits or symbols or be empty.";
        }
    }



    /* MISC */

    //Use for Classes that required 'active' to be highlighted
    function returnActive (...$param){
        for ($i=1; $i < count($param); $i++) { 
            if($param[0] == $param[$i]){
                echo "active";
            }
        }
    }
    //eg
    //returnStrTrue($input["school"], $school["id"], "selected");
    //short for ====  if($input["school"] == $school["id"]) echo "selected";
    function returnStrTrue($param,$param2,$strToReturn){
        if($param == $param2){
            echo $strToReturn;
        }
    }

    function returnIsInvalid($param){
        if($param != ""){
            echo "is-invalid";
        }
    }


    //Will empty a variable or array-> Use in forms
    function returnEmptyVar(&$data){
        if(is_array($data)){
            foreach ($data as &$value) {
                $value = "";
            }
        }else{
            $data = "";
        }
        return $data;
    }

    function returnErrorMsg($data){
        if($data != ""){
            echo '<small class="text-danger">'.$data.'</small>';
        }
    }

    function returnSuccess($data){
        if($data != ""){
            echo '<small class="text-success">'.$data.'</small>';
        }
    }
 

?>