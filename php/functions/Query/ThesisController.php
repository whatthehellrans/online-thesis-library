<?php
    
//include_once(dirname(__FILE__)."/../Classes/Thesis.php");

class ThesisController
{
    public $connection;

    public function __construct()
    {
        include_once(dirname(__FILE__)."/../Config.php");
        $this->connection = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        // Check connection
        if ($this->connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
    }

    public function __destruct()
    {
        $this->connection->close();
    }

    public function UpdateThesis($Thesis){
        $stmt = $this->connection->prepare("UPDATE `Thesis`
                                            SET    `title` = ?,
                                                   `author` = ?,
                                                   `abstract` = ?,
                                                   `published_date` = ?,
                                                   `school` = ?,
                                                   `course` = ?
                                            WHERE  `Thesis`.`id` = ?");
        $stmt->bind_param("ssssiii",$Thesis["title"],$Thesis["author"],$Thesis["abstract"],$Thesis["published_date"],$Thesis["school"],$Thesis["course"],$Thesis["id"]);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }


    public function InsertThesis($Thesis){
        // Prepare and Bind 
        $stmt = $this->connection->prepare("INSERT INTO Thesis VALUES(NULL,?,?,?,?,?,?)");

        $stmt->bind_param("ssssii", $Thesis["title"],
                                    $Thesis["author"],
                                    $Thesis["abstract"],
                                    $Thesis["published_date"],
                                    $Thesis["school"],
                                    $Thesis["course"]);

        //Nothing follows-> Execute and Check if there's an Error
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }

    public function InsertThesisPending($Thesis){
        // Prepare and Bind 
        $stmt = $this->connection->prepare("INSERT INTO thesis_pending VALUES(NULL,?,?,?,?,?,?,NULL)");

        $stmt->bind_param("ssssii", $Thesis["title"],
                                    $Thesis["author"],
                                    $Thesis["abstract"],
                                    $Thesis["published_date"],
                                    $Thesis["school"],
                                    $Thesis["course"]);

        //Nothing follows-> Execute and Check if there's an Error
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }

    // ----
    // INSERT TAGS
    // ----
    public function InsertTags($ThesisID,$tag){
        // Prepare and Bind 
        $stmt = $this->connection->prepare("INSERT INTO tags VALUES(?,?)");

        $stmt->bind_param("is", $ThesisID, $tag);

        //Nothing follows-> Execute and Check if there's an Error
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }

    public function FetchTags($ThesisID){
        // Prepare and Bind 
        $stmt = $this->connection->prepare("SELECT `tag` from tags WHERE Thesis_id LIKE ?");
        $stmt->bind_param("i", $ThesisID);

        //Nothing follows-> Execute and Check if there's an Error
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        
        //Bind variables to prepared statement
        $stmt->bind_result($tag);
        
        $tags = array();
        //Fetch values and push to array.
        while($stmt->fetch()){
            array_push($tags,$tag);
        }

        $stmt->close();
        return $tags;
    }


    public function FetchThesisBaseOnTags($search){
        // Prepare and Bind 
        $stmt = $this->connection->prepare("SELECT DISTINCT `Thesis_id` FROM `tags` WHERE `tag` LIKE ? ORDER BY `Thesis_id` DESC");
        $search = "%$search%";
        $stmt->bind_param("s", $search);

        //Nothing follows-> Execute and Check if there's an Error
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        
        //Bind variables to prepared statement
        $stmt->bind_result($id);
        
        $Thesis_id = array();
        //Fetch values and push to array.
        while($stmt->fetch()){
            array_push($Thesis_id,$id);
        }
        $stmt->close();
 
        $Thesis = array();
        foreach ($Thesis_id as $t_id) {
            $temp = $this->FetchThesis($t_id);
            array_push($Thesis,$temp);
        }
        return $Thesis;
    }



    public function FetchLastThesis(){
        $stmt = $this->connection->prepare("SELECT id FROM Thesis ORDER BY id DESC LIMIT 1");

        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->bind_result($id);
        $stmt->fetch();
        $stmt->close();
        return $id;
    }

    public function FetchThesis($id){
        $stmt = $this->connection->prepare("SELECT * FROM Thesis WHERE id LIKE ?");
        
        $stmt->bind_param("i", $id);
        
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->bind_result($id,$title,$author,$abstract,$published_date,$school,$course);
        $stmt->fetch();
        $stmt->close();
        return array("id"=>$id,"title"=>$title,"author"=>$author, "abstract"=>$abstract, "published_date"=>$published_date, "school"=>$school,"course"=>$course);
    }

    public function FetchThesisPending($id){
        $stmt = $this->connection->prepare("SELECT * FROM thesis_pending WHERE id LIKE ?");
        
        $stmt->bind_param("i", $id);
        
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->bind_result($id,$title,$author,$abstract,$published_date,$school,$course,$date_submitted);
        $stmt->fetch();
        $stmt->close();
        return array("id"=>$id,"title"=>$title,"author"=>$author, "abstract"=>$abstract, "published_date"=>$published_date, "school"=>$school,"course"=>$course);
    }

    public function FetchThesisTitlePending($search){
        $Thesis = array();
        $search = "%".$search."%";

        $title  = mysqli_real_escape_string($this->connection,$search);
   

        $sql = "SELECT * FROM `thesis_pending` WHERE `title` LIKE '$title' ORDER BY `date_submitted` DESC";
        $result = $this->connection->query($sql);
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                $arr = array( "id" => $row["id"],
                            "title" => $row["title"],
                            "author" => $row["author"],
                            "abstract" => $row["abstract"],
                            "published_date" => $row["published_date"],
                            "school" => $row["school"],
                            "course" => $row["course"],
                            "date_submitted" => $row["date_submitted"]    
                        );
				array_push($Thesis,$arr);
            }
            
        }
        return $Thesis;
    }

    //Use in search
    public function FetchThesisTitle($search){
        $Thesis = array();
        $search["title"] = "%".$search["title"]."%";

        $title  = mysqli_real_escape_string($this->connection,$search["title"]);
        $school	= mysqli_real_escape_string($this->connection,$search["school"]);
        $course = mysqli_real_escape_string($this->connection,$search["course"]);

        $sql = "SELECT * FROM `Thesis` WHERE `title` LIKE '$title' ".(($school != "") ? " AND `school` LIKE $school ":"").(($course != "") ? " AND `course` LIKE $course ":""). "ORDER BY `id` DESC";
        $result = $this->connection->query($sql);
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                $arr = array( "id" => $row["id"],
                            "title" => $row["title"],
                            "author" => $row["author"],
                            "abstract" => $row["abstract"],
                            "published_date" => $row["published_date"],
                            "school" => $row["school"],
                            "course" => $row["course"]);
				array_push($Thesis,$arr);
            }
            
        }
        return $Thesis;
    }

    //Use in Deletion
    public function FetchThesisBaseOnSchool($school){
        $Thesis = array();
        $stmt = $this->connection->prepare("SELECT * FROM Thesis WHERE school LIKE ?");
        
        $stmt->bind_param("i", $school);

        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        //Bind variables to prepared statement
        $stmt->bind_result($id,$title,$author,$abstract,$published_date,$school_id,$course);
        
        //Fetch values and push to array.
        while($stmt->fetch()){
            $t = array("id"=>$id,"title"=>$title,"author"=>$author,"abstract"=>$abstract,"published_date"=>$published_date,"school"=>$school_id,"course"=>$course);
            array_push($Thesis,$t);
        }

        $stmt->close();
        return $Thesis;
    }

    //Use in Deletion
    public function FetchThesisBaseOnCourse($course){
        $Thesis = array();
        $stmt = $this->connection->prepare("SELECT * FROM Thesis WHERE course LIKE ?");
        
        $stmt->bind_param("i", $course);

        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        //Bind variables to prepared statement
        $stmt->bind_result($id,$title,$author,$abstract,$published_date,$school_id,$course);
        
        //Fetch values and push to array.
        while($stmt->fetch()){
            $t = array("id"=>$id,"title"=>$title,"author"=>$author,"abstract"=>$abstract,"published_date"=>$published_date,"school"=>$school_id,"course"=>$course);
            array_push($Thesis,$t);
        }

        $stmt->close();
        return $Thesis;
    }

    public function DeleteThesis($id){
        //Thesis
        $stmt = $this->connection->prepare("DELETE FROM `Thesis` WHERE `id` LIKE ?");
        $stmt->bind_param("i",$id);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        $this->DeleteTags($id);
    }

    public function DeleteThesisPending($id){
        //Thesis
        $stmt = $this->connection->prepare("DELETE FROM `thesis_pending` WHERE `id` LIKE ?");
        $stmt->bind_param("i",$id);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }

    public function DeleteTags($id){
        //TAGS
        $stmt = $this->connection->prepare("DELETE FROM `tags` WHERE `Thesis_id` LIKE ?");
        $stmt->bind_param("i",$id);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }
}

?>