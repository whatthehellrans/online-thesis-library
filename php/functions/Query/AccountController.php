<?php
    
//include_once(dirname(__FILE__)."/../Classes/School.php");
// Will be using arrays instead of object :/

class AccountController
{
    public $connection;

    public function __construct()
    {
        include_once(dirname(__FILE__)."/../Config.php");
        $this->connection = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        // Check connection
        if ($this->connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
    }

    public function __destruct()
    {
        $this->connection->close();
    }


    public function InsertAccount($username,$password,$fname,$lname,$email,$access,$school){
        // Prepare and Bind
        $stmt = $this->connection->prepare("INSERT INTO Users VALUES(NULL,?,?,?,?,?,?,?)");
        $stmt->bind_param("ssssssi", $username,$password,$fname,$lname,$email,$access,$school);
        // Execute and check for error.
        if(!$stmt->execute()){
            //echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }

    public function UpdateAccount($account){
        $stmt = $this->connection->prepare("UPDATE  `Users`
                                            SET     `username` = ?,
                                                    `password` = ?,
                                                    `fname`    = ?,
                                                    `lname`    = ?,
                                                    `email`    = ?,
                                                    `access`   = ?,
                                                    `school_id`= ?
                                            WHERE   `Users`.`id` = ?");
        $stmt->bind_param("ssssssii",$account["username"],$account["password"],$account["fname"],$account["lname"],$account["email"],$account["access"],$account["school"],$account["id"]);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }

    public function FetchAccount($id){
        $stmt = $this->connection->prepare("SELECT * FROM Users WHERE id LIKE ?");

        $stmt->bind_param("i", $id);
        
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->bind_result($id,$username,$password,$fname,$lname,$email,$access,$school);
        $stmt->fetch();
        $stmt->close();
        return array("id"=>$id,"username"=>$username,"password"=>$password,"fname"=>$fname,"lname"=>$lname,"email"=>$email, "access"=>$access, "school"=>$school);
    }



    public function FetchLoginAccount($username,$password){
        $stmt = $this->connection->prepare("SELECT * FROM Users WHERE `username` LIKE ? AND `password` LIKE ?");
        $stmt->bind_param("ss",$username,$password);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        $stmt->bind_result($id,$username,$password,$fname,$lname,$email,$access,$school);
        $stmt->fetch();
        $stmt->close();
        return array("id"=>$id,"username"=>$username,"password"=>$password,"fname"=>$fname,"lname"=>$lname,"email"=>$email, "access"=>$access, "school"=>$school);
    }

    public function FetchAllAccounts(){
        $accounts = array();
        $stmt = $this->connection->prepare("SELECT * FROM Users");
    
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        //Bind variables to prepared statement
        $stmt->bind_result($id,$username,$password,$fname,$lname,$email,$access,$school_id);
        
        //Fetch values and push to array.
        while($stmt->fetch()){
            $account = array("id" => $id,"username" => $username,
                            "password" => $password, "fname"=> $fname,
                            "lname" => $lname, "email" => $email,
                            "access" => $access,
                            "school_id" => $school_id);
            array_push($accounts,$account);
        }

        $stmt->close();
        return $accounts;
    }

    public function FetchSearchAccount($search){
        $accounts = array();

        $searchparam = "%$search%";
        $stmt = $this->connection->prepare("SELECT * FROM Users WHERE fname LIKE ? OR lname LIKE ?");
        $stmt->bind_param("ss", $searchparam,$searchparam);
        
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        //Bind variables to prepared statement
        $stmt->bind_result($id,$username,$password,$fname,$lname,$email,$access,$school_id);
        
        //Fetch values and push to array.
        while($stmt->fetch()){
            $account = array("id" => $id,"username" => $username,
                            "password" => $password, "fname"=> $fname,
                            "lname" => $lname, "email" => $email,
                            "access" => $access,
                            "school_id" => $school_id);
            array_push($accounts,$account);
        }

        $stmt->close();
        return $accounts;
    }

    //Use in School Deletion
    public function FetchAllAccountsBySchool($school_id){
        $accounts = array();
        $stmt = $this->connection->prepare("SELECT * FROM Users WHERE `school_id` LIKE ?");
        $stmt->bind_param("i",$school_id);
    
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        //Bind variables to prepared statement
        $stmt->bind_result($id,$username,$password,$fname,$lname,$email,$access,$school_id);
        
        //Fetch values and push to array.
        while($stmt->fetch()){
            $account = array("id" => $id,"username" => $username,
                            "password" => $password, "fname"=> $fname,
                            "lname" => $lname, "email" => $email,
                            "access" => $access,
                            "school_id" => $school_id);
            array_push($accounts,$account);
        }

        $stmt->close();
        return $accounts;
    }

    public function DeleteAccount($id){
        $stmt = $this->connection->prepare("DELETE FROM `Users` WHERE `id` LIKE ?");
        $stmt->bind_param("i",$id);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }



    //Controls
    public function SchoolUserAlreadyExists($schoolID)
    {
        $results = 0;

        //Check if there is an School User already with the School ID. (exlude the $user["id"])
        $query = "SELECT * FROM `users` WHERE `access` = 'School User' AND `school_id` = ?";
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("i",$schoolID);
        /* execute query */
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        /* store result */
        $stmt->store_result();
        $results = $stmt->num_rows;
        /* close statement */
        $stmt->close();
        return $results;
    }

    //Controls
    public function SchoolUserAlreadyExistsEdit($schoolID,$user)
    {
        $results = 0;

        //Check if there is an School User already with the School ID. (exlude the $user["id"])
        $query = "SELECT * FROM `users` WHERE `access` = 'School User' AND `school_id` = ? AND `id` NOT ?";
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("ii",$schoolID,$user);
        /* execute query */
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        /* store result */
        $stmt->store_result();
        $results = $stmt->num_rows;
        /* close statement */
        $stmt->close();
        return $results;
    }


    public function CheckEmailDuplicate($email)
    {

        //Check if there is an School User already with the School ID. (exlude the $user["id"])
        $query = "SELECT * FROM `users` WHERE `email` = ?";
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("s",$email);
        /* execute query */
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        /* store result */
        $stmt->store_result();
        $results = $stmt->num_rows;
        /* close statement */
        $stmt->close();
        if($results != 0)
        {
            return true;
        }else{
            return false;
        }

    }

    public function CheckEmailDuplicateEdit($email,$user)
    {
        //Check if there is an School User already with the School ID. (exlude the $user["id"])
        $query = "SELECT * FROM `users` WHERE `email` = ? AND `id` NOT LIKE ?";
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("si",$email,$user);
        /* execute query */
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        /* store result */
        $stmt->store_result();
        $results = $stmt->num_rows;
        /* close statement */
        $stmt->close();
        if($results != 0)
        {
            return true;
        }else{
            return false;
        }

    }
}

?>