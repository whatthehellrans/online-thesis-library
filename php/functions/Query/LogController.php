<?php
    
//include_once(dirname(__FILE__)."/../Classes/School.php");
// Will be using arrays instead of object :/

class LogController
{
    public $connection;

    public function __construct()
    {
        include_once(dirname(__FILE__)."/../Config.php");
        $this->connection = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        // Check connection
        if ($this->connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
    }

    public function __destruct()
    {
        $this->connection->close();
    }

    /****
    //LOG_Search
    *****/
    public function InsertLogSearch($userID,$thesisID){
        // Prepare and Bind 
        $stmt = $this->connection->prepare("INSERT INTO log_search(`user_id`,`thesis_id`) VALUES(?,?)");

        $stmt->bind_param("ii", $userID,$thesisID);

        //Nothing follows-> Execute and Check if there's an Error
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }

    public function FetchUserLogSearch($userID){
        $logs = array();
        $stmt = $this->connection->prepare("SELECT * FROM log_search WHERE `user_id` LIKE ? ORDER BY `time` DESC");
        $stmt->bind_param("i",$userID);
    
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        //Bind variables to prepared statement
        $stmt->bind_result($id,$time,$thesisID);
        
        //Fetch values and push to array.
        while($stmt->fetch()){
            $log = array("id" => $id,"time" => $time,"thesis_id"=>$thesisID);
            array_push($logs,$log);
        }

        $stmt->close();
        return $logs;
    }
    

    public function DeleteLogSearchByUser($userID){
        //TAGS
        $stmt = $this->connection->prepare("DELETE FROM `log_search` WHERE `user_id` LIKE ?");
        $stmt->bind_param("i",$userID);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }
    public function DeleteLogSearchByThesis($thesisID){
        //TAGS
        $stmt = $this->connection->prepare("DELETE FROM `log_search` WHERE `thesis_id` LIKE ?");
        $stmt->bind_param("i",$thesisID);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }


    /****
    //LOG_User
    *****/

    public function InsertLogUser($userID,$type){
        // Prepare and Bind 
        $stmt = $this->connection->prepare("INSERT INTO log_user (`user_id`,`type`) VALUES(?,?)");

        $stmt->bind_param("is", $userID,$type);

        //Nothing follows-> Execute and Check if there's an Error
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }
    public function FetchLogUser($userID){
        $logs = array();
        $stmt = $this->connection->prepare("SELECT * FROM log_user WHERE `user_id` LIKE ? ORDER BY `time` DESC");
        $stmt->bind_param("i",$userID);
    
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        //Bind variables to prepared statement
        $stmt->bind_result($id,$time,$type);
        
        //Fetch values and push to array.
        while($stmt->fetch()){
            $log = array("id" => $id,"time" => $time,"type"=>$type);
            array_push($logs,$log);
        }

        $stmt->close();
        return $logs;
    }

    public function DeleteLogUser($userID){
        $stmt = $this->connection->prepare("DELETE FROM `log_user` WHERE `user_id` LIKE ?");
        $stmt->bind_param("i",$userID);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }

    /****
    //LOG_Thesis
    *****/
    public function InsertLogThesis($thesisID,$userID,$type){
        // Prepare and Bind 
        $stmt = $this->connection->prepare("INSERT INTO log_study (`thesis_id`,`user_iD`,`type`) VALUES(?,?,?)");

        $stmt->bind_param("iis", $thesisID,$userID,$type);

        //Nothing follows-> Execute and Check if there's an Error
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }

    public function FetchLogThesis($userID){
        $logs = array();
        $stmt = $this->connection->prepare("SELECT * FROM log_study WHERE `user_id` LIKE ? ORDER BY `time` DESC");
        $stmt->bind_param("i",$userID);
    
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        //Bind variables to prepared statement
        $stmt->bind_result($thesisID,$userID,$time,$type);
        
        //Fetch values and push to array.
        while($stmt->fetch()){
            $log = array("thesis_id" => $thesisID,"user_id" => $userID ,"time" => $time,"type"=>$type);
            array_push($logs,$log);
        }

        $stmt->close();
        return $logs;
    }

    public function DeleteLogStudyByThesis($thesisID){
        $stmt = $this->connection->prepare("DELETE FROM `log_study` WHERE `thesis_id` LIKE ?");
        $stmt->bind_param("i",$thesisID);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }
    public function DeleteLogStudyByUser($userID){
        $stmt = $this->connection->prepare("DELETE FROM `log_study` WHERE `user_id` LIKE ?");
        $stmt->bind_param("i",$userID);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }

}

?>