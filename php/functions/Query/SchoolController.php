<?php
    
//include_once(dirname(__FILE__)."/../Classes/School.php");

class SchoolController
{
    public $connection;

    public function __construct()
    {
        include_once(dirname(__FILE__)."/../Config.php");
        $this->connection = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        // Check connection
        if ($this->connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
    }

    public function __destruct()
    {
        $this->connection->close();
    }

    public function InsertSchool($name,$address,$contact,$courses){
        // Prepare and Bind
        $stmt = $this->connection->prepare("INSERT INTO Schools VALUES(NULL,?,?,?)");
        $stmt->bind_param("sss", $name,$address,$contact);

        // Execute and check for error.
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $lastSchoolID = $stmt->insert_id;
        $stmt->close();
        foreach ($courses as $course) {
            $this->InsertSchoolCourses($lastSchoolID,$course);
        }
        return true;
    }

    public function InsertSchoolCourses($schoolID,$courseID)
    {
        $query = "INSERT INTO `schools_courses` VALUES(?,?)";
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("ii",$schoolID,$courseID);
        // Execute and check for error.
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }
    
    public function DeleteSchoolCourses($schoolID)
    {
        $query = "DELETE FROM `schools_courses` WHERE `school_id` LIKE ?";
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("i",$schoolID);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }
    
    


    public function UpdateSchool($school){
        $stmt = $this->connection->prepare("UPDATE `Schools`
                                            SET    `name` = ?,
                                                   `address` = ?,
                                                   `contact` = ?
                                            WHERE  `Schools`.`id` = ?");
        $stmt->bind_param("sssi",$school["name"],$school["address"],$school["contact"],$school["id"]);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        
        $this->DeleteSchoolCourses($school["id"]);
        foreach ($school["courses"] as $courses) {
            $this->InsertSchoolCourses($school["id"],$courses);
        }

        return true;
    }

    public function FetchAllSchool(){
        $Schools = array();
        $stmt = $this->connection->prepare("SELECT * FROM Schools");
    
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        //Bind variables to prepared statement
        $stmt->bind_result($id,$name,$address,$contact);
        
        //Fetch values and push to array.
        while($stmt->fetch()){
            array_push($Schools,array("id" => $id, "name" => $name, "address" => $address, "contact" => $contact) );
        }

        $stmt->close();
        return $Schools;
    }

    public function FetchSchoolCourse($schoolID)
    {   
        $query = "SELECT `course_id` FROM schools_courses WHERE school_id LIKE ?";
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("i",$schoolID);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        $stmt->bind_result($courseID);
        
        $coursesArray = array();

        while($stmt->fetch())
        {
            array_push($coursesArray, $courseID);
        }
        $stmt->close();
        return $coursesArray;

    }

    public function FetchSchool($id){
        $stmt = $this->connection->prepare("SELECT * FROM Schools WHERE id LIKE ?");

        $stmt->bind_param("i", $id);
        
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->bind_result($id,$name,$address,$contact);
        $stmt->fetch();
        $stmt->close();

        $schoolCourses = $this->FetchSchoolCourse($id);
        return array("id"=>$id,"name"=>$name,"address"=>$address,"contact"=>$contact,"courses"=>$schoolCourses);
    }
    

    public function FetchLastSchoolID(){
        $stmt = $this->connection->prepare("SELECT * FROM Schools ORDER BY id DESC LIMIT 1");
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        $stmt->bind_result($id,$name,$address,$contact);
        $stmt->fetch();
        $stmt->close();
        return array("id"=>$id,"name"=>$name,"address"=>$address,"contact"=>$contact);
    }

    public function DeleteSchool($id){
        $stmt = $this->connection->prepare("DELETE FROM `Schools` WHERE `id` = ?");
        $stmt->bind_param("i",$id);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        $imgPath = "assets/img/Schools/";
        if(file_exists($imgPath.$id.".jpg")){
            unlink($imgPath.$id.".jpg");
        }else if(file_exists($imgPath.$id.".png")){
            unlink($imgPath.$id.".png");
        }
        return true;
    }
}

?>