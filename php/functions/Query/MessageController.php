<?php
    
//include_once(dirname(__FILE__)."/../Classes/School.php");
// Will be using arrays instead of object :/

class MessageController
{
    public $connection;

    public function __construct()
    {
        include_once(dirname(__FILE__)."/../Config.php");
        $this->connection = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        // Check connection
        if ($this->connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
    }

    public function __destruct()
    {
        $this->connection->close();
    }


    public function InsertMessage($message){
        // Prepare and Bind
        $stmt = $this->connection->prepare("INSERT INTO Messages(thesis_involved,message,sender_name,sender_contact) VALUES(?,?,?,?)");

        $stmt->bind_param("isss", $message["thesis"],$message["message"],$message["name"],$message["contact"]);

        // Execute and check for error.
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }

    public function InsertAppointment($id,$date){
        $stmt = $this->connection->prepare("INSERT INTO `appointment` VALUES (?,?)");
        $stmt->bind_param("is",$id,$date);
        // Execute and check for error.
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }



    public function FetchMessage($id){
        $stmt = $this->connection->prepare("SELECT * FROM `Messages` WHERE id LIKE ?");

        $stmt->bind_param("i", $id);
        
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->bind_result($id,$thesis,$message,$sender,$contact,$date,$status,$response);
        $stmt->fetch();
        $stmt->close();
        return array("id"=>$id,"thesis"=>$thesis,"message"=>"$message","sender"=>$sender,"contact"=>$contact,"date"=>$date,"status"=>$status,"response"=>$response);
    }

    //Fetch in Thesis Deletion
    public function FetchMessageWithThesisID($id){
        $stmt = $this->connection->prepare("SELECT * FROM `Messages` WHERE `thesis_involved` LIKE ?");

        $stmt->bind_param("i", $id);
        
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
   
        $mesArr = array();

        $stmt->bind_result($id,$thesis,$message,$sender,$contact,$date,$status,$response);
        while($stmt->fetch()){
            array_push($mesArr, array("id"=>$id,"thesis"=>$thesis,"message"=>"$message","sender"=>$sender,"contact"=>$contact,"date"=>$date,"status"=>$status,"response"=>$response));
        }
        $stmt->close();
        return $mesArr;
    }

    public function FetchAppointment($id){
        $stmt = $this->connection->prepare("SELECT * FROM `appointment` WHERE `message_id` LIKE ?");
        $stmt->bind_param("i",$id);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        $stmt->bind_result($msgid,$date);
        $stmt->fetch();
        $stmt->close();
        return array("id"=>$msgid,"date"=>$date);
    }

    

    public function UpdateStatus($id,$status){
        $stmt = $this->connection->prepare("UPDATE `Messages`
                                            SET `status` = ?
                                            WHERE `Messages`.`id` = ?");
        $stmt->bind_param("si",$status,$id);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }
    
    public function UpdateResponse($id,$response){
        $stmt = $this->connection->prepare("UPDATE `Messages`
                                            SET `response` = ?
                                            WHERE `Messages`.`id` = ?");
        $stmt->bind_param("si",$response,$id);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }

    public function DeleteAppointment($id){
        $stmt = $this->connection->prepare("DELETE FROM `appointment` WHERE `appointment`.`message_id` = ?");
        $stmt->bind_param("i",$id);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }

    public function DeleteMessage($id){
        $stmt = $this->connection->prepare("DELETE FROM `Messages` WHERE `id` = ?");
        $stmt->bind_param("i",$id);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }
    
    

    

    //Use in Inbox search
    public function FetchSearchMessage($searchdate){
        $Messages = array();

        $date   = mysqli_real_escape_string($this->connection,$searchdate);
        

        //$sql = "SELECT * FROM `Messages` WHERE `date` LIKE '$date' ".(($school != "") ? " AND `school` LIKE $school ":"");
        $sql = "SELECT * FROM `Messages` WHERE `date_sent` LIKE '$date%'";
        $result = $this->connection->query($sql);
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                $arr = array(   "id"      => $row["id"],
                                "thesis"  => $row["thesis_involved"],
                                "message" => $row["message"],
                                "sender"  => $row["sender_name"],
                                "contact" => $row["sender_contact"],
                                "date"    => $row["date_sent"],
                                "status"  => $row["status"],
                                "response"=> $row["response"]);
				array_push($Messages,$arr);
            }
            
        }
        return $Messages;
    }

    //Use in Appointment Search
    public function FetchAppointmentMessage($searchdate,$response){
        $Messages = array();

        $date   = mysqli_real_escape_string($this->connection,$searchdate);
        

        //$sql = "SELECT * FROM `Messages` WHERE `date` LIKE '$date' ".(($school != "") ? " AND `school` LIKE $school ":"");
        $sql = "SELECT * FROM `Messages` WHERE `date_sent` LIKE '$date%' AND `response` LIKE '$response'";
        $result = $this->connection->query($sql);
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                $arr = array(   "id"      => $row["id"],
                                "thesis"  => $row["thesis_involved"],
                                "message" => $row["message"],
                                "sender"  => $row["sender_name"],
                                "contact" => $row["sender_contact"],
                                "date"    => $row["date_sent"],
                                "status"  => $row["status"],
                                "response"=> $row["response"]);
				array_push($Messages,$arr);
            }
            
        }
        return $Messages;
    }

}

?>