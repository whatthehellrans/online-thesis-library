<?php
    
//include_once(dirname(__FILE__)."/../Classes/School.php");
// Will be using arrays instead of object :/

class CourseController
{
    public $connection;

    public function __construct()
    {
        include_once(dirname(__FILE__)."/../Config.php");
        $this->connection = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        // Check connection
        if ($this->connection->connect_error) {
            die("Connection failed: " . $connection->connect_error);
        }
    }

    public function __destruct()
    {
        $this->connection->close();
    }


    public function InsertCourse($name,$abbr){
        // Prepare and Bind
        $stmt = $this->connection->prepare("INSERT INTO Courses VALUES(NULL,?,?)");
        $stmt->bind_param("ss", $name,$abbr);
        // Execute and check for error.
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }
    

    public function UpdateCourse($course){
        $stmt = $this->connection->prepare("UPDATE `Courses`
                                            SET `full_name` = ?,
                                                `abbr` = ?
                                            WHERE `Courses`.`id` = ?");
        $stmt->bind_param("ssi",$course["name"],$course["abbr"],$course["id"]);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }

    public function FetchCourse($id){
        $stmt = $this->connection->prepare("SELECT * FROM Courses WHERE id LIKE ?");

        $stmt->bind_param("i", $id);
        
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->bind_result($id,$name,$abbr);
        $stmt->fetch();
        $stmt->close();
        return array("id"=>$id,"name"=>$name,"abbr"=>"$abbr");
    }

    

    public function FetchAllCourse(){
        $Courses = array();
        $stmt = $this->connection->prepare("SELECT * FROM Courses");
    
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        //Bind variables to prepared statement
        $stmt->bind_result($id,$name,$abbr);
        
        //Fetch values and push to array.
        while($stmt->fetch()){
            $course = array("id"=>$id,"name"=>$name,"abbr"=>$abbr);
            array_push($Courses,$course);
        }

        $stmt->close();
        return $Courses;
    }

    public function DeleteCourse($id){
        $stmt = $this->connection->prepare("DELETE FROM `Courses` WHERE `id` LIKE ?");
        $stmt->bind_param("i",$id);
        if(!$stmt->execute()){
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $stmt->close();
            return false;
        }
        $stmt->close();
        return true;
    }

}

?>