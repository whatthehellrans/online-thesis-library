<?php
    include_once("php/functions/misc.php");
    include_once("php/functions/Query/SchoolController.php");
    $SchoolController = new SchoolController();
    $schools = $SchoolController->FetchAllSchool();
    $SchoolController = NULL; // close sql connection

    $errors = array("username" => "","password" => "","fname"=>"","lname"=>"","email"=>"","access" => "", "school" => "");
    $input = array("username" => "","password" => "","fname"=>"","lname"=>"","email"=>"","access" => "", "school" => "");

    $success = "";

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        include_once("php/functions/Query/AccountController.php");
        $AccountController = new AccountController();
            
  
        //Clean all input before passing it to the array
        $input["username"]  = cleanInput($_POST["username"]);
        $input["password"]  = cleanInput($_POST["password"]);
        $input["fname"]  = cleanInput($_POST["fname"]);
        $input["lname"]  = cleanInput($_POST["lname"]);
        $input["email"]  = cleanInput($_POST["email"]);
        $input["access"]    = cleanInput($_POST["access"]);
        $input["school"]    = cleanInput($_POST["school"]);

        //Validate all inputs
        $errors["username"] = checkEmpty($input["username"],"Username");
        $errors["password"] = checkEmpty($input["password"],"Password");
        $errors["fname"]    = checkEmpty($input["fname"],"First name");
        $errors["lname"]    = checkEmpty($input["lname"],"Last name");
        $errors["fname"]    = checkHasDigits($input["fname"],"First name");
        $errors["lname"]    = checkHasDigits($input["lname"],"Last name");
        $errors["email"]    = checkEmpty($input["email"],"Email");
        $errors["access"]   = checkEmpty($input["access"],"Access");
        $errors["school"]   = checkEmpty($input["school"],"School");
        if($input["access"] == MOD)
        {
            $errors["school"]   = ($AccountController->SchoolUserAlreadyExists($input["school"])) ? "The school has already a School User":"";
        }


        if($AccountController->CheckEmailDuplicate($input["email"]))
        {
            $errors["email"] = "Email is already used.";
        }
        
        //Check all Errors (If there is any)
        $validated = true;
        foreach ($errors as $error) {
            if($error != ""){
                $validated = false;
            }
        }

        if($validated){
            $success = $AccountController->InsertAccount($input["username"], $input["password"], $input["fname"],$input["lname"],$input["email"], $input["access"], $input["school"]);
            if($success){
                $success = "Account has been added.";
                $input = returnEmptyVar($input); // --- Clear the input so the default value of form is empty also. 
            }else{
                $success = '<p class="text-danger">There was an error adding this account</p>';
            }
        }

    }
?>

        <div class="card shadow ">
            <div class="card-header py-3">
                <p class="text-primary m-0 font-weight-bold">Please fill up this form</p>
                <?php returnSuccess($success); ?>
            </div>
            <div class="card-body ">
                <form action="" method="post">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="username">Username </label>
                            <input class="form-control <?php returnIsInvalid($errors["username"]); ?>" type="text" id="username" name="username" value=<?php echo $input["username"];?> >
                            <?php returnErrorMsg($errors["username"]);?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="password">Password </label>
                            <input class="form-control <?php returnIsInvalid($errors["password"]); ?> " type="password" id="password" name="password" value=<?php echo $input["password"];?>>
                            <?php returnErrorMsg($errors["password"]);?>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="username">First name </label>
                            <input class="form-control <?php returnIsInvalid($errors["fname"]); ?> " type="text" id="fname" name="fname" value=<?php echo $input["fname"];?> >
                            <?php returnErrorMsg($errors["fname"]);?>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="username">Last name </label>
                            <input class="form-control <?php returnIsInvalid($errors["lname"]); ?>" type="text" id="lname" name="lname" value=<?php echo $input["lname"];?> >
                            <?php returnErrorMsg($errors["lname"]);?>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="username">Email </label>
                            <input class="form-control <?php returnIsInvalid($errors["email"]); ?> " type="text" id="email" name="email" value=<?php echo $input["email"];?> >
                            <?php returnErrorMsg($errors["email"]);?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="school">Access</label>
                            <select class="form-control <?php returnIsInvalid($errors["access"]); ?> " id="access" name="access">
                                <option <?php returnStrTrue($input["access"], MOD, "selected"); ?> value="School User">School User</option>
                                <option <?php returnStrTrue($input["access"], NORM, "selected"); ?> value="Normal User">Normal User</option>
                            </select>
                            <?php returnErrorMsg($errors["access"]);?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="school">School <?php returnErrorMsg($errors["school"]);?></label>
                            <select class="form-control" id="school" name="school">
                                <?php foreach ($schools as $school) :?>
                                    <option <?php returnStrTrue($input["school"], $school["id"], "selected"); ?> value="<?php echo $school["id"];?>"><?php echo $school["name"]?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 m-auto">
                        <input type="submit" class="form-control btn btn-secondary"value="Submit">
                    </div>
                </div>
                
                </form>    
            </div>
        </div>