<?php
    include_once("php/functions/misc.php");
    if (!isset($_GET["view"]) || empty($_GET["view"])) {
        //echo 'Cannot find ID | <a href="index.php">Click here to return</a>';
        include_once("404.php");
        exit();
    }
    include_once("php/functions/Query/MessageController.php");
    include_once("php/functions/Query/ThesisController.php");
    include_once("php/functions/Query/SMSController.php");
    $SMSController = new SMSController();
    $MessageController = new MessageController();
    $ThesisController  = new ThesisController();

    $success = "";
    $errors = array("date"=>"","time"=>"");
    $input = array("date"=>date("Y-m-d"),"time"=> date("h:i"));

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(isset($_POST["decline"])){
            //SMS notification 
            $message = $MessageController->FetchMessage($_GET["view"]);
            $title   = $ThesisController->FetchThesis($message["thesis"])["title"];
            $contact = $message["contact"];
            $smsMessage = "Online ICT Library System: Your request for appointment about $title has been declined. Automated Message DO NOT REPLY.";
            $SMSController->sendSms($smsMessage,$contact);
            $MessageController->UpdateResponse($_GET["view"],"Declined");
        }else{
            //Clean all input before passing it to the array
            $input["date"]  = cleanInput($_POST["date"]);
            $input["time"]  = cleanInput($_POST["time"]);
            $errors["date"] = checkEmpty($input["date"],"Date");
            $errors["time"] = checkEmpty($input["time"],"Time");
            $datetime = "";

            if($errors["date"] == "" && $errors["time"] == ""){
                $datetime = $input["date"]." ".$input["time"].":00";
                $dateTimeStamp = strtotime($datetime);
                if($dateTimeStamp < time()){
                    $errors["date"] = "You have entered invalid time.";
                }
            }
            
            //Check all Errors (If there is any)
            $validated = true;
            foreach ($errors as $error) {
                if($error != ""){
                    $validated = false;
                }
            }

            if($validated){
                //Update message response to Accepted
                //Insert new Entry to Appointment
                $MessageController->UpdateResponse($_GET["view"],"Accepted");
                $MessageController->InsertAppointment($_GET["view"],$datetime);
                $success = "Meeting has been set";

                //SMS notification 
                $message = $MessageController->FetchMessage($_GET["view"]);
                $title   = $ThesisController->FetchThesis($message["thesis"])["title"];
                $contact = $message["contact"];
                $smsMessage = "Online ICT Library System: Your request for appointment about $title has been Accepted and will be on $datetime. Automated Message DO NOT REPLY.";
                $SMSController->sendSms($smsMessage,$contact);
            }
        }
        
        
    }


    $message = $MessageController->FetchMessage($_GET["view"]);
    if (checkUser(MOD) && $message["status"] == "Unread") {
        $MessageController->UpdateStatus($message["id"], "Read");
    }

?>
        <div class="card shadow ">
            <div class="card-header py-3">
                <p class="text-primary m-0 ">Research Study Involved: <span class="font-weight-bold"> <?php echo $ThesisController->FetchThesis($message["thesis"])["title"]; ?> <span></p>

            </div>
            <div class="card-body">
                <form action="" method="post">
                <div class="row">
                    <div class="col-md-8">
                        <h5>Sender Name: <?php echo $message["sender"];?></h5>
                        <h6 class="m-0">Contact: <?php echo $message["contact"];?></h6>
                        <small>Date sent: <span class="text-danger"> <?php echo $message["date"];?> </span> </small>
                    </div>
                    <div class="col-md-8 mt-3">
                        <h5>Message:</h5>
                        <p><?php echo $message["message"];?></p>
                    </div>

                </div>
                <!-- TODO: add set appointment Page -->
                <hr>
                <hr>
                <?php returnSuccess($success); ?>


                <h3><?php 
                    if($message["response"] == "Accepted"){
                        $appointmentDate = strtotime( $MessageController->FetchAppointment($message["id"])["date"] );
                        echo 'Meeting '. (($appointmentDate < time())? "was":"on") . ' ( <span class="text-info">'.$MessageController->FetchAppointment($message["id"])["date"] ."</span> )";

                    }else if($message["response"] == "Declined"){
                        echo 'This Request has been <span class="text-danger">Declined</span>.';
                    }else{
                        echo "Set an Appointment";
                    }
                   
                ?></h3>
                <form action="" method="post">
                    <?php if($message["response"] == "Unset") : ?>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="published_date">Date <?php returnErrorMsg($errors["date"]); ?> </label>
                                <input class="form-control" type="date" name="date" id="published_date" value="<?php echo $input["date"];?>">
                            </div>
                        </div> 
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="published_date">Time <?php returnErrorMsg($errors["time"]); ?></label>
                                <input class="form-control" type="time" name="time" id="published_date" value="<?php echo $input["time"];?>">
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <input class="form-control btn btn-dark px-5" type="submit" value="Set Appointment">
                            </div>
                        </div>
                        <div class="col-md-3" >
                            <div class="form-group">
                                <input class="form-control btn btn-dark px-5" type="submit" name="decline" value="Decline Request">
                            </div>
                        </div>
                    </div>
                    <?php endif ?>
                </form>  

                 
            </div>
        </div>