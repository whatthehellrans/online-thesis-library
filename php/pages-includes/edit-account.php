<?php 
    if(!isset($_GET["edit"]) || empty($_GET["edit"])){
        //echo 'Cannot find ID | <a href="index.php">Click here to return</a>';
        include_once("404.php");
        exit();
    }

    include_once("php/functions/misc.php");
    include_once("php/functions/Query/AccountController.php");
    include_once("php/functions/Query/SchoolController.php");
    $SchoolController = new SchoolController();
    $schools = $SchoolController->FetchAllSchool();
    $SchoolController = NULL; // close sql connection

    $AccountController = new AccountController();
    $account = $AccountController->FetchAccount($_GET["edit"]);

    $success = "";
    $errors = array("username" => "","password" => "","fname"=>"","lname"=>"","email"=>"", "access" => "", "school" => "");

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        include_once("php/functions/Query/AccountController.php");
        $AccountController = new AccountController();
        
        //Clean all input before passing it to the array
        $account["username"]  = cleanInput($_POST["username"]);
        $account["password"]  = cleanInput($_POST["password"]);
        $account["fname"]     = cleanInput($_POST["fname"]);
        $account["lname"]     = cleanInput($_POST["lname"]);
        $account["email"]     = cleanInput($_POST["email"]);
        
        $account["access"]    = cleanInput($_POST["access"]);
        $account["school"]    = cleanInput($_POST["school"]);

        //Validate all inputs
        $errors["username"] = checkEmpty($account["username"],"Username");
        $errors["password"] = checkEmpty($account["password"],"Password");
        $errors["fname"]    = checkEmpty($account["fname"],"First name");
        $errors["lname"]    = checkEmpty($account["lname"],"Last name");
        $errors["fname"]    = checkHasDigits($account["fname"],"First name");
        $errors["lname"]    = checkHasDigits($account["lname"],"Last name");
        $errors["email"]    = checkEmpty($account["email"],"Email");   
        $errors["access"]   = checkEmpty($account["access"],"Access");
        $errors["school"]   = checkEmpty($account["school"],"School");
        if($account["access"] == MOD)
        {
            $errors["school"]   = ($AccountController->SchoolUserAlreadyExists($account["school"],$account["id"] )) ? "The school has already a School User":"";
        }

        if($AccountController->CheckEmailDuplicateEdit($account["email"], $account["id"] ))
        {
            $errors["email"] = "Email is already used.";
        }

        //Check all Errors (If there is any)
        $validated = true;
        foreach ($errors as $error) {
            if($error != ""){
                $validated = false;
            }
        }

        if($validated){
            if($AccountController->UpdateAccount($account)){
                if($_SESSION["user_id"] == $account["id"]){
                    $_SESSION["username"] = $account["username"];
                    $_SESSION["fname"]    = $account["fname"];
                    $_SESSION["lname"]    = $account["lname"];
                    $_SESSION["access"]   = $account["access"];
                    $_SESSION["school"]   = $account["school"];
                }
                $success = "Account has been modified.";
                $input = returnEmptyVar($input); // --- Clear the input so the default value of form is empty also. 
            }else{
                $success = '<p class="text-danger">There was an error adding this account</p>';
            }
        }
    }

?>
        
        <div class="card shadow">
            <div class="card-header py-3">
                <p class="text-primary m-0 font-weight-bold">Modify a User</p>
                <?php returnSuccess($success); ?>
            </div>
            <div class="card-body ">
            <form action="" method="post">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="username">Username </label>
                            <input class="form-control <?php returnIsInvalid($errors["username"]); ?>" type="text" id="username" name="username" value=<?php echo $account["username"];?> >
                            <?php returnErrorMsg($errors["username"]);?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="password">Password </label>
                            <input class="form-control <?php returnIsInvalid($errors["password"]); ?> " type="password" id="password" name="password" value=<?php echo $account["password"];?>>
                            <?php returnErrorMsg($errors["password"]);?>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="username">First name </label>
                            <input class="form-control <?php returnIsInvalid($errors["fname"]); ?> " type="text" id="fname" name="fname" value=<?php echo $account["fname"];?> >
                            <?php returnErrorMsg($errors["fname"]);?>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="username">Last name </label>
                            <input class="form-control <?php returnIsInvalid($errors["lname"]); ?>" type="text" id="lname" name="lname" value=<?php echo $account["lname"];?> >
                            <?php returnErrorMsg($errors["lname"]);?>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="username">Email </label>
                            <input class="form-control <?php returnIsInvalid($errors["email"]); ?> " type="text" id="email" name="email" value=<?php echo $account["email"];?> >
                            <?php returnErrorMsg($errors["email"]);?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="school">Access</label>
                            <select class="form-control <?php returnIsInvalid($errors["access"]); ?> " id="access" name="access">
                                <?php if($_SESSION["user_id"]  == $account["id"]): ?>
                                    <option <?php returnStrTrue($account["access"], ADMIN, "selected"); ?> value="Administrator">Administrator</option>
                                <?php endif; ?>
                                <option <?php returnStrTrue($account["access"], MOD, "selected"); ?> value="School User">School User</option>
                                <option <?php returnStrTrue($account["access"], NORM, "selected"); ?> value="Normal User">Normal User</option>
                            </select>
                            <?php returnErrorMsg($errors["access"]);?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="school">School <?php returnErrorMsg($errors["school"]);?></label>
                            <select class="form-control" id="school" name="school">
                                <?php foreach ($schools as $school) :?>
                                    <option <?php returnStrTrue($account["school"], $school["id"], "selected"); ?> value="<?php echo $school["id"];?>"><?php echo $school["name"]?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 m-auto">
                        <input type="submit" class="form-control btn btn-secondary"value="Submit">
                    </div>
                </div>
                
                </form>    
            </div>
        </div>