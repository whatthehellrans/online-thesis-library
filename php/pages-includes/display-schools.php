<?php 
    include_once("php/functions/Query/SchoolController.php");
    $SchoolController = new SchoolController();
    $schools = $SchoolController->FetchAllSchool();

    if(isset($_GET["delete"]) && $_GET["delete"] != ""){
        include_once("php/functions/Query/AccountController.php");
        include_once("php/functions/Query/ThesisController.php");
        include_once("php/functions/Query/MessageController.php");
        include_once("php/functions/Query/LogController.php");

        $AccountController = new AccountController();
        $ThesisController  = new ThesisController();
        $MessageController = new MessageController();
        $LogController = new LogController();

        $idToDelete = $_GET["delete"];

        $accounts   = $AccountController->FetchAllAccountsBySchool($idToDelete);
        $thesis     = $ThesisController->FetchThesisBaseOnSchool($idToDelete);
        //Deleting All Accounts Related to this School
        foreach ($accounts as $acc) {
            $AccountController->DeleteAccount($acc["id"]);
            $LogController->DeleteLogSearchByUser($acc["id"]);
            $LogController->DeleteLogStudyByUser($acc["id"]);
            $LogController->DeleteLogUser($acc["id"]);
        }
        //End of Accounts

        //Start of Thesis Deletion
        foreach ($thesis as $thes) {
            //Start of Message Deletion
            $messages = $MessageController->FetchMessageWithThesisID($thes["id"]);
            foreach ($messages as $msg) {
                //Deleting Messages and Appointments
                $MessageController->DeleteAppointment($msg["id"]);
                $MessageController->DeleteMessage($msg["id"]);
            }
            //End of Message Deletion
            $ThesisController->DeleteThesis($thes["id"]);
            $LogController->DeleteLogSearchByThesis($thes["id"]);
            $LogController->DeleteLogStudyByThesis($thes["id"]);
        }
        //End of Thesis Deletion
        
        if($SchoolController->DeleteSchool($idToDelete)){
            echo "School Has been Deleted. ";
            echo '<a href="schools.php">Click here to go back.</a>';
         }else{
            echo "There was an error deleting this School.";
         }
         exit();
    }

    $SchoolController = NULL;

?>
    <div class="card shadow ">
        <div class="card-header py-3">
            <a class="btn btn-primary" href="schools.php?option=1">Add new School</a>
        </div>
        <div class="card-body ">

            <!-- TODO ADD SEARCH ACCOUNT -->
            <!-- <form action="" method="get">
                <div class="row">
                    <div class="col-md-6">
                            <div class="input-group md-form form-sm form-1 pl-0">
                                <div class="input-group-prepend">
                                    <button class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search text-white"></i></button>
                                </div>
                                <input class="form-control my-0 py-1" type="text" placeholder="Title Search" name="search">
                            </div>
                    </div>
                    <div class="col-md-2 py-1">
                            <select class="form-control form-control-sm custom-select custom-select-sm" name="school">
                                <option value="" selected disabled>Filter School</option>
                                <option value="CCDI">CCDI</option>
                                <option value="CAT">CAT</option>
                            </select>
                    </div>
                </div>
            </form> -->


            <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                <table class="table dataTable my-0" id="dataTable">
                    <thead>
                        <tr>
                            <th>School ID</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Contact</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($schools as $school) : ?>
                        <tr>
                            <td><?php echo $school["id"];?></td>
                            <td><?php echo $school["name"];?></td>
                            <td><?php echo $school["address"];?></td>
                            <td><?php echo $school["contact"];?></td>
                            <td><a href="schools.php?option=2&edit=<?php echo $school["id"];?>">Edit</a> 
                            <!-- <?php if($_SESSION["school"] != $school["id"]) : ?>
                            | <a onclick="return confirm('Are you sure you want to delete this School?\nIt will Delete Every Data that is Associated with this School.')" href="schools.php?delete=<?php echo $school["id"];?>">Delete</a></td>
                            <?php endif ?> -->
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>

            <!-- TODO ADD PAGING -->
            <!-- <div class="row">
                <div class="col-md-6 align-self-center">
                    <p id="dataTable_info" class="dataTables_info" role="status" aria-live="polite">Showing 1 to 10 of 27</p>
                </div>
                <div class="col-md-6">
                    <nav class="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers">
                        <ul class="pagination">
                            <li class="page-item disabled"><a class="page-link" href="#" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div> -->
        </div>
    </div>