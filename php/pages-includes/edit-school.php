<?php 
    if(!isset($_GET["edit"]) || empty($_GET["edit"])){
        //echo 'Cannot find ID | <a href="index.php">Click here to return</a>';
        include_once("404.php");
        exit();
    }
    include_once("php/functions/misc.php");
    include_once("php/functions/Query/SchoolController.php");
    include_once("php/functions/Query/CourseController.php");

    $CourseController = new CourseController();
    $courses = $CourseController->FetchAllCourse();

    $SchoolController = new SchoolController();
    $school = $SchoolController->FetchSchool($_GET["edit"]);

    $success = "";
    $errors = array("name" => "", "address" => "", "contact" => "","img"=>"","courses"=>"");

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        //Clean all input before passing it to the array
        $school["name"]    = cleanInput($_POST["name"]);
        $school["address"] = cleanInput($_POST["address"]);
        $school["contact"] = cleanInput($_POST["contact"]);
        //Validate all inputs
        $errors["name"] = checkEmpty($school["name"],"Name");
        $errors["address"] = checkEmpty($school["address"],"Address");
        $currentNumber = strlen($_POST["contact"]);
        
        if($currentNumber != 12){
            $errors["contact"] .= " Contact number should be 12 digit. Starting (639)+9 digit number";
        }

        if(!empty($_POST["courses"]))
        {
            $school["courses"] = $_POST["courses"];
        }else{
            $errors["courses"] = "Courses cannot be Empty.";
        }


        if($_FILES["img"]["error"] == 0){
           
            /* Image Handling */
            $imgDir = "assets/img/schools/";
            $target_file = $imgDir . basename($_FILES["img"]["name"]);
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

            //Check if real image or fake
            $check = getimagesize($_FILES["img"]["tmp_name"]);
            if($check === FALSE){
                $errors["img"] = "Invalid Image File.";
            }
            // Check file size
            if ($_FILES["img"]["size"] > 500000) {
                $errors["img"] = "Sorry, your file is too large.";
            }
            //check for file extension
            if($imageFileType != "jpg" ) {
                $errors["img"] = "Sorry, only JPG files are allowed.";
            }
        }


        
        //Check all Errors (If there is any)
        $validated = true;
        foreach ($errors as $error) {
            if($error != ""){
                $validated = false;
            }
        }

        if($validated){
            if($SchoolController->UpdateSchool($school)){
                $success = "School has been modified.";
                if($_FILES["img"]["error"] == 0){
                    ImageReplace();
                }
            }else{
                $success = '<p class="text-danger">There was an error adding this school</p>';
            }
        }

    }

    function ImageReplace(){
        global $school,$imgDir,$_FILES,$imageFileType;

        if(file_exists($imgDir.$school["id"].".jpg")){
            unlink($imgDir.$school["id"].".jpg");
        }else if(file_exists($imgDir.$school["id"].".png")){
            unlink($imgDir.$school["id"].".png");
        }
        move_uploaded_file($_FILES["img"]["tmp_name"], $imgDir . $school["id"] .".".$imageFileType);
    }

    $SchoolController = NULL;

?>
        <div class="card shadow ">
            <div class="card-header py-3">
                <p class="text-primary m-0 font-weight-bold">Modify School</p>
                <?php returnSuccess($success); ?>
            </div>
            <div class="card-body ">
                <form action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Name <?php returnErrorMsg($errors["name"]);?></label>
                            <input class="form-control" type="text" id="name" name="name" value="<?php echo $school["name"]; ?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="address">Address <?php returnErrorMsg($errors["address"]);?></label>
                            <input class="form-control" type="text" id="address" name="address" value="<?php echo $school["address"]; ?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="contact">Contact <?php returnErrorMsg($errors["contact"]);?></label>
                            <input class="form-control" type="number" id="contact" name="contact" value="<?php echo $school["contact"]; ?>">
                        </div>
                    </div>
                </div>
                <h3>Courses  <?php returnErrorMsg($errors["courses"]); ?></h3>
                <div class="row mt-1">
                    <div class="col-sm-4 ">
                    <?php foreach ($courses as $course) : ?>
                        <div class="form-check">
                            <input 
                            <?php foreach ($school["courses"] as $selectedCourse) {
                                returnStrTrue($selectedCourse,$course["id"],"checked");
                            } ?>
                            class="form-check-input" name="courses[]" type="checkbox"  value="<?php echo $course["id"]; ?>" id="<?php echo $course["id"]; ?>">
                            <label class="form-check-label" for="<?php echo $course["id"]; ?>">
                                <?php echo $course["name"]. ' (<span class="text-info">'.$course["abbr"] .'</span>)'  ?>
                            </label>
                        </div>
                    <?php endforeach ?>
                    </div>
                    <div class="col-sm-4 was-validated">
                        <small>Leave blank if you don't want to replace the current logo.</small>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="validatedCustomFile" name="img" accept=".jpg">
                            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                            <div class="invalid-feedback">Select an Image Logo for the School</div>
                            <?php returnErrorMsg($errors["img"]); ?>
                        </div>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-md-6 m-auto">
                            <input type="submit" class="form-control btn btn-secondary"value="Submit">
                    </div>
                </div>
                </form>    
            </div>
        </div>