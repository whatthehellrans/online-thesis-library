<?php 

    //Check if GET_edit is set,
    if(!isset($_GET["edit"]) || empty($_GET["edit"])){
        //echo 'Cannot find ID | <a href="index.php">Click here to return</a>';
        include_once("404.php");
        exit();
    }

    //Proceed
    include_once("php/functions/misc.php");
    include_once("php/functions/Query/CourseController.php");

    $CourseController = new CourseController();
    $course = $CourseController->FetchCourse($_GET["edit"]);

    $success = "";
    $errors = array("name"=>"","abbr"=>"");


    if($_SERVER["REQUEST_METHOD"] == "POST"){
        //Clean all input before passing it to the array
        $course["name"] = cleanInput($_POST["name"]);
        $course["abbr"] = cleanInput($_POST["abbr"]);
        //Validate all inputs
        $errors["name"] = checkEmpty($course["name"],"Full Name");
        $errors["abbr"] = checkEmpty($course["abbr"],"Abbreviation");

        //Check all Errors (If there is any)
        $validated = true;
        foreach ($errors as $error) {
            if($error != ""){
                $validated = false;
            }
        }

        if($validated){
            if($CourseController->UpdateCourse($course)){
                $success = "Course has been modified.";
            }else{
                $success = '<p class="text-danger">There was an error adding this Course</p>';
            }
            
        }
    }
    $CourseController = NULL;

?>
        <div class="card shadow ">
            <div class="card-header py-3">
                <p class="text-primary m-0 font-weight-bold">Modify Course </p>
                <?php returnSuccess($success); ?>
            </div>
            <div class="card-body ">
                <form action="" method="post">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="name">Full Name <?php returnErrorMsg($errors["name"]);?></label>
                            <input class="form-control" type="text" id="name" name="name" value="<?php echo $course["name"]; ?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="abbr">Abbreviation <?php returnErrorMsg($errors["abbr"]);?></label>
                            <input class="form-control" type="text" id="abbr" name="abbr" value="<?php echo $course["abbr"]; ?>">
                        </div>
                    </div>
                    
                    <div class="col-md-6 m-auto">
                        <input type="submit" class="form-control btn btn-secondary"value="Submit">
                    </div>
                    
                </div>
                
                </form>    
            </div>
        </div>