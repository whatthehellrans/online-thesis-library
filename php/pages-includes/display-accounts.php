<?php 
    include_once("php/functions/Query/AccountController.php");
    include_once("php/functions/Query/SchoolController.php");
    include_once("php/functions/Query/LogController.php");
    
    $AccountController = new AccountController();
    $SchoolController = new SchoolController();
    $accounts = $AccountController->FetchAllAccounts();

    if(isset($_GET["delete"]) && $_GET["delete"] != ""){
        
        if($AccountController->DeleteAccount($_GET["delete"])){
            $LogController = new LogController();
            $LogController->DeleteLogSearchByUser($_GET["delete"]);
            $LogController->DeleteLogStudyByUser($_GET["delete"]);
            $LogController->DeleteLogUser($_GET["delete"]);

            echo "Account Has been Deleted. ";
            echo '<a href="accounts.php">Click here to go back.</a>';
         }else{
            echo "There was an error deleting this account.";
         }
         exit();
    }

    
    $AccountController = NULL;

?>
    <div class="card shadow">
        <div class="card-header py-3">
            <a class="btn btn-primary" href="accounts.php?option=1">Add new Account</a>
        </div>
        <div class="card-body ">


            <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                <table class="table dataTable my-0" id="dataTable">
                    <thead>
                        <tr>
                            <th>Account ID</th>
                            <th>Username</th>
                            <th>Access</th>
                            <th>School</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($accounts as $account) : ?>
                        <tr>
                            <td><?php echo $account["id"];?></td>
                            <td><?php echo $account["username"];?></td>
                            <td><?php echo $account["access"];?></td>
                            <td><?php echo $SchoolController->FetchSchool($account["school_id"])["name"]; ?></td>
                            <td><a href="accounts.php?option=2&edit=<?php echo $account["id"];?>">Edit</a> 
                            <!-- <?php if($_SESSION["username"] != $account["username"]) : ?>
                            | <a onclick="return confirm('Are you sure you want to delete this Account?')"  href="accounts.php?delete=<?php echo $account["id"];?>">Delete</a>
                            <?php endif ?> -->
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>

            <!-- TODO ADD PAGING -->
            <!-- <div class="row">
                <div class="col-md-6 align-self-center">
                    <p id="dataTable_info" class="dataTables_info" role="status" aria-live="polite">Showing 1 to 10 of 27</p>
                </div>
                <div class="col-md-6">
                    <nav class="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers">
                        <ul class="pagination">
                            <li class="page-item disabled"><a class="page-link" href="#" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div> -->
        </div>
    </div>

<?php $SchoolController = NULL //to close sql connection ?>