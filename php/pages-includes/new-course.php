<?php
    include_once("php/functions/misc.php");
    
    $errors = array("name"=>"","abbr"=>"");
    $input = array("name"=>"","abbr"=>"");

    $success = "";


    if($_SERVER["REQUEST_METHOD"] == "POST"){
        //Clean all input before passing it to the array
        $input["name"] = cleanInput($_POST["name"]);
        $input["abbr"] = cleanInput($_POST["abbr"]);
        //Validate all inputs
        $errors["name"] = checkEmpty($input["name"],"Full Name");
        $errors["abbr"] = checkEmpty($input["abbr"],"Abbreviation");

        //Check all Errors (If there is any)
        $validated = true;
        foreach ($errors as $error) {
            if($error != ""){
                $validated = false;
            }
        }

        if($validated){
            include_once("php/functions/Query/CourseController.php");
            $CourseController = new CourseController();
            if($CourseController->InsertCourse($input["name"], $input["abbr"])){
                $success = "Course has been added.";
                $input = returnEmptyVar($input); // --- Clear the input so the default value of form is empty also.
            }else{
                $success = '<p class="text-danger">There was an error adding this course</p>';
            }
            $CourseController = NULL;
        }
    }
?>
        <div class="card shadow ">
            <div class="card-header py-3">
                <p class="text-primary m-0 font-weight-bold">Please fill up this form</p>
                <?php returnSuccess($success); ?>
            </div>
            <div class="card-body ">
                <form action="" method="post">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="name">Full Name <?php returnErrorMsg($errors["name"]);?></label>
                            <input class="form-control" type="text" id="name" name="name" value="<?php echo $input["name"];?>" placeholder="Bachelor of Science in Information Technology">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="abbr">Abbreviation <?php returnErrorMsg($errors["abbr"]);?> </label>
                            <input class="form-control" type="text" id="abbr" name="abbr" value="<?php echo $input["abbr"];?>" placeholder="BSIT">
                        </div>
                    </div>

                    
                    <div class="col-md-6 m-auto">
                        <input type="submit" class="form-control btn btn-secondary" value="Submit">
                    </div>
                    
                </div>
                
                </form>    
            </div>
        </div>