
<?php 
     include_once("php/functions/Query/ThesisController.php");
     include_once("php/functions/Query/SchoolController.php");
     include_once("php/functions/Query/MessageController.php");
 
     $MessageController = new MessageController();
     $SchoolController  = new SchoolController();
     $ThesisController  = new ThesisController();
     $schools = $SchoolController->FetchAllSchool();
     
 
     $search = array("date"=>"","school"=>"");
 
     $search["school"] = (checkUser(MOD,ADMIN))?  $_SESSION["school"] : cleanInput($_GET["school"]);
 
     if(isset($_GET["date"]) && $_GET["date"] != ""){
         $search["date"] = $_GET["date"];
     }
     if(isset($_GET["school"]) && $_GET["school"] != ""){
         $search["school"] = $_GET["school"];
     }
 
     if(checkUser(ADMIN) && isset($_GET["school"]) && $_GET["school"] == ""){
         $search["school"] = "";
     }
 
   
 
     $thesisInvolvedSearch = $ThesisController->FetchThesisBaseOnSchool($search["school"]);
     $result = $MessageController->FetchSearchMessage($search["date"]);
 

?>

                    <div class="card shadow">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Search for Message</p>
                        </div>
                        <div class="card-body ">
                            <form action="" method="get">
                                <div class="row">
                                    <div class="col-md-3">
                                        
                                            <div class="input-group md-form form-sm form-1 pl-0">
                                                <div class="input-group-prepend">
                                                    <button class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search text-white"></i></button>
                                                </div>
                                                <input class="form-control my-0 py-1" type="date" placeholder="date" name="date" value="<?php echo $search["date"]; ?>">
                                            </div>
                                        
                                    </div>
                                    <div class="col-md-3 py-1">
                                            <select <?php if(checkUser(MOD)) echo "disabled"; ?> class="form-control form-control-sm custom-select custom-select-sm" name="school">
                                            <option value="" >Filter School</option>
                                                <?php foreach ($schools as $school) :?>
                                                    <option <?php if(checkModSchool($school["id"])){echo "selected";}?> value="<?php echo $school["id"];?>" ><?php echo $school["name"];?></option>
                                                <?php endforeach; ?>
                                            </select>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table class="table dataTable my-0" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Research Study Involved</th>
                                            <th>School</th>
                                            <th>Sender Name</th>
                                            <th>Date Sent</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($result as $msg) : ?>
                                            <?php $skip = TRUE; 
                                                foreach($thesisInvolvedSearch as $schoolfilter){
                                                    if($msg["thesis"] == $schoolfilter["id"]){
                                                        $skip = FALSE;
                                                    }
                                                }
                                                if($skip && count($thesisInvolvedSearch) != 0){
                                                    continue;
                                                }
                                            ?>
                                        <tr>
                                            <td><?php echo $ThesisController->FetchThesis($msg["thesis"])["title"]; ?></td>
                                            <td><?php echo $SchoolController->FetchSchool($ThesisController->FetchThesis($msg["thesis"])["school"])["name"];?></td>
                                            <td><?php echo $msg["sender"];?></td>
                                            <td><?php echo $msg["date"];?></td>
                                            <td class="text-<?php if($msg["status"] == "Unread") echo "danger font-weight-bold"; else echo "success"; ?> "> <?php echo $msg["status"];?></td>
                                            <!-- <td class="text-danger font-weight-bold">Unread</td> -->
                                            <td><a href="inbox.php?view=<?php echo $msg["id"];?>">View</a></td>
                                        </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- TODO ADD PAGING -->
                            <!-- <div class="row">
                                <div class="col-md-6 align-self-center">
                                    <p id="dataTable_info" class="dataTables_info" role="status" aria-live="polite">Showing 1 to 10 of 27</p>
                                </div>
                                <div class="col-md-6">
                                    <nav class="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers">
                                        <ul class="pagination">
                                            <li class="page-item disabled"><a class="page-link" href="#" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
                                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                            <li class="page-item"><a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div> -->
                        </div>
                    </div>