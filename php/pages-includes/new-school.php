<?php
     include_once("php/functions/misc.php");
     include_once("php/functions/Query/CourseController.php");

     $CourseController = new CourseController();
     $courses = $CourseController->FetchAllCourse();
    
     $errors = array("name"=>"","address"=>"","contact"=>"","img"=>"","courses"=>"");
     $input = array("name"=>"","address"=>"","contact"=>"","img"=>"","courses"=>array());
     $success = "";

     
     if($_SERVER["REQUEST_METHOD"] == "POST"){


        //$input["courses"]   = $_POST["check_courses"];


        //Clean all input before passing it to the array
        $input["name"]      = cleanInput($_POST["name"]);
        $input["address"]   = cleanInput($_POST["address"]);
        $input["contact"]   = cleanInput($_POST["contact"]);
        //validate
        $errors["name"]     =  checkEmpty($input["name"],"Name");
        $errors["address"]  =  checkEmpty($input["address"],"Address");

        if(!empty($_POST["courses"]))
        {
            $input["courses"] = $_POST["courses"];
        }else{
            $errors["courses"] = "Courses cannot be Empty.";
        }


        $currentNumber = strlen($_POST["contact"]);
        if($currentNumber != 12){
            $errors["contact"] .= " Contact number should be 12 digit. Starting (639)+9 digit number";
        }

        /* Image Handling */

        $imgDir = "assets/img/schools/";
        $target_file = $imgDir . basename($_FILES["img"]["name"]);
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        //Check if real image or fake
        $check = getimagesize($_FILES["img"]["tmp_name"]);
        if($check === FALSE){
            $errors["img"] = "Invalid Image File.";
        }
        // Check file size
        if ($_FILES["img"]["size"] > 500000) {
            $errors["img"] = "Sorry, your file is too large.";
        }
        //check for file extension
        if($imageFileType != "jpg" ) {
            $errors["img"] = "Sorry, only JPG files are allowed.";
        }

        
        


        //Check all Errors (If there is any)
        $validated = true;
        foreach ($errors as $error) {
            if($error != ""){
                $validated = false;
            }
        }

        if($validated){
            include_once("php/functions/Query/SchoolController.php");
            $SchoolController = new SchoolController();
            if($SchoolController->InsertSchool($input["name"],$input["address"],$input["contact"],$input["courses"])){
                $success = "School has been added.";
                $input = returnEmptyVar($input); // --- Clear the input so the default value of form is empty also.
                $input["courses"] = array();
            }else{
                $success = '<p class="text-danger">There was an error adding this School</p>';
            }
            $lastID = $SchoolController->FetchLastSchoolID()["id"];
            
            move_uploaded_file($_FILES["img"]["tmp_name"], $imgDir . $lastID .".".$imageFileType);
            
            
            $SchoolController = NULL; // dont need it anymore...
        }
        


     }
?>
        <div class="card shadow ">
            <div class="card-header py-3">
                <p class="text-primary m-0 font-weight-bold">Please fill up this form</p>
                <?php returnSuccess($success); ?>
            </div>
            <div class="card-body ">
                <form action="" method="post"  enctype="multipart/form-data">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="name">Name <?php returnErrorMsg($errors["name"]); ?></label>
                            <input class="form-control" type="text" id="name" name="name" value="<?php echo $input["name"];?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="address">Address <?php returnErrorMsg($errors["address"]); ?></label>
                            <input class="form-control" type="text" id="address" name="address" value="<?php echo $input["address"];?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="contact">Contact <?php returnErrorMsg($errors["contact"]); ?></label>
                            <input class="form-control" type="number" id="contact" name="contact" value="<?php echo $input["contact"];?>">
                        </div>
                    </div>
                </div>
                <h3>Courses  <?php returnErrorMsg($errors["courses"]); ?></h3>
                <div class="row mt-1">
                    <div class="col-sm-4 ">
                    <?php foreach ($courses as $course) : ?>
                        <div class="form-check">
                            <input 
                            <?php foreach ($input["courses"] as $selectedCourse) {
                                returnStrTrue($selectedCourse,$course["id"],"checked");
                            } ?>
                            class="form-check-input" name="courses[]" type="checkbox"  value="<?php echo $course["id"]; ?>" id="<?php echo $course["id"]; ?>">
                            <label class="form-check-label" for="<?php echo $course["id"]; ?>">
                                <?php echo $course["name"]. ' (<span class="text-info">'.$course["abbr"] .'</span>)'  ?>
                            </label>
                        </div>
                    <?php endforeach ?>
                    </div>

                    <div class="col-sm-4 was-validated">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="validatedCustomFile" name="img" accept=".jpg" required>
                            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                            <div class="invalid-feedback">Select an Image Logo for the School</div>
                            <?php returnErrorMsg($errors["img"]); ?>
                        </div>
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-sm-6 m-auto">
                        <input type="submit" class="form-control btn btn-secondary"value="Submit">
                    </div>
                </div>
                
                </form>   


                
                 
            </div>
        </div>