<?php
    include_once("php/functions/Query/CourseController.php");
    $CourseController = new CourseController();
    $courses = $CourseController->FetchAllCourse();

    if(isset($_GET["delete"]) && $_GET["delete"] != ""){
        include_once("php/functions/Query/ThesisController.php");
        include_once("php/functions/Query/MessageController.php");

        $ThesisController  = new ThesisController();
        $MessageController = new MessageController();

        $idToDelete = $_GET["delete"];

        $thesis     = $ThesisController->FetchThesisBaseOnCourse($idToDelete);
        //Start of Thesis Deletion
        foreach ($thesis as $thes) {
            //Start of Message Deletion
            $messages = $MessageController->FetchMessageWithThesisID($thes["id"]);
            foreach ($messages as $msg) {
                //Deleting Messages and Appointments
                $MessageController->DeleteAppointment($msg["id"]);
                $MessageController->DeleteMessage($msg["id"]);
            }
            //End of Message Deletion
            $ThesisController->DeleteThesis($thes["id"]);
        }
        //End of Thesis Deletion
        
        if($CourseController->DeleteCourse($idToDelete)){
            echo "Course Has been Deleted. ";
            echo '<a href="courses.php">Click here to go back.</a>';
         }else{
            echo "There was an error deleting this Course.";
         }
         exit();
    }

    $CourseController = NULL;

?>
    <div class="card shadow">
        <div class="card-header py-3">
            <a class="btn btn-primary" href="courses.php?option=1">Add Course</a>
        </div>
        <div class="card-body ">
 
            <!-- TODO ADD SEARCH -->
            <!-- <form action="" method="get">
                <div class="row">
                    <div class="col-md-6">
                            <div class="input-group md-form form-sm form-1 pl-0">
                                <div class="input-group-prepend">
                                    <button class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search text-white"></i></button>
                                </div>
                                <input class="form-control my-0 py-1" type="text" placeholder="Title Search" name="search">
                            </div>
                    </div>
                    <div class="col-md-2 py-1">
                            <select class="form-control form-control-sm custom-select custom-select-sm" name="school">
                                <option value="" selected disabled>Filter School</option>
                                <option value="CCDI">CCDI</option>
                                <option value="CAT">CAT</option>
                            </select>
                    </div>
                </div>
            </form> -->


            <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                <table class="table dataTable my-0" id="dataTable">
                    <thead>
                        <tr>
                            <th>Course ID</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($courses as $course) : ?>
                        <tr>
                            <td><?php echo $course["id"];?></td>
                            <td><?php echo $course["name"]." (".$course["abbr"].")";?></td>
                            <td><a href="courses.php?option=2&edit=<?php echo $course["id"];?>">Edit</a> 
                           <!--  | <a onclick="return confirm('Are you sure you want to delete this School?\nIt will Delete Every Study that is Associated with this Course.')" href="courses.php?delete=<?php echo $course["id"];?>">Delete</a></td>
                         --></tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>

            
        </div>
    </div>