-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 08, 2019 at 09:43 AM
-- Server version: 5.7.24
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thesis_library`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
CREATE TABLE IF NOT EXISTS `appointment` (
  `message_id` int(11) NOT NULL,
  `meeting_date` timestamp NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `abbr` varchar(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=502 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `full_name`, `abbr`) VALUES
(500, 'Bachelor of Science in Computer Science', 'BSCS'),
(501, 'Bachelor of Science in Information Technology', 'BSIT');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thesis_involved` int(11) NOT NULL,
  `message` varchar(7500) NOT NULL,
  `sender_name` varchar(50) NOT NULL,
  `sender_contact` varchar(24) NOT NULL,
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Unread','Read') NOT NULL DEFAULT 'Unread',
  `response` enum('Accepted','Declined','Unset') NOT NULL DEFAULT 'Unset',
  PRIMARY KEY (`id`),
  KEY `Messages_fk0` (`thesis_involved`)
) ENGINE=MyISAM AUTO_INCREMENT=5006 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `thesis_involved`, `message`, `sender_name`, `sender_contact`, `date_sent`, `status`) VALUES
(5000, 10000, 'Just testing', 'Tester', '123123', '2019-11-07 20:39:10', 'Read'),
(5001, 10000, 'Test message ddd', 'gaga', '123123', '2019-11-05 08:29:20', 'Unread'),
(5002, 10001, 'ytryhr', 'dsgsdgsd', '42343242', '2019-11-08 08:29:29', 'Unread'),
(5003, 10002, 'abs', 'sndna', '34534', '2019-11-08 08:29:42', 'Unread'),
(5004, 10004, 'tetete', 'gagaga', '13123', '2019-11-08 08:29:48', 'Read'),
(5005, 10005, 'CSI TEST', 'CSSCSCS', '1231312', '2019-11-08 08:41:33', 'Unread');

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

DROP TABLE IF EXISTS `schools`;
CREATE TABLE IF NOT EXISTS `schools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` varchar(5000) NOT NULL,
  `contact` varchar(24) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1003 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`id`, `name`, `address`, `contact`) VALUES
(1000, 'CCDI', 'Legazpi City, Albay', '09123456791'),
(1001, 'CSI', 'Legazpi City, Albay', '123456789'),
(1002, 'CAT College', 'Legazpi City, Albay', '432432');

-- --------------------------------------------------------

--
-- Table structure for table `thesis`
--

DROP TABLE IF EXISTS `thesis`;
CREATE TABLE IF NOT EXISTS `thesis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `abstract` varchar(15000) NOT NULL,
  `published_date` date NOT NULL,
  `school` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Thesis_fk0` (`school`),
  KEY `Thesis_fk1` (`course`)
) ENGINE=MyISAM AUTO_INCREMENT=10008 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `thesis`
--

INSERT INTO `thesis` (`id`, `title`, `author`, `abstract`, `published_date`, `school`, `course`) VALUES
(10000, 'Test Title', 'Me', 'dfwal;krj werwp\'\'\' // p\';\'', '2019-10-30', 1000, 500),
(10001, 'Thesis Numba 2', 'Yeyes', 'Eze abstract dfgsdt\'ret/e/gfda \'\' adfsga;', '2019-11-04', 1000, 501),
(10002, 'saddd', 'dasdfas', 'reatert345\'dfgsdfdas', '2019-10-29', 1000, 501),
(10003, 'Cat Thesis Testes', 'asdasd', 'gfdgadfgadfdasd', '2019-11-01', 1002, 501),
(10004, '2nd Thesis Cats', 'Authorrrr', 'dasdas', '2019-11-03', 1002, 500),
(10006, 'asdadasdas', 'ewarwae', 'asd', '2019-11-04', 1002, 500),
(10005, 'CSI 1st thesis', 'Kunyaware', 'd;flsgkeawrt wrweas', '2019-11-04', 1001, 501),
(10007, 'qweqweq144', 'ewarwaege', 'asddfg fdagafdgadf', '2019-11-01', 1002, 501);

-- --------------------------------------------------------

DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags`(
	`thesis_id` INT NOT NULL,
	`tag` varchar (255) NOT NULL
);

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `fname`	varchar(255) NOT NULL,
	`lname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `access` enum('Administrator','School User','Normal User') NOT NULL DEFAULT 'Normal User',
  `school_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `Users_fk0` (`school_id`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `access`, `school_id`) VALUES
(100, 'admin', 'password', 'John', 'Doe', 'johndoe@john.com', 'Administrator', 1000),
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


