<?php 
    include_once("header.php");
    include_once("php/functions/misc.php");

    include_once("php/functions/Query/SchoolController.php");
    $SchoolController   = new SchoolController();
    $schools    = $SchoolController->FetchAllSchool();

?>
            <div class="container-fluid ">
                <h3 class="mb-4 text-dark">Home</h3>
                <div class="row">
                <?php foreach ($schools as $school) : ?>
                    <div class="col-sm-3 mx-auto my-2">
                        <div class="card-deck">
                            <div class="card " >
                                <img class="card-img-top" style="min-height: 150px; max-height:150px; max-width:150px; margin:10px auto;" src="assets/img/schools/<?php echo $school["id"];?>.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $school["name"];?></h5>
                                    <p class="card-text"><?php echo $school["address"];?><br>
                                    <small><?php echo $school["contact"]; ?></small>
                                    </p>
                                </div>
                                <div class="card-footer">
                                    <small class="text-muted"><a href="search.php?title=&school=<?php echo $school["id"]; ?>&course=">Show all research study from this school</a> </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>

                </div>
                <!-- <div class="card-deck">
                    <div class="card " >
                        <img class="card-img-top" src="..." alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Last updated 3 mins ago</small>
                        </div>
                    </div>
                </div> -->

                
            </div>
        </div>
<?php 

    $SchoolController = NULL; // to close sql connection
    include_once("footer.php");
?>
