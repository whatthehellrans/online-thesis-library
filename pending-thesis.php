<?php 

    include_once("header.php");
    include_once("php/functions/misc.php");
    include_once("php/functions/Query/ThesisController.php");
    include_once("php/functions/Query/SchoolController.php");
    include_once("php/functions/Query/CourseController.php");

    $_SESSION["forback"] = basename($_SERVER['REQUEST_URI']);

    $ThesisController   = new ThesisController();
    $SchoolController   = new SchoolController();
    $CourseController   = new CourseController();

    
    if(isset($_GET["delete"]) && $_GET["delete"] != ""){
        $idToDelete = $_GET["delete"];
        if($ThesisController->DeleteThesisPending($idToDelete)){
            echo "Research Study Has been Deleted. ";
            echo '<a href="pending-thesis.php">Click here to go back.</a>';
         }else{
            echo "There was an error deleting this Research Study.";
         }
         exit();
    }

    if(isset($_GET["accept"]) && $_GET["accept"] != "")
    {
        $idToApprove = $_GET["accept"];

        $thesisToInsert = $ThesisController->FetchThesisPending($idToApprove);

        if($ThesisController->InsertThesis($thesisToInsert)){
            $ThesisController->DeleteThesisPending($idToApprove);
            echo "Research Study Has been Added. ";
            echo '<a href="pending-thesis.php">Click here to go back.</a>';
         }else{
            echo "There was an error deleting this Research Study.";
         }
         exit();
    }

    $search = "";
    if(isset($_GET["title"]) && $_GET["title"] != ""){
        $search = $_GET["title"];
    }

    $results = $ThesisController->FetchThesisTitlePending($search);


?>
    <div class="w-75 mx-auto mt-5">
        <h3 class="mb-3 text-dark"><?php echo $pageName; ?></h3>
        <div class="card shadow">
            <div class="card-header py-3">
                <p class="text-primary m-0 font-weight-bold">Search for User's Name</p>
            </div>
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-6">
                            
                                <div class="input-group md-form form-sm form-1 pl-0">
                                    <div class="input-group-prepend">
                                        <button class="input-group-text purple lighten-3" id="basic-text1"><i class="fas fa-search text-white"></i></button>
                                    </div>
                                    <input class="form-control my-0 py-1" type="text" placeholder="First name / Last name" name="search" value="<?php echo $search;?>">
                                </div>
                            
                        </div>
    
                    </div>
                </form>
                <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                    <table class="table dataTable my-0" id="dataTable">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Date - Submitted</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($results as $result) :?>
                            <tr>
                                <td><?php echo $result["title"] ?></td>
                                <td><?php echo $result["date_submitted"] ?></td>
                                <td class="text-info">Pending Approval</td>
                                <td><a href="pending-thesis.php?accept=<?php echo $result["id"] ?>">Accept</a> | <a href="pending-thesis.php?delete=<?php echo $result["id"] ?>">Delete</a></td>    
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php 

    $SchoolController = NULL; // to close sql connection
    include_once("footer.php");
?>
